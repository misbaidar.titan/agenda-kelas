@extends('layouts.app')
@section('title', "Tanggal Agenda Guru | Agenda Kelas")
@section('dailyagenda')
<style>
    input::-webkit-calendar-picker-indicator {
  cursor: pointer !important;
}
</style>
    <div class="py-6">
        <div class="max-w-6xl mx-auto pl-4 sm:px-6 lg:px-8 space-y-6">
            <nav class="flex items-center sm:pl-8 text-gray-500 text-lg font-medium" aria-label="Breadcrumb">
                <ol class="flex items-center space-x-2">
                    <li>
                        <a href="{{ route('class.show', $id) }}" class="hover:text-gray-900">Kelas</a>
                    </li>
                    <li>
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M5.707 14.707a1 1 0 1 1-1.414-1.414L7.586 10 4.293 6.707a1 1 0 1 1 1.414-1.414l4 4a1 1 0 0 1 0 1.414l-4 4z" clip-rule="evenodd" />
                        </svg>
                    </li>
                    <li>
                        <a href="{{ route('daily-agenda.show', [$id, $idClass]) }}" class="text-gray-900">Tanggal agenda guru</a>
                    </li>
                </ol>
            </nav>
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <div>
                    <div class="border-b pb-3 mb-4 border-gray-400">
                        @php
                            $class = \App\Models\Classes::find($idClass);
                        @endphp
                        @if($class)
                            <h2 class="text-lg font-medium text-gray-900">
                                {{ __('Data tanggal agenda guru kelas ') }}{{ $class->class_name }}
                            </h2>
                        @endif
                    </div>
                    <div class="flex my-4 justify-between">
                        <form action="{{ route('daily-agenda.search', [$id, $idClass]) }}" method="GET">
                            <div class="flex">
                                <div class="flex">
                                    <label for="start_date" class="block text-gray-700 self-center mr-2">Data dari</label>
                                    <input type="date" class="my-2 input-text-primary hover:cursor-text focus:outline-none focus:shadow-outline" id="start_date" name="start_date" value="{{ request('start_date') }}" required>
                                </div>
                                <div class="flex">
                                    <label for="end_date" class="block text-gray-700 self-center mx-2">sampai</label>
                                    <input type="date" class="my-2 input-text-primary hover:cursor-text focus:outline-none focus:shadow-outline" id="end_date" name="end_date" value="{{ request('end_date') }}" @if(request()->has('start_date')) {{ 'min='.request('start_date') }} @else {{ 'disabled' }} @endif required>
                                </div>
                                <button type="submit" class="btn-primary p-2 ml-2 self-center">
                                    <svg height="18px" width="18px" stroke-width='1.5' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
                                        <path d='M13.447 20.905A.988.988 0 0 0 14 20.02V14.7c0-.262.105-.514.293-.7l6.121-6.054c.375-.371.586-.875.586-1.4V3.99A.994.994 0 0 0 20 3H4c-.553 0-1 .442-1 .99v2.556c0 .525.211 1.029.586 1.4L9.707 14A.984.984 0 0 1 10 14.7v6.31c0 .735.782 1.213 1.447.884l2-.989Z' fill='#F7F7F7FF'/>
                                    </svg>
                                </button>
                            </div>
                        </form>
                        <!--<div class="flex-end relative self-center"><a href="{{ route('daily-agenda.create', [$id, $idClass]) }}"><button class="btn-primary">tambah</button></a></div>-->
                    </div>
                    <div class="flex mt-4 mb-4">
                        <div class="flex-1">
                            <form action="{{ route('daily-agenda.search', [$id, $idClass]) }}" method="GET">
                                <div class="relative">
                                    <input type="text" name="search" placeholder="Cari" class="py-2 pl-10 min-w-full pr-4 border border-gray-400 rounded-md shadow-sm focus:outline-none">
                                    <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                        <img src="{{ asset('images/search.svg') }}" alt="cari" width="16">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="my-4">
                        @isset ($search)
                            <p class="mt-1 text-sm text-gray-600">Menampilkan hasil pencarian dari "{{ $search }}"</p>
                        @endisset
                    </div>
                    <div class="flex flex-col">
                        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                                <div class="shadow overflow-hidden border border-gray-400 sm:rounded-lg">
                                    <table class="min-w-full divide-y divide-gray-400">
                                        <thead class="bg-gray-100 text-left border-b border-gray-400 text-sm font-medium uppercase tracking-wider">
                                            <tr class="whitespace-nowrap">
                                            <th class="px-6 py-3">Hari</th>
                                            <th class="px-6 py-3 w-2/12">Tanggal</th>
                                            <th class="px-6 py-3 w-9/12">Keterangan</th>
                                            <th class="px-6 py-3"></th>
                                            <th class="px-6 py-3"></th>
                                            </tr>
                                        </thead>
                                        <tbody class="bg-white divide-y divide-gray-400 text-gray-700">
                                            @foreach ($dailyagendas as $daily)
                                                <tr class="whitespace-nowrap"><!--
                                                    <td @if(Auth::user()->hasRole('admin')) class="pl-5 py-4">
                                                        <div class="flex items-center">
                                                            <input type="checkbox" name="selected[]" value="{{ $daily->id }}" class="form-checkbox w-5 h-5 text-gray-600">
                                                        </div>
                                                    @endif
                                                    </td>-->
                                                    <td class="px-6 py-4">{{ $daily->day }}</td>
                                                    <td class="px-6 py-4">{{ $daily->date }}</td>
                                                    <td class="px-6 py-4">{{ $daily->description }}</td>
                                                    <td class="pr-5 py-4">
                                                        <a href="{{ route('daily-content.show', ['id' => $id, 'idClass' => $class->id, 'idDay' => $daily->id]) }}">
                                                            <button class="flex items-center flex-shrink-0 justify-center h-8 w-8 rounded-md text-gray-500 hover:text-gray-400 focus:outline-none focus:ring-2 focus:ring-gray-400 focus:ring-offset-2 focus:ring-offset-white transition-all text-sm">
                                                                <img src="{{ asset('images/right-arrow.svg') }}" alt="pilih">
                                                            </button>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <button class="p-2 btn-primary flex items-center justify-center" onclick="editAgenda('{{ $daily->id }}')">
                                                            <img src="{{ asset('images/edit.svg') }}" alt="edit" width="15px">
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr id="edit-row-{{ $daily->id }}" class="edit-row hidden">
                                                    <td colspan="6" class="px-6 py-4">
                                                    <div>
                                                        <form id="edit-form-{{ $daily->id }}" action="{{ route('daily-agenda.update', [$id, $daily->id]) }}" method="POST">
                                                        @csrf
                                                        @method('PUT')
                                                        <!--<div class="mb-1">
                                                            <label for="date" class="block text-gray-700 font-bold">Tanggal</label>
                                                            <input type="date" name="date" id="date" class="my-2 input-text-primary hover:cursor-text focus:outline-none focus:shadow-outline" value="{{ $daily->date }}">
                                                        </div>-->
                                                        <div class="mb-4">
                                                            <label class="block text-gray-700 font-bold mb-2" for="password">
                                                            Keterangan
                                                            </label>
                                                            <textarea id="description" name="description" rows="4" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">{{ $daily->description }}</textarea>
                                                        </div>
                                                        <div class="flex items-center justify-end">
                                                            <button class="btn-secondary mr-6" type="button" onclick="cancelEdit(this)">
                                                            Batal
                                                            </button>
                                                            <button class="btn-primary" type="submit">
                                                            Simpan
                                                            </button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <form id="delete-form" action="{{ route('daily-agenda.delete-selected', [$id, $idClass]) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            <tr id="delete-row" class="hidden">
                                                <td colspan="6" class="px-6 py-4 text-center">
                                                    <button type="submit" form="delete-form" class="btn-primary">Hapus</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="mt-4">
                                  {{ $dailyagendas->withQueryString()->links('vendor.pagination.custom-pagination', ['perPage' => $perPage, 'idClass' => $idClass]) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function editAgenda(id) {
          document.querySelectorAll('[id^="edit-row-"]').forEach(row => {
            row.classList.add('hidden');
          });
          document.getElementById('edit-row-' + id).classList.remove('hidden');
        }

        function cancelEdit(button) {
        if (!button) {
            console.error('Button parameter is null');
            return;
        }
        var editRow = button.closest('.edit-row');
        if (!editRow) {
            console.error('Edit row not found');
            return;
        }
        editRow.classList.add('hidden');
        location.reload();
        }
        document.getElementById('start_date').addEventListener('change', function() {
            document.getElementById('end_date').disabled = !this.value
            if (!this.value) {
                document.getElementById('end_date').value = ''
            } else {
                document.getElementById('end_date').min = this.value
            }
        });
        const form = document.getElementById('delete-form');
        const dataList = document.getElementsByName('selected[]');
        const deleteRow = document.getElementById('delete-row');
        const deleteBtn = deleteRow.querySelectorAll('button')[0];

        dataList.forEach((checkbox) => {
        checkbox.addEventListener('change', () => {
            const checkedBoxes = document.querySelectorAll('input[name="selected[]"]:checked');
            if (checkedBoxes.length > 0) {
            deleteRow.classList.remove('hidden');
            const lastCheckedRow = checkedBoxes[checkedBoxes.length - 1].closest('tr');
            deleteRow.parentNode.insertBefore(deleteRow, lastCheckedRow.nextSibling);
            checkbox.closest('tr').classList.toggle('bg-red-100');
            } else {
            deleteRow.classList.add('hidden');
            checkbox.closest('tr').classList.remove('bg-red-100');
            }
        });
        });
        deleteBtn.addEventListener('click',  () => {
            const checkedBoxes = document.querySelectorAll('input[name="selected[]"]:checked');
            form.querySelectorAll('input[name="selected[]"]').forEach(c => c.remove())
            checkedBoxes.forEach((checkBox) => {
                form.appendChild(checkBox.cloneNode())
            })
        })
    </script>
@endsection
