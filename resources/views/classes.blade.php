@extends('layouts.app')
@section('title', "Kelas | Agenda Kelas")
@section('classes')
<div class="py-6">
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8 space-y-6">
        <nav class="flex items-center pl-4 sm:pl-8 text-gray-500 text-lg font-medium" aria-label="Breadcrumb">
            <ol class="flex items-center space-x-2">
            <li>
                <a href="{{ route('class.show', $id) }}" class="text-gray-900">Kelas</a>
            </li>
            </ol>
        </nav>
        @if (session('success'))
            <div class="fixed left-1/2 -translate-x-1/2 z-50 message-component flex bg-green-100 border border-green-400 text-green-700 px-2 py-3 shadow-lg rounded" role="alert">
                <div>
                    <strong class="font-bold">Selamat!</strong>
                    <span class="block sm:inline mr-4">{{ session('success') }}</span>
                </div>
                <div class="flex items-center">
                    <button type="button" onclick="this.closest('.message-component').remove()">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-green-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                        </svg>
                    </button>
                </div>
            </div>
        @endif
        @if($errors->any())
            <div class="fixed left-1/2 -translate-x-1/2 z-50 message-component flex bg-red-100 border border-red-400 text-red-700 px-2 py-3 shadow-lg rounded" role="alert">
                <div>
                    <strong class="font-bold">Oops!</strong>
                    <span class="block sm:inline">{{ $errors->first() }}</span>
                </div>
                <div class="flex items-center">
                    <button type="button" onclick="this.closest('.message-component').remove()">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                        </svg>
                    </button>
                </div>
            </div>
        @endif
        <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
            <div>
                <div class="border-b pb-3 mb-4 border-gray-400">
                    <h2 class="text-lg font-medium text-gray-900">
                        {{ __('Data kelas ') }}{{ Auth::user()->school_name }}
                    </h2>
                </div>
                <div class="flex my-4 justify-between">
                    <form action="{{ route('class.search', $id) }}" method="GET">
                        <div class="flex">
                            <div class="relative w-48">
                                <select class="w-full py-2 input-text-primary" onchange="this.form.submit()" style="-webkit-appearance: none;" name="selected_numeral">
                                    <option value="" selected>Semua kelas</option>
                                    @foreach ($grades as $grade)
                                        <option value="{{ $grade }}" {{ request('selected_numeral') == $grade ? 'selected' : '' }}>
                                            {{ $grade }}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                                    <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div align="right" class="self-center"><a href="{{ route('class.create', ['id' => $id]) }}"><button class="btn-primary">tambah</button></a></div>
                </div>
                <div class="flex mt-4 mb-4">
                    <div class="flex-1">
                        <form action="{{ route('class.search', ['id' => $id]) }}" method="GET">
                            <div class="relative">
                                <input type="text" name="search" placeholder="Cari" class="py-2 pl-10 min-w-full pr-4 border border-gray-400 rounded-md shadow-sm focus:outline-none">
                                <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                    <img src="{{ asset('images/search.svg') }}" alt="cari" width="16">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="my-4">
                    <p class="mt-1 text-sm text-gray-600">Total {{ $classes->total() }} hasil</p>
                    @isset ($search)
                        <p class="mt-1 text-sm text-gray-600">Menampilkan hasil pencarian dari "{{ $search }}"</p>
                    @endisset
                </div>
                <div class="flex flex-col">
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="shadow overflow-hidden border border-gray-400 sm:rounded-lg">
                                <table class="min-w-full divide-y divide-gray-400">
                                    <thead class="bg-gray-100 text-left border-b border-gray-400 text-sm font-medium uppercase tracking-wider">
                                        <tr class="whitespace-nowrap">
                                            <th class="px-2 py-4 text-center"></th>
                                            <th class="px-2 py-4">
                                                <div class="flex items-center">
                                                    <input type="checkbox" id="select-all-checkbox" class="form-checkbox w-5 h-5">
                                                </div>
                                            </th>
                                            <th class="px-2 py-3 w-2/12">Nama Kelas</th>
                                            <th class="px-6 py-3 w-2/12">Jumlah Siswa</th>
                                            <th class="px-6 py-3 w-2/12">Jumlah Guru</th>
                                            <th class="px-6 py-3 w-6/12"></th>
                                            <th class="px-6 py-3"></th>
                                        </tr>
                                    </thead>
                                    <tbody class="bg-white divide-y divide-gray-400 text-gray-700">
                                        @foreach($classes as $class)
                                            <tr class="whitespace-nowrap">
                                                <td class="px-2 py-4 text-center border-r border-gray-400">{{ $loop->iteration + $classes->firstItem() - 1 }}</td>
                                                <td class="px-2 py-4">
                                                    <div class="flex items-center">
                                                        <input type="checkbox" name="selected[]" value="{{ $class->id }}" class="form-checkbox w-5 h-5 text-gray-600">
                                                    </div>
                                                </td>
                                                <td class="px-2 py-4">{{ $class->class_name }}</td>
                                                <td class="px-6 py-4">
                                                    <a href="{{ route('student.search', [$id, 'class_id' => $class->id]) }}" class="link">
                                                        {{ $class->students_count }}
                                                        <img src="{{ asset('images/link.svg') }}" alt="edit" width="10px">
                                                    </a>
                                                </td>
                                                <td class="px-6 py-4">
                                                    <a href="{{ route('teachers.cariGuru', [$id, 'idClass' => $class->id]) }}" class="link">
                                                        {{ $class->teachers_count }}
                                                        <img src="{{ asset('images/link.svg') }}" alt="edit" width="10px">
                                                    </a>
                                                </td>
                                                <td class="text-center"><a href="{{ route('daily-agenda.show', ['id' => $id, 'idClass' => $class->id]) }}" class="link">Agenda guru<img src="{{ asset('images/link.svg') }}" alt="edit" width="10px"></a></td>
                                                <td>
                                                    <button class="p-2 btn-primary flex items-center justify-center" onclick="editClass('{{ $class->id }}')">
                                                        <img src="{{ asset('images/edit.svg') }}" alt="edit" width="15px">
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr id="edit-row-{{ $class->id }}" class="edit-row hidden">
                                                <td colspan="8" class="px-6 py-4">
                                                    <div>
                                                        <form id="edit-form-{{ $class->id }}" action="{{ route('class.update', $class->id) }}" method="POST">
                                                            @csrf
                                                            @method('PUT')
                                                            @php
                                                            $classWords = explode(' ', $class->class_name);
                                                            $remainingWords = implode(' ', array_slice($classWords, 0, 1));
                                                            @endphp
                                                            <div class="mb-4">
                                                                <label for="class" class="block text-gray-700 font-bold">Kelas</label>
                                                                <div class="my-2 flex">
                                                                    <div class="relative">
                                                                        <select class="input-text-norounded rounded-l-md pr-6" style="-webkit-appearance: none;" id="class" name="class" required>
                                                                            @for ($i = 1; $i <= 12; $i++)
                                                                                <option value="{{ $i }}"{{ $remainingWords === (string) $i ? 'selected' : '' }}>{{ $i }}</option>
                                                                            @endfor
                                                                        </select>
                                                                        <div class="absolute inset-y-0 right-1 flex items-center pointer-events-none">
                                                                            <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                    @php
                                                                    $classWords = explode(' ', $class->class_name);
                                                                    $remainingWords = implode(' ', array_slice($classWords, 1));
                                                                    @endphp
                                                                    <input type="text" placeholder="ex: RPL 1" id="class_name" name="class_name" class="input-text-norounded grow rounded-r-md"  value="{{ $remainingWords }}" required>
                                                                </div>
                                                            </div>
                                                            <div class="flex items-center justify-end">
                                                                <button class="btn-secondary mr-6" type="button" onclick="cancelEdit(this)">
                                                                Batal
                                                                </button>
                                                                <button class="btn-primary" type="submit">
                                                                Simpan
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        <form id="delete-form" action="{{ route('class.delete-selected', $id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <tr id="delete-row" class="hidden">
                                            <td colspan="5" class="px-6 py-4 text-center">
                                                <dialog id="delete-dialog" class="dialog-component rounded-md">
                                                    <div class="dialog-content">
                                                        <div class="w-full mb-4 message-component flex justify-center items-center bg-yellow-100 border border-yellow-400 text-yellow-700 px-2 py-3 rounded relative" role="alert">
                                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-11 w-11 text-yellow-500 mr-2" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                <circle cx="12" cy="12" r="10" />
                                                                <line x1="12" y1="8" x2="12" y2="12" />
                                                                <line x1="12" y1="16" x2="12" y2="16" />
                                                            </svg>
                                                            <span class="block sm:inline">Apakah Anda yakin ingin menghapus data ini?</span>
                                                        </div>
                                                    </div>
                                                    <div class="dialog-actions flex justify-center">
                                                        <button class="btn-secondary mr-6" onclick="cancelDelete()">Batal</button>
                                                        <button id="delete-button" class="btn-primary" type="submit" form="delete-form">Hapus</button>
                                                    </div>
                                                </dialog>

                                                <button class="btn-primary" onclick="showDeleteDialog()">Hapus</button>
                                            </td>
                                        </tr>
                                        <form id="delete-form" action="{{ route('class.delete-selected', $id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <div id="delete-row" class="fixed z-50 bg-white shadow-[0_0_15px_-2px_rgba(0,0,0,0.3)] rounded-md ">
                                            <div colspan="6" class="px-6 py-4 text-center">
                                                <span id="checkbox-count" class="mr-3"></span>
                                                <button onclick="showDeleteDialog()" class="btn-delete pointer-events-auto">Hapus</button>
                                            </div>
                                        </div>
                                        <dialog id="delete-dialog" class="dialog-component rounded-md">
                                            <div class="dialog-content">
                                                <div class="w-full mb-4 message-component flex justify-center items-center bg-yellow-100 border border-yellow-400 text-yellow-700 px-2 py-3 rounded relative" role="alert">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-11 w-11 text-yellow-500 mr-2" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                        <circle cx="12" cy="12" r="10" />
                                                        <line x1="12" y1="8" x2="12" y2="12" />
                                                        <line x1="12" y1="16" x2="12" y2="16" />
                                                    </svg>
                                                    <span class="block sm:inline">Apakah anda yakin?</span>
                                                </div>
                                            </div>
                                            <div class="dialog-actions flex justify-center">
                                                <button class="btn-secondary mr-6" onclick="cancelDelete()">Batal</button>
                                                <button id="delete-button" class="btn-primary" type="submit" form="delete-form">Yakin</button>
                                            </div>
                                        </dialog>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-4">
                              {{ $classes->withQueryString()->links('vendor.pagination.custom-pagination', ['perPage' => $perPage]) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    #delete-row {
        /* Other styles for the delete-row element */

        /* Initial state */
        opacity: 0;
        transform: translateY(100%);
        pointer-events: none;

        /* Transition properties */
        transition-property: opacity, transform;
        transition-duration: 0.3s;
        transition-timing-function: ease-in-out;

        bottom: 35px;
        left: 50%;
        translate: -50%;
    }

    #delete-row.visible {
        /* Visible state */
        opacity: 1;
        transform: translateY(0);
    }
</style>
<script>
    function editClass(id) {
      document.querySelectorAll('[id^="edit-row-"]').forEach(row => {
        row.classList.add('hidden');
      });
      document.getElementById('edit-row-' + id).classList.remove('hidden');
    }

    function cancelEdit(button) {
    if (!button) {
        console.error('Button parameter is null');
        return;
    }
    var editRow = button.closest('.edit-row');
    if (!editRow) {
        console.error('Edit row not found');
        return;
    }
    editRow.classList.add('hidden');
    location.reload();
    }
</script>

<script>
function showDeleteDialog() {
        const deleteDialog = document.getElementById('delete-dialog');
        deleteDialog.showModal();
    }

    function cancelDelete() {
        const deleteDialog = document.getElementById('delete-dialog');
        deleteDialog.close();
    }
    const selectAllCheckbox = document.getElementById('select-all-checkbox');
    const deleteRow = document.getElementById('delete-row');
    const dataList = document.getElementsByName('selected[]');
    const form = document.getElementById('delete-form');
    const deleteBtn = document.getElementById('delete-button');

    // Function to handle checkbox changes
    function handleCheckboxChange() {
        const checkedBoxes = document.querySelectorAll('input[name="selected[]"]:checked');
        const totalChecked = checkedBoxes.length; // Count the selected checkboxes

        if (totalChecked > 0) {
            deleteRow.classList.add('visible');
        } else {
            deleteRow.classList.remove('visible');
        }

        // Display the count somewhere in your UI, e.g., in a <span> element with the id "checkbox-count"
        const checkboxCountElement = document.getElementById('checkbox-count');
        if (checkboxCountElement) {
            checkboxCountElement.textContent = `${totalChecked} data terpilih`;
        }

        dataList.forEach((checkbox) => {
            checkbox.closest('tr').classList.remove('bg-red-100');
        });

        checkedBoxes.forEach((checkbox) => {
            checkbox.closest('tr').classList.add('bg-red-100');
        });
    }

    // Event listener for the "Select All" checkbox
    selectAllCheckbox.addEventListener('change', () => {
        dataList.forEach((checkbox) => {
            checkbox.checked = selectAllCheckbox.checked;
        });

        handleCheckboxChange();
    });

        dataList.forEach((checkbox) => {
        checkbox.addEventListener('change', () => {
            if (!checkbox.checked) {
                // If any individual checkbox is unchecked, uncheck the "Select All" checkbox
                selectAllCheckbox.checked = false;
            }

            handleCheckboxChange();
        });
    });

    // Event listeners for individual checkboxes
    dataList.forEach((checkbox) => {
        checkbox.addEventListener('change', () => {
            handleCheckboxChange();
        });
    });

    // Function to handle deleting selected rows
    deleteBtn.addEventListener('click', () => {
        const checkedBoxes = document.querySelectorAll('input[name="selected[]"]:checked');
        form.querySelectorAll('input[name="selected[]"]').forEach((c) => c.remove());
        checkedBoxes.forEach((checkBox) => {
            form.appendChild(checkBox.cloneNode());
        });
    });
</script>
@endsection
