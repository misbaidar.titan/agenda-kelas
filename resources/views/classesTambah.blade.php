@extends('layouts.app')
@section('title', "Tambah | Agenda Kelas")
@section('classesTambah')
<div class="flex flex-col items-center">
    <div class="w-full sm:max-w-xl p-4 overflow-hidden">
        <nav class="flex items-center pb-4 sm:pl-8 text-gray-500 text-lg font-medium" aria-label="Breadcrumb">
            <ol class="flex items-center space-x-2">
            <li>
                <a href="{{ route('class.show', $id) }}" class="hover:text-gray-900">Kelas</a>
            </li>
            <li>
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                <path fill-rule="evenodd" d="M5.707 14.707a1 1 0 1 1-1.414-1.414L7.586 10 4.293 6.707a1 1 0 1 1 1.414-1.414l4 4a1 1 0 0 1 0 1.414l-4 4z" clip-rule="evenodd" />
                </svg>
            </li>
            <li>
                <a href="#" class="text-gray-900">Tambah data</a>
            </li>
            </ol>
        </nav>
        @if($errors->any())
            <div class="w-full mb-4 message-component flex justify-between bg-red-100 border border-red-400 text-red-700 px-2 py-3 rounded relative" role="alert">
                <div>
                    <strong class="font-bold">Oops!</strong>
                    <span class="block sm:inline">{{ $errors->first() }}</span>
                </div>
                <div class="flex items-center">
                    <button type="button" onclick="this.closest('.message-component').remove()">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                        </svg>
                    </button>
                </div>
            </div>
        @endif
        <div class="container mx-auto">
            <form id="classForm" action="{{ route('class.store', $id) }}" method="POST">
                @csrf
                <div class="bg-white shadow-md rounded px-8 py-8 mb-8">
                    <label for="classAmount" class="block mb-2">Jumlah kelas yang akan ditambahkan:</label>
                    <input type="number" id="classAmount" name="classAmount" min="1" class="mb-2 input-text-primary pr-2" onchange="updateClassFields()">
                </div>
                <div id="classFields" class="block"></div>

                <div class="flex justify-end">
                <button type="submit" class="btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function updateClassFields() {
        const classAmount = document.getElementById('classAmount').value;
        let classFields = '';

        for (let i = 1; i <= classAmount; i++) {
            classFields += `
                <div class="class-field-group bg-white shadow-md rounded px-8 pt-6 pb-8 mb-8">
                    <div class="border-b pb-3 mb-3 border-gray-400">
                        <h3 class="text-1xl text-center font-bold">Kelas baru ${i}</h3>
                    </div>
                    <label for="class${i}" class="block text-gray-700 font-bold">Kelas</label>
                    <div class="my-2 flex">
                        <div class="relative">
                            <select class="input-text-norounded rounded-l-md pr-6" style="-webkit-appearance: none;" id="class${i}" name="class${i}" required>
                                <option value="" selected disabled>Pilih kelas</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                            <div class="absolute inset-y-0 right-1 flex items-center pointer-events-none">
                                <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                </svg>
                            </div>
                        </div>
                        <input type="text" placeholder="ex: RPL 1" id="class${i}_name" name="class${i}_name" class="input-text-norounded grow rounded-r-md" required>
                    </div>
                </div>`;
        }

        document.getElementById('classFields').innerHTML = classFields;
    }
</script>


@endsection
