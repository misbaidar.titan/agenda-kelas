@extends('layouts.app')
@section('title', "Tambah | Agenda Kelas")
@section('scheduleTambah')
<div class="flex flex-col items-center">
    <div class="w-full sm:max-w-xl p-4 overflow-hidden">
        <nav class="flex items-center pb-4 sm:pl-8 text-gray-500 text-lg font-medium" aria-label="Breadcrumb">
            <ol class="flex items-center space-x-2">
                <li>
                    <a href="{{ route('schedule.timetable', [$id, 'selected_class' => $idClass]) }}" class="hover:text-gray-900">Jadwal</a>
                </li>
                <li>
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M5.707 14.707a1 1 0 1 1-1.414-1.414L7.586 10 4.293 6.707a1 1 0 1 1 1.414-1.414l4 4a1 1 0 0 1 0 1.414l-4 4z" clip-rule="evenodd" />
                    </svg>
                </li>
                <li>
                    <a href="{{ route('schedule.show', [$id, $idClass]) }}" class="hover:text-gray-900">Kelola Jadwal</a>
                </li>
                <li>
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M5.707 14.707a1 1 0 1 1-1.414-1.414L7.586 10 4.293 6.707a1 1 0 1 1 1.414-1.414l4 4a1 1 0 0 1 0 1.414l-4 4z" clip-rule="evenodd" />
                    </svg>
                </li>
                <li>
                    <a href="#" class="text-gray-900">Tambah data</a>
                </li>
            </ol>
        </nav>
        @if($errors->any())
            <div class="fixed left-1/2 -translate-x-1/2 z-50 message-component flex bg-red-100 border border-red-400 text-red-700 px-2 py-3 shadow-lg rounded" role="alert">
                <div>
                    <strong class="font-bold">Oops!</strong>
                    <span class="block sm:inline">{{ $errors->first() }}</span>
                </div>
                <div class="flex items-center">
                    <button type="button" onclick="this.closest('.message-component').remove()">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                        </svg>
                    </button>
                </div>
            </div>
        @endif
        <div class="container mx-auto">
            <form id="scheduleForm" action="{{ route('schedule.store', [$id, $idClass]) }}" method="POST">
                @csrf
                <div class="bg-white shadow-md rounded px-8 py-8 mb-8">
                    <label for="scheduleAmount" class="block mb-2">Jumlah jadwal yang akan ditambahkan:</label>
                    <input type="number" id="scheduleAmount" name="scheduleAmount" min="1" class="mb-2 input-text-primary pr-2" onchange="updateScheduleField()">
                </div>
                <div id="scheduleFields" class="block"></div>

                <div class="flex justify-end">
                <button type="submit" class="btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function updateScheduleField() {
        const scheduleAmount = document.getElementById('scheduleAmount').value;
        let scheduleFields = '';

        for (let i = 1; i <= scheduleAmount; i++) {
            scheduleFields += `
                <div class="class-field-group bg-white shadow-md rounded px-8 pt-6 pb-8 mb-8">
                    <div class="border-b pb-3 mb-3 border-gray-400">
                        <h3 class="text-1xl text-center font-bold">Jadwal baru ${i}</h3>
                    </div>
                    <label for="day${i}" class="block text-gray-700 font-bold">Hari</label>
                    <div class="relative">
                        <select id="day${i}" name="day${i}" class="w-full py-2 my-2 input-text-primary" style="-webkit-appearance: none;" required>
                            <option value="senin">Senin</option>
                            <option value="selasa">Selasa</option>
                            <option value="rabu">Rabu</option>
                            <option value="kamis">Kamis</option>
                            <option value="jumat">Jumat</option>
                        </select>
                        <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                            <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                            </svg>
                        </div>
                    </div>
                    <label for="teacher${i}" class="block text-gray-700 font-bold">Guru</label>
                    <div class="relative">
                        <select id="teacher${i}" name="teacher${i}" class="w-full py-2 my-2 input-text-primary" style="-webkit-appearance: none;" required onchange="updateSubjectDropdown(this, 'subject${i}')">
                            <option value="" disabled selected>Pilih Guru</option>
                            @foreach ($teachers as $teacher)
                                <option value="{{ $teacher->id }}">{{ $teacher->teacher_name }}</option>
                            @endforeach
                        </select>
                        <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                            <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                            </svg>
                        </div>
                    </div>
                    <label for="subject${i}" class="block text-gray-700 font-bold">Mata Pelajaran</label>
                    <div class="relative">
                        <select id="subject${i}" name="subject${i}" class="w-full py-2 my-2 input-text-primary" style="-webkit-appearance: none;" required disabled>
                            <option value="" disabled selected>Pilih Guru terlebih dahulu</option>
                        </select>
                        <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                            <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                            </svg>
                        </div>
                    </div>
                    <label for="start_hour${i}" class="block text-gray-700 font-bold">Jam Pelajaran</label>
                    <div class="my-2 flex">
                        <div class="relative">
                            <select class="input-text-norounded rounded-l-md pr-6" style="-webkit-appearance: none;" id="start_hour${i}" name="start_hour${i}" required onchange="populateEndHour(${i})">
                                <option value="">Jam Mulai</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                            <div class="absolute inset-y-0 right-1 flex items-center pointer-events-none">
                                <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                </svg>
                            </div>
                        </div>
                        <div class="relative">
                            <select class="input-text-norounded rounded-r-md pr-6" style="-webkit-appearance: none;" id="end_hour${i}" name="end_hour${i}" required disabled>
                                <option value="">Jam Akhir</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                            <div class="absolute inset-y-0 right-1 flex items-center pointer-events-none">
                                <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>`;
        }

        document.getElementById('scheduleFields').innerHTML = scheduleFields;
    }

    function updateSubjectDropdown(teacherDropdown, subjectDropdownId) {
    const selectedTeacherId = teacherDropdown.value;
    const subjectDropdown = document.getElementById(subjectDropdownId);
    subjectDropdown.innerHTML = '';

    if (selectedTeacherId) {
        const availableSubjects = @json($availableSubjects);
        const teacherSubjects = availableSubjects[selectedTeacherId];console.log(teacherSubjects);

        if (teacherSubjects) {
            for (const subject of teacherSubjects) {
                const option = document.createElement('option');
                option.value = subject.id;
                option.textContent = subject.subject_name;
                subjectDropdown.appendChild(option);
            }
        }

        subjectDropdown.removeAttribute('disabled');
    } else {
        subjectDropdown.innerHTML = '<option value="" disabled selected>Pilih Guru terlebih dahulu</option>';
        subjectDropdown.setAttribute('disabled', 'disabled');
    }
}

function populateEndHour(index) {
        var startHourSelect = document.getElementById(`start_hour${index}`);
        var endHourSelect = document.getElementById(`end_hour${index}`);
        var startHour = startHourSelect.options[startHourSelect.selectedIndex].value;

        endHourSelect.innerHTML = '';

        var defaultOption = document.createElement('option');
        defaultOption.value = '';
        defaultOption.text = 'Jam Akhir';
        endHourSelect.add(defaultOption);

        for (var i = parseInt(startHour); i <= 10; i++) {
            var option = document.createElement('option');
            option.value = i;
            option.text = i;
            endHourSelect.add(option);
        }

        endHourSelect.disabled = false;
    }
</script>

@endsection
