@extends('layouts.app')
@section('title', "Tambah | Agenda Kelas")
@section('studentTambah')
<div class="flex flex-col items-center">
    <div class="w-full sm:max-w-xl p-4 overflow-hidden">
        <nav class="flex items-center pb-4 sm:pl-8 text-gray-500 text-lg font-medium" aria-label="Breadcrumb">
            <ol class="flex items-center space-x-2">
            <li>
                <a href="{{ route('student.show', $id) }}" class="hover:text-gray-900">Siswa</a>
            </li>
            <li>
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                <path fill-rule="evenodd" d="M5.707 14.707a1 1 0 1 1-1.414-1.414L7.586 10 4.293 6.707a1 1 0 1 1 1.414-1.414l4 4a1 1 0 0 1 0 1.414l-4 4z" clip-rule="evenodd" />
                </svg>
            </li>
            <li>
                <a href="#" class="text-gray-900">Tambah data</a>
            </li>
            </ol>
        </nav>
        @if (session('success'))
            <div class="fixed left-1/2 -translate-x-1/2 z-50 message-component flex bg-green-100 border border-green-400 text-green-700 px-2 py-3 shadow-lg rounded" role="alert">
                <div>
                    <strong class="font-bold">Selamat!</strong>
                    <span class="block sm:inline mr-4">{{ session('success') }}</span>
                </div>
                <div class="flex items-center">
                    <button type="button" onclick="this.closest('.message-component').remove()">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-green-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                        </svg>
                    </button>
                </div>
            </div>
        @endif
        @if($errors->any())
            <div class="fixed left-1/2 -translate-x-1/2 z-50 message-component flex bg-red-100 border border-red-400 text-red-700 px-2 py-3 shadow-lg rounded" role="alert">
                <div>
                    <strong class="font-bold">Oops!</strong>
                    <span class="block sm:inline">{{ $errors->first() }}</span>
                </div>
                <div class="flex items-center">
                    <button type="button" onclick="this.closest('.message-component').remove()">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                        </svg>
                    </button>
                </div>
            </div>
        @endif
        <div class="container mx-auto">
            <form id="studentForm" action="{{ route('student.store', $id) }}" method="POST">
                @csrf
                <div class="bg-white shadow-md rounded px-8 py-8 mb-8">
                    <label for="studentAmount" class="block mb-2">Jumlah siswa yang akan ditambahkan:</label>
                    <input type="number" id="studentAmount" name="studentAmount" min="1" class="mb-2 input-text-primary pr-2" onchange="updateStudentFields()">
                </div>
                <div id="studentFields" class="block"></div>

                <div class="flex justify-end">
                <button type="submit" class="btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function updateStudentFields() {
        const studentAmount = document.getElementById('studentAmount').value;
        let studentFields = '';

        for (let i = 1; i <= studentAmount; i++) {
            studentFields += `
                <div class="class-field-group bg-white shadow-md rounded px-8 pt-6 pb-8 mb-8">
                    <div class="border-b pb-3 mb-3 border-gray-400">
                        <h3 class="text-1xl text-center font-bold">Siswa baru ${i}</h3>
                    </div>
                    <label for="student${i}_kode" class="block text-gray-700 font-bold">Kode</label>
                    <p class="text-xs text-red-600">Gunakan NISN untuk kode siswa (harus 10 digit).</p>
                    <div class="relative">
                        <input type="number" id="student${i}_kode" name="student${i}_kode" class="w-full my-2 input-text-primary pr-2" required oninput="updateKodeIndicator(${i})">
                        <span id="kodeIndicator${i}" class="absolute inset-y-0 right-4 flex items-center pr-2 text-sm text-gray-400">0/10</span>
                    </div>
                    <label for="student${i}_name" class="block text-gray-700 font-bold">Nama</label>
                    <input type="text" id="student${i}_name" name="student${i}_name" class="w-full my-2 input-text-primary" required>
                    <label class="block text-gray-700 font-bold" for="class${i}_name">
                        Kelas
                    </label>
                    <div class="relative" id="classContainer${i}_name">
                        <select class="w-full py-2 my-2 input-text-primary" style="-webkit-appearance: none;" id="class${i}_name" name="class${i}_name" required>
                            <option value="" selected>Pilih kelas</option>
                            @foreach ($classes as $class)
                                <option value="{{ $class->class_name }}">
                                    {{ $class->class_name }}
                                </option>
                            @endforeach
                        </select>
                        <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                            <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                            </svg>
                        </div>
                    </div>
                    <div id="kelasbaru${i}_container" class="hidden">
                        <div class="my-2 flex">
                            <div class="relative">
                                <select class="input-text-norounded rounded-l-md pr-6" style="-webkit-appearance: none;" id="tingkatkelasbaru${i}_name" name="tingkatkelasbaru${i}_name">
                                    <option value="" selected disabled>Pilih kelas</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                                <div class="absolute inset-y-0 right-1 flex items-center pointer-events-none">
                                    <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                    </svg>
                                </div>
                            </div>
                            <input type="text" placeholder="ex: RPL 1" id="kelasbaru${i}_name" name="kelasbaru${i}_name" class="input-text-norounded grow rounded-r-md">
                        </div>
                    </div>
                    <label for="useKelasBaru${i}" class="inline-flex space-x-1 text-gray-700 text-sm font-bold">
                        <input type="checkbox" id="useKelasBaru${i}" onclick="toggleKelasBaruField(${i})">
                        <p>Buat Kelas Baru <span class="text-xs font-medium">(jika tidak ada kelas)</span></p>
                    </label>
                </div>`;
        }

        document.getElementById('studentFields').innerHTML = studentFields;
    }

function updateKodeIndicator(index) {
    const input = document.getElementById(`student${index}_kode`);
    const kodeIndicator = document.getElementById(`kodeIndicator${index}`);
    kodeIndicator.textContent = `${input.value.length}/10`;
}
function toggleKelasBaruField(i) {
        const checkbox = document.getElementById(`useKelasBaru${i}`);
        const inputContainer = document.getElementById(`kelasbaru${i}_container`);
        const inputField = document.getElementById(`kelasbaru${i}_name`);
        const inputGradeDrop = document.getElementById(`tingkatkelasbaru${i}_name`);
        const ClassDropField = document.getElementById(`class${i}_name`);
        const ClassContainer = document.getElementById(`classContainer${i}_name`);

        if (checkbox.checked) {
            inputContainer.classList.remove("hidden");
            inputField.setAttribute("required", true);
            inputGradeDrop.setAttribute("required", true);
            ClassDropField.removeAttribute("required");
            ClassContainer.classList.add("hidden");
            ClassDropField.value = "";
        } else {
            inputContainer.classList.add("hidden");
            inputField.removeAttribute("required");
            inputGradeDrop.removeAttribute("required");
            inputField.value = "";
            inputGradeDrop.value = "";
            ClassDropField.setAttribute("required", true);
            ClassContainer.classList.remove("hidden");
        }
    }
</script>


@endsection
