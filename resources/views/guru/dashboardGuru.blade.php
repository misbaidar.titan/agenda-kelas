@extends('layouts.app')
@section('title', "Dashboard | Agenda Kelas")
@section('dashboardGuru')
<div class="py-6">
@if (session('success'))
    <div class="fixed left-1/2 -translate-x-1/2 z-50 message-component flex bg-green-100 border border-green-400 text-green-700 px-2 py-3 shadow-lg rounded" role="alert">
        <div>
            <strong class="font-bold">Selamat!</strong>
            <span class="block sm:inline mr-4">{{ session('success') }}</span>
        </div>
        <div class="flex items-center">
            <button type="button" onclick="this.closest('.message-component').remove()">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-green-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                </svg>
            </button>
        </div>
    </div>
@endif
@if($errors->any())
    <div class="fixed left-1/2 -translate-x-1/2 z-50 message-component flex bg-red-100 border border-red-400 text-red-700 px-2 py-3 shadow-lg rounded" role="alert">
        <div>
            <strong class="font-bold">Oops!</strong>
            <span class="block sm:inline">{{ $errors->first() }}</span>
        </div>
        <div class="flex items-center">
            <button type="button" onclick="this.closest('.message-component').remove()">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                </svg>
            </button>
        </div>
    </div>
@endif
@foreach ($schedule as $s)
<div id="hs-agenda-modal{{ $s->id }}" class="hs-overlay hidden w-full h-full fixed top-0 left-0 z-[60] overflow-x-hidden overflow-y-auto">
    <div class="hs-overlay-open:mt-7 hs-overlay-open:opacity-100 hs-overlay-open:duration-500 mt-0 opacity-0 ease-out transition-all sm:max-w-lg sm:w-full mb-6 sm:mx-auto">
        <form id="agenda-form{{ $s->id }}" action="{{ isset($s->dailycontent)?route('dashboard-guru.update',[$id, 'day_id' => $s->day_id]):route('dashboard-guru.store',[$id, 'day_id' => $s->day_id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @if(isset($s->dailycontent))
                @method('PUT')
            @endif
        <input type="hidden" name="subject_id" value="{{ $s->subject->id }}">
        <input type="hidden" name="teacher_id" value="{{ $s->teacher->id }}">
        <input type="hidden" name="start_hour" value="{{ $s->start_hour }}">
        <input type="hidden" name="end_hour" value="{{ $s->end_hour }}">
        <div class="p-6 bg-white border border-gray-200 rounded-lg shadow">
            <div class="border-b pb-3 mb-3 border-gray-400">
                <h2 class="text-xl text-left font-bold">Agenda Kelas {{ $s->class->class_name }}</h2>
                <span class="text-sm text-gray-500">{{ $s->teacher->teacher_name }} - {{ $s->subject->subject_name }} - Jam pelajaran ke
                                            @if ($s->start_hour === $s->end_hour)
                                                {{ $s->start_hour }}
                                            @else
                                                {{ $s->start_hour }} sampai {{ $s->end_hour }}
                                            @endif</span>
            </div>
            <div class="mb-1">
                <div class="flex justify-between items-center mb-2">
                    <label for="confirmation_image{{ $s->id }}" class="block text-gray-700 font-bold">Bukti Foto</label>
                    <p class="block text-xs text-red-700 uppercase font-semibold">wajib diisi</p>
                </div>
                @if (isset($s->dailycontent) && $s->dailycontent->confirmation_image)
                    <img src="{{ asset('storage/confirmation_images/'.$s->dailycontent->confirmation_image) }}" alt="Confirmation Image" width="200px" class="mb-2">
                @endif
                <input type="file" name="confirmation_image" id="confirmation_image{{ $s->id }}" accept="image/*" class="input-text-primary mb-2 p-0 cursor-pointer w-full
                file:border-0 file:cursor-pointer
                file:bg-gray-800 file:mr-4 file:text-white
                file:py-3 file:px-4" accept="image/*"@if(!isset($s->dailycontent)) required @endif>
            </div>
            <div class="mb-1">
                <div class="flex justify-between items-center mb-2">
                    <label for="confirmation_image{{ $s->id }}" class="block text-gray-700 font-bold">Kompetensi Dasar</label>
                    <p class="block text-xs text-red-700 uppercase font-semibold">wajib diisi</p>
                </div>
                <input type="text" name="indicator" id="indicator{{ $s->id }}" class="w-full input-text-primary mb-2" value="{{ old('indicator', $s->dailycontent->indicator ?? '') }}" required>
            </div>
            <div class="mb-1">
                <div class="flex justify-between items-center">
                    <label for="learning_activities{{ $s->id }}" class="block text-gray-700 font-bold mb-2">Kegiatan Pembelajaran</label>
                    <p class="block text-xs text-red-700 uppercase font-semibold">wajib diisi</p>
                </div>
                <textarea name="learning_activities" id="learning_activities{{ $s->id }}" class="w-full input-text-primary mb-2" rows="4" required>{{ old('learning_activities', $s->dailycontent->learning_activities ?? '') }}</textarea>
            </div>
            <div class="mb-4">
                <label for="description{{ $s->id }}" class="block text-gray-700 font-bold mb-2">Deskripsi Kegiatan</label>
                <textarea name="description" id="description{{ $s->id }}" class="w-full input-text-primary mb-2" rows="4">{{ old('description', $s->dailycontent->description ?? '') }}</textarea>
            </div>
            <div class="flex items-center justify-end space-x-2">
                <button class="btn-secondary" onclick="location.reload();" type="button" data-hs-overlay="#hs-agenda-modal">
                Batal
                </button>
                <button class="btn-primary" type="submit">
                Simpan
                </button>
            </div>
        </div>
        </form>
    </div>
</div>
@endforeach

@foreach ($classArray as $classData)
<div id="hs-absent-modal{{ $classData['class_schedule']->id }}" class="hs-overlay hidden w-full h-full fixed top-0 left-0 z-[60] overflow-x-hidden overflow-y-auto">
    <div class="hs-overlay-open:mt-7 hs-overlay-open:opacity-100 hs-overlay-open:duration-500 mt-0 opacity-0 ease-out transition-all sm:max-w-lg sm:w-full mb-6 sm:mx-auto">
        <form id="absent-form{{ $classData['class_schedule']->id }}" action="{{ route('dashboard-guru.storeAbsent',[$id,'day_id'=>$classData['daily_agenda']->id]) }}" method="POST">
            @csrf
            <div class="p-6 bg-white border border-gray-200 rounded-lg shadow">
                <div class="mb-4">
                    <label for="absent" class="block text-gray-700 font-bold mb-2">Siswa yang tidak hadir hari ini kelas {{ $classData['class_schedule']->class_name }}</label>
                    <div class="flex">
                        <div id="absentStudent{{ $classData['class_schedule']->id }}" class="flex flex-wrap">
                            @foreach ($classData['absent_students'] as $absent_student)
                            @php
                            $reason = $absent_student->reason;
                            $colorClass = '';
                            switch ($reason) {
                                case 'Sakit':
                                    $colorClass = 'bg-green-100 text-green-700 ring-green-500/40';
                                    break;
                                case 'Izin':
                                    $colorClass = 'bg-yellow-100 text-yellow-700 ring-yellow-500/40';
                                    break;
                                case 'Alpa':
                                    $colorClass = 'bg-red-100 text-red-700 ring-red-500/40';
                                    break;
                                default:
                                    $colorClass = '';
                            }
                            @endphp
                            <div id="selection{{ $absent_student->student->id }}{{ $classData['class_schedule']->id }}" class="{{ $colorClass }} mb-1 text-xs rounded-md uppercase ring-1 ring-inset py-1 px-2 mr-2">
                                {{ $absent_student->student->student_name }} - {{ $absent_student->student->student_kode }} ({{ $absent_student->reason }})
                                <button type="button" class="ml-1 text-inherit cursor-pointer" onclick="removeSelectionEdit({{ $absent_student->student->id }},{{ $classData['class_schedule']->id }})">×</button>
                            </div>
                            <input type="hidden" name="absent[]" id="absentHidden{{ $absent_student->student->id }}{{ $classData['class_schedule']->id }}" value="{{ $absent_student->student->id }},{{ $absent_student->reason }}">
                            @endforeach
                            <div class="hs-dropdown [--auto-close:inside]">
                                <button id="hs-dropdown-tambah" class="hs-dropdown-toggle self-center w-3 fill-gray-800" type="button" title="tambah">
                                    <svg id="Layer_1" style="enable-background:new 0 0 32 32;" version="1.1" viewBox="0 0 32 32" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M28,14H18V4c0-1.104-0.896-2-2-2s-2,0.896-2,2v10H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h10v10c0,1.104,0.896,2,2,2  s2-0.896,2-2V18h10c1.104,0,2-0.896,2-2S29.104,14,28,14z"/></svg>
                                </button>
                                <div class="hs-dropdown-menu transition-[opacity,margin] duration hs-dropdown-open:opacity-100 opacity-0 z-80 origin-top-right rounded-md bg-white p-3 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none hidden" id="myDropdown{{ $classData['class_schedule']->id }}" aria-labelledby="hs-dropdown-auto-close-inside">
                                    <div class="my-2 flex">
                                        <div class="relative">
                                            <select class="input-text-norounded grow rounded-l-md pr-6" style="-webkit-appearance: none;" id="student{{ $classData['class_schedule']->id }}" name="student" onchange="handleSelection({{ $classData['class_schedule']->id }})">
                                                <option value="">Pilih Siswa</option>
                                                @foreach ($classData['students'] as $student)
                                                    <option value="{{ $student->id }}" data-id="{{ $student->id }}">{{ $student->student_name }} - {{ $student->student_kode }}</option>
                                                @endforeach
                                            </select>
                                            <div class="absolute inset-y-0 right-1 flex items-center pointer-events-none">
                                                <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="relative">
                                            <select class="input-text-norounded rounded-r-md pr-6" style="-webkit-appearance: none;" id="reason{{ $classData['class_schedule']->id }}" name="reason" onchange="handleSelection({{ $classData['class_schedule']->id }})">
                                                <option value="">Alasan</option>
                                                <option value="Sakit">Sakit</option>
                                                <option value="Izin">Izin</option>
                                                <option value="Alpa">Alpa</option>
                                            </select>
                                            <div class="absolute inset-y-0 right-1 flex items-center pointer-events-none">
                                                <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flex items-center justify-end space-x-2">
                    <button class="btn-secondary" onclick="location.reload();" type="button" data-hs-overlay="#hs-absent-modal">
                    Batal
                    </button>
                    <button class="btn-primary" type="submit">
                    Simpan
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endforeach

    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8 space-y-6">
        <nav class="flex items-center pl-4 sm:pl-8 text-gray-500 text-lg font-medium" aria-label="Breadcrumb">
            <ol class="flex items-center space-x-2">
            <li>
                <a href="{{ route('dashboard.show', $id) }}" class="text-gray-900">Dashboard</a>
            </li>
            </ol>
        </nav>
        <div class="sm:flex sm:gap-4 space-y-4 sm:space-y-0">
            <div class="flex flex-col sm:gap-4 sm:min-w-fit sm:h-full justify-between">
                <div class="p-6 sm:w-[350px] bg-white border border-gray-200 rounded-lg shadow">
                    @foreach ($teacher as $t)
                    <div class="flex flex-col">
                        <div class="flex border-b border-gray-400 pb-4">
                            @if ($t->picture)
                                <img src="{{ asset('storage/pictures/'.$t->picture) }}" class="object-cover w-20 h-20 mb-3 rounded-full shadow-lg" alt="picture">
                            @else
                            <img src="{{ asset('images/default-profile.svg') }}" alt="Default Profile Photo" class="object-cover w-24 h-24 mb-3 rounded-full shadow-lg">
                            @endif
                            <div class="flex w-full items-start">
                                <div class="flex-row ml-3">
                                    <p class="text-xl font-medium text-gray-900">{{ $t->teacher_name }}</p>
                                    <span class="text-sm text-gray-500">{{ $t->kode }}</span>
                                    <span class="text-sm items-center flex text-gray-500 group">{{ $t->email }}
                                        <div class="hs-dropdown [--auto-close:inside]">
                                            <button id="hs-dropdown-email" class="hs-dropdown-toggle hidden group-hover:block focus:block btn-secondary bg-transparent p-0 border-0">
                                                <svg class="feather feather-edit" fill="none" height="16" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"/><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"/></svg>
                                            </button>
                                            <div class="hs-dropdown-menu transition-[opacity,margin] duration hs-dropdown-open:opacity-100 opacity-0 z-100 origin-top-right rounded-md bg-white p-3 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none hidden" aria-labelledby="hs-dropdown-auto-close-inside">
                                                <form action="{{ route('dashboard-guru.updateEmail', [$id, 'user_id' => $t->user_id]) }}" method="post">
                                                    <div class="flex items-center gap-1">
                                                        @csrf
                                                        <input type="email" class="input-text-primary group-focus:block" name="email" placeholder="Email baru">
                                                        <button class="btn-primary fill-white p-[6px]">
                                                            <svg xmlns="http://www.w3.org/2000/svg" class="w-5" viewBox="0 0 512 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.7 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z"/></svg>
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="self-start mt-4 w-5/6 space-y-4">
                            <div class="relative">
                                <div class="font-semibold text-sm px-1 whitespace-nowrap">Mata Pelajaran</div>
                                @foreach (explode(',', $t->subject_names) as $subject)
                                    <p class="inline-flex items-center rounded-md bg-gray-100 hover:-translate-y-0.5 transition px-2 py-1 my-0.5 text-xs font-medium uppercase text-gray-700 ring-1 ring-inset ring-gray-500/40">
                                        {{ $subject }}
                                    </p>
                                @endforeach
                            </div>
                            <div class="relative">
                                <div class="font-semibold text-sm px-1">Kelas</div>
                                @foreach (explode(',', $t->class_names) as $class)
                                    <p class="inline-flex items-center rounded-md bg-gray-100 hover:-translate-y-0.5 transition px-2 py-1 my-0.5 text-xs font-medium uppercase text-gray-700 ring-1 ring-inset ring-gray-500/40">
                                        {{ $class }}
                                    </p>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="flex-grow flex-col space-y-4">
                <div class="text-sm font-medium text-center bg-white divide-x divide-gray-300 rounded-lg shadow flex">
                    <div class="group w-full">
                      <button id="agendaButton" type="button" class="flex w-full justify-center items-center p-4 rounded-l-lg text-gray-800 ease-in-out transition-all">
                        <svg xmlns="http://www.w3.org/2000/svg" class="mr-1 w-3" viewBox="0 0 384 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M192 0c-41.8 0-77.4 26.7-90.5 64H64C28.7 64 0 92.7 0 128V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V128c0-35.3-28.7-64-64-64H282.5C269.4 26.7 233.8 0 192 0zm0 64a32 32 0 1 1 0 64 32 32 0 1 1 0-64zM72 272a24 24 0 1 1 48 0 24 24 0 1 1 -48 0zm104-16H304c8.8 0 16 7.2 16 16s-7.2 16-16 16H176c-8.8 0-16-7.2-16-16s7.2-16 16-16zM72 368a24 24 0 1 1 48 0 24 24 0 1 1 -48 0zm88 0c0-8.8 7.2-16 16-16H304c8.8 0 16 7.2 16 16s-7.2 16-16 16H176c-8.8 0-16-7.2-16-16z"/></svg>
                        Agenda Hari Ini
                      </button>
                      <div id="agendaLine" class="bg-gray-800 m-auto w-11/12 h-1 rounded-t-md group-hover:bg-gray-800 ease-in-out transition-all"></div>
                    </div>
                    <div class="w-full group">
                        <button id="jadwalButton" type="button" class="flex w-full justify-center items-center p-4 text-gray-800 ease-in-out transition-all">
                            <svg xmlns="http://www.w3.org/2000/svg" class="mr-1 w-3" viewBox="0 0 448 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M96 32V64H48C21.5 64 0 85.5 0 112v48H448V112c0-26.5-21.5-48-48-48H352V32c0-17.7-14.3-32-32-32s-32 14.3-32 32V64H160V32c0-17.7-14.3-32-32-32S96 14.3 96 32zM448 192H0V464c0 26.5 21.5 48 48 48H400c26.5 0 48-21.5 48-48V192z"/></svg>
                            Jadwal
                        </button>
                        <div id="jadwalLine" class="m-auto w-11/12 h-1 rounded-t-md group-hover:bg-gray-800 ease-in-out transition-all"></div>
                    </div>
                    <div class="w-full group">
                        <button id="arsipButton" type="button" class="flex w-full justify-center items-center p-4 rounded-r-lg text-gray-800 ease-in-out transition-all">
                            <svg xmlns="http://www.w3.org/2000/svg" class="mr-1 w-3" viewBox="0 0 512 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M75 75L41 41C25.9 25.9 0 36.6 0 57.9V168c0 13.3 10.7 24 24 24H134.1c21.4 0 32.1-25.9 17-41l-30.8-30.8C155 85.5 203 64 256 64c106 0 192 86 192 192s-86 192-192 192c-40.8 0-78.6-12.7-109.7-34.4c-14.5-10.1-34.4-6.6-44.6 7.9s-6.6 34.4 7.9 44.6C151.2 495 201.7 512 256 512c141.4 0 256-114.6 256-256S397.4 0 256 0C185.3 0 121.3 28.7 75 75zm181 53c-13.3 0-24 10.7-24 24V256c0 6.4 2.5 12.5 7 17l72 72c9.4 9.4 24.6 9.4 33.9 0s9.4-24.6 0-33.9l-65-65V152c0-13.3-10.7-24-24-24z"/></svg>
                            Riwayat
                        </button>
                        <div id="arsipLine" class="m-auto w-11/12 h-1 rounded-t-md group-hover:bg-gray-800 ease-in-out transition-all"></div>
                    </div>
                </div>
                <div id="agendaContent" class="flex flex-col space-y-4">
                    <div class="flex flex-col p-4 bg-white w-full sm:rounded-lg shadow ease-in-out transition-all">
                        <div class="sm:flex sm:justify-between sm:items-center border-b pb-3 mb-4 space-y-1 border-gray-400">
                            <h2 class="text-lg font-medium text-gray-900">
                                Jadwal agenda kelas anda hari ini - {{ \Carbon\Carbon::today()->locale('id')->isoFormat("dddd, D MMMM Y") }}
                            </h2>
                            <div class="relative w-fit">
                                <select class="w-full pl-3 pr-8 input-text-primary" style="-webkit-appearance: none;" id="classFilter" required>
                                    <option value="" selected>Semua kelas</option>
                                    @php
                                        $uniqueClasses = $schedule->unique('class.id');
                                    @endphp

                                    @foreach ($uniqueClasses as $s)
                                        <option value="{{ $s->class->class_name }}">
                                            {{ $s->class->class_name }}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                                    <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div class="space-y-2">
                            @foreach($schedule as $s)
                            <ul role="list" data-class="{{ $s->class->class_name }}" class="schedule divide-y px-4 divide-gray-100 rounded-lg border border-gray-300 hover:transform hover:-translate-y-1 hover:shadow-md transition-all">
                                <li class="flex justify-between gap-x-6 py-5">
                                    <div class="flex min-w-0 gap-x-4">
                                        <div class="min-w-0 flex-auto">
                                            <div class="flex flex-wrap items-center gap-2">
                                                <p class="text-sm font-semibold leading-6 text-gray-900">{{ $s->class->class_name }}</p>
                                                <span class="flex-shrink-0 w-max-content rounded-md px-2 text-xs font-thin leading-loose border {{ $s->status === 'Belum diisi' ? 'bg-red-100 text-red-700 border-red-500/40' : ($s->status === 'Sudah diisi' ? 'bg-green-100 text-green-700 border-green-500/40' : '') }}">{{ $s->status }}</span>
                                            </div>
                                            <p class="mt-1 truncate text-xs leading-5 text-gray-500">{{ $s->subject->subject_name }} - Jam pelajaran ke
                                                @if ($s->start_hour === $s->end_hour)
                                                    {{ $s->start_hour }}
                                                @else
                                                    {{ $s->start_hour }} sampai {{ $s->end_hour }}
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    <div class="shrink-0 self-center">
                                        <button class="btn-secondary border-0 w-9 h-9 p-[5px] hover:-translate-y-0.5 hover:bg-gray-200 focus:bg-gray-200 transition-all fill-gray-800" data-hs-overlay="#hs-agenda-modal{{ $s->id }}">
                                            <svg class="feather feather-edit" fill="none" height="24" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"/><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"/></svg>
                                        </button>
                                    </div>
                                </li>
                            </ul>
                            @endforeach
                        </div>
                    </div>
                    <div class="p-6 bg-white border border-gray-200 rounded-lg shadow">
                        <div class="border-b pb-3 mb-4 border-gray-400">
                            <h2 class="text-lg font-medium text-gray-900">
                                Siswa yang tidak hadir di kelas anda hari ini
                            </h2>
                        </div>
                        <div class="space-y-2">
                            @foreach($classArray as $classData)
                            <ul role="list" class="divide-y px-4 divide-gray-100 rounded-lg border border-gray-300 hover:transform hover:-translate-y-1 hover:shadow-md transition-all">
                                <li class="flex justify-between gap-x-6 py-5">
                                    <div class="flex min-w-0 gap-x-4">
                                        <div class="min-w-0 flex-auto space-y-2">
                                            <div class="mb-4">
                                                <p class="text-sm font-semibold leading-6 text-gray-900">{{ $classData['class_schedule']->class_name }}</p>
                                            </div>
                                            @if(!empty($classData['absent_students']))
                                                <div class="flex flex-row flex-wrap gap-2">
                                                    @foreach ($classData['absent_students'] as $absent_student)
                                                        @php
                                                        $reason = $absent_student->reason;
                                                        $colorClass = '';
                                                        switch ($reason) {
                                                            case 'Sakit':
                                                                $colorClass = 'bg-green-400';
                                                                break;
                                                            case 'Izin':
                                                                $colorClass = 'bg-yellow-400';
                                                                break;
                                                            case 'Alpa':
                                                                $colorClass = 'bg-red-400';
                                                                break;
                                                            default:
                                                                $colorClass = '';
                                                        }
                                                        @endphp
                                                        <div class="divide-y overflow-hidden divide-gray-100 rounded-lg border border-gray-300 hover:transform hover:-translate-y-1 hover:shadow-md transition-all">
                                                            <div class="flex justify-between gap-x-6">
                                                                <div class="relative flex min-w-0 gap-x-4 py-2 px-4">
                                                                    <div class="absolute inset-y-0 left-0 {{ $colorClass }} w-3"></div>
                                                                    <div class="min-w-0 ml-2 flex-auto">
                                                                        <div class="flex w-full items-start">
                                                                            <div class="flex-row">
                                                                                <p class="text-base font-medium text-gray-900">{{ $absent_student->student->student_name }}</p>
                                                                                <p class="text-xs text-gray-500">{{ $absent_student->student->student_kode }}</p>
                                                                            </div>
                                                                        </div>
                                                                        <p class="mt-2 truncate text-sm leading-5 text-gray-500">Alasan: {{ $absent_student->reason }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @else
                                                <div>
                                                    -
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="shrink-0 self-center">
                                        <button class="btn-secondary border-0 w-9 h-9 p-[5px] hover:-translate-y-0.5 hover:bg-gray-200 focus:bg-gray-200 transition-all fill-gray-800" data-hs-overlay="#hs-absent-modal{{ $classData['class_schedule']->id }}">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M96 128a128 128 0 1 1 256 0A128 128 0 1 1 96 128zM0 482.3C0 383.8 79.8 304 178.3 304h91.4C368.2 304 448 383.8 448 482.3c0 16.4-13.3 29.7-29.7 29.7H29.7C13.3 512 0 498.7 0 482.3zM504 312V248H440c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V136c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H552v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z"/></svg>
                                        </button>
                                    </div>
                                </li>
                            </ul>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div id="jadwalContent" class="hidden p-4 bg-white w-full sm:rounded-lg shadow">
                    <div class="border-b pb-3 mb-4 border-gray-400">
                        <h2 class="text-lg font-medium text-gray-900">
                            Jadwal mengajar anda
                        </h2>
                    </div>
                    <div class=" overflow-x-auto">
                        <div class="w-[650px] m-auto">
                            <table class="min-w-full divide-y divide-gray-400 text-left table-fixed">
                                <tr class="bg-gray-100">
                                    <th class="px-3 py-3 border border-gray-400 text-xs font-medium uppercase whitespace-nowrap">Jam ke</th>
                                    @foreach($weekDays as $day)
                                    <th class="px-3 py-3 w-1/5 border border-gray-400 text-xs font-medium uppercase">{{ $day }}</th>
                                    @endforeach
                                </tr>@php
                                $backgroundColors = [
                                    'border-red-600',
                                    'border-blue-600',
                                    'border-green-600',
                                    'border-yellow-600',
                                    'border-indigo-600',
                                ];
                                $colorIndex = 0;

                                function getNextColor(&$colorIndex, $backgroundColors) {
                                    $colorClass = $backgroundColors[$colorIndex];
                                    $colorIndex = ($colorIndex + 1) % count($backgroundColors);
                                    return $colorClass;
                                }
                                @endphp

                                @foreach($hours as $hour)
                                    <tr class="text-center">
                                        <th class="border border-gray-400 py-5 text-xs font-medium uppercase whitespace-nowrap">{{ $hour }}</th>
                                        @foreach($weekDays as $day)
                                            @if($remainingDuration[$day] > 0)
                                                @php $remainingDuration[$day]--; @endphp
                                            @elseif(isset($schedules[$day][$hour]))
                                                @php $colorClass = getNextColor($colorIndex, $backgroundColors); @endphp
                                                <td rowspan="{{ $schedules[$day][$hour]->first()->duration ?? 1 }}" class="border border-gray-400 bg-gray-50 relative">
                                                    @foreach($schedules[$day][$hour] as $schedule)
                                                        <div class="absolute inset-0 pl-2 border-l-4 hover:border-l-8 {{ $colorClass }} duration-150 flex flex-col justify-center text-left">
                                                            <div class="font-bold">{{ $schedule->class->class_name }}</div>
                                                            <div class="text-sm">{{ $schedule->subject->subject_name }}</div>
                                                        </div>
                                                    @endforeach
                                                </td>
                                                @php $remainingDuration[$day] = $schedules[$day][$hour]->first()->duration - 1; @endphp
                                            @else
                                                <td class="border border-gray-400 px-6 py-4"></td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                <div id="arsipContent" class="hidden p-4 bg-white w-full sm:rounded-lg shadow">
                    <div class="flex justify-between items-center border-b pb-3 mb-4 border-gray-400">
                        <h2 class="text-lg font-medium text-gray-900">
                            Riwayat agenda kelas anda sebulan terakhir
                        </h2>
                        <div class="flex justify-end">
                            <p class="text-sm self-center mr-3">Urutkan dari:</p>
                            <button onclick="toggleSort('asc')" class="btn-sort rounded-s-md">Terlama</button>
                            <button onclick="toggleSort('desc')" class="btn-sort rounded-e-md">Terbaru</button>
                        </div>
                    </div>
                    <div class="flex flex-col mx-auto gap-2 max-w-2xl" id="archives-container">
                        @foreach ($archives as $date => $classes)
                        <div class="rounded-md border border-gray-300 overflow-hidden" data-date="{{ $date }}">
                            <div onclick="toggleClasses('{{ $date }}')" class="divide-y p-4 divide-gray-300 cursor-pointer hover:bg-gray-100 transition-all">
                                <div class="flex w-full items-center justify-between min-w-0">
                                    <div class="font-semibold min-w-0 flex-auto">
                                        {{ \Carbon\Carbon::parse($date)->locale('id')->isoFormat("D MMMM Y") }}
                                    </div>
                                    <svg class="w-2.5 h-2.5" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M2 5L8.16086 10.6869C8.35239 10.8637 8.64761 10.8637 8.83914 10.6869L15 5" stroke="currentColor" stroke-width="2" stroke-linecap="round"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="{{ $date }}-classes" class="hidden max-w-2xl">
                                @foreach ($classes as $classId => $contents)
                                    <div onclick="toggleContents('{{ $contents->first()->dailyAgenda->class->id }}', '{{ $date }}')" class="px-4 py-2 border-t border-gray-300 cursor-pointer hover:bg-gray-100 transition-all">
                                        <p>{{ $contents->first()->dailyAgenda->class->class_name }}</p>
                                    </div>
                                    <div id="contents{{ $contents->first()->dailyAgenda->class->id }}{{ $date }}" class="hidden max-w-2xl overflow-x-auto">
                                        <table class="w-full max-w-full divide-y divide-gray-400 text-left">
                                            <tr class="bg-gray-100">
                                                <th class="px-1 py-3 border border-gray-400 text-center text-xs font-medium uppercase whitespace-nowrap">Jam Ke</th>
                                                <th class="px-1 py-3 border border-gray-400 text-center text-xs font-medium uppercase whitespace-nowrap">Mata Pelajaran</th>
                                                <th class="px-1 py-3 border border-gray-400 text-center text-xs font-medium uppercase whitespace-nowrap">Kompetensi Dasar</th>
                                                <th class="px-1 py-3 border border-gray-400 text-center text-xs font-medium uppercase whitespace-nowrap">Kegiatan Pembelajaran</th>
                                                <th class="px-1 py-3 border border-gray-400 text-center text-xs font-medium uppercase whitespace-nowrap">Deskripsi</th>
                                                <th class="px-1 py-3 border border-gray-400 text-center text-xs font-medium uppercase whitespace-nowrap">Bukti Foto</th>
                                            </tr>
                                            @foreach ($contents as $content)
                                            <tr>
                                                <td class="px-3 text-center border border-gray-400">{{ $content->lesson_hours }}</td>
                                                <td class="px-3 border border-gray-400">{{ $content->subject->subject_name }}</td>
                                                <td class="px-3 border border-gray-400">{{ $content->indicator }}</td>
                                                <td class="px-3 border border-gray-400">{{ $content->learning_activities }}</td>
                                                <td class="px-3 border border-gray-400">{{ $content->description }}</td>
                                                <td class="px-3 border border-gray-400"><img src="{{ asset('storage/confirmation_images/'.$content->confirmation_image) }}" alt="Confirmation Image Preview" width="200px"></td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.getElementById('classFilter').addEventListener('change', function() {
        var selectedClass = this.value;
        var schedules = document.querySelectorAll('.schedule');

        schedules.forEach(function(schedule) {
            var scheduleClass = schedule.getAttribute('data-class');
            schedule.style.display = (selectedClass === '' || selectedClass === scheduleClass) ? 'block' : 'none';
        });
    });

    function toggleSort(order) {
        const container = document.getElementById('archives-container');
        const items = container.children;

        const sortedItems = Array.from(items).sort((a, b) => {
            const dateA = new Date(a.getAttribute('data-date'));
            const dateB = new Date(b.getAttribute('data-date'));

            return order === 'asc' ? dateA - dateB : dateB - dateA;
        });

        container.innerHTML = '';
        sortedItems.forEach(item => container.appendChild(item));
    }

    function toggleContents(classId, date) {
        const contentsDiv = document.getElementById(`contents${classId}${date}`);
        contentsDiv.classList.toggle('hidden');
    }

    function toggleClasses(date) {
        const contentsDiv = document.getElementById(`${date}-classes`);
        contentsDiv.classList.toggle('hidden');
    }

    function removeSelectionEdit(studentId,idSchedule) {
        const selectionElement = document.getElementById('selection' + studentId + idSchedule);
        const hiddenField = document.getElementById('absentHidden' + studentId + idSchedule);

        if (selectionElement && hiddenField) {
            selectionElement.parentNode.removeChild(selectionElement);
            hiddenField.parentNode.removeChild(hiddenField);
        }
    }

    function handleSelection(idSchedule) {
        const studentDropdown = document.getElementById('student' + idSchedule);
        const reasonDropdown = document.getElementById('reason' + idSchedule);
        const absentStudentContainer = document.getElementById('absentStudent' + idSchedule);
        const dropdown = document.getElementById('myDropdown' + idSchedule);

        const selectedStudent = studentDropdown.value;
        const selectedStudentText = studentDropdown.options[studentDropdown.selectedIndex].text;
        const selectedReason = reasonDropdown.value;

        if (selectedStudent && selectedReason) {
            studentDropdown.value = '';
            reasonDropdown.value = '';

            const hiddenFieldId = `absentHidden${Date.now()}`;
            const selectionId = `selection${Date.now()}`;

            const hiddenField = document.createElement('input');
            hiddenField.type = 'hidden';
            hiddenField.name = 'absent[]';
            hiddenField.id = hiddenFieldId;
            hiddenField.value = `${selectedStudent},${selectedReason}`;


            const selectionElement = document.createElement('div');
            selectionElement.id = selectionId;
            selectionElement.className = getReasonClass(selectedReason);
            selectionElement.textContent = `${selectedStudentText} (${selectedReason})`;


            const removeButton = document.createElement('button');
            removeButton.type = 'button';
            removeButton.className = 'ml-1 text-inherit cursor-pointer';
            removeButton.textContent = '×';
            removeButton.onclick = () => removeSelection(hiddenFieldId, selectionElement, idSchedule);


            selectionElement.appendChild(removeButton);
            absentStudentContainer.insertBefore(selectionElement, absentStudentContainer.lastChild.previousSibling);


            const form = document.getElementById('absent-form' + idSchedule);
            if (form) {
                form.appendChild(hiddenField);
            }

            document.body.click();
        }
    }

    function removeSelection(hiddenFieldId, selectionElement, idSchedule) {
        const absentStudentContainer = document.getElementById('absentStudent' + idSchedule);
        absentStudentContainer.removeChild(selectionElement);

        const hiddenField = document.getElementById(hiddenFieldId);
        if (hiddenField) {
            hiddenField.parentNode.removeChild(hiddenField);
        }
    }
    function getReasonClass(reason) {
        switch (reason) {
            case 'Sakit':
                return 'bg-green-100 mb-1 text-xs text-green-700 rounded-md uppercase ring-1 ring-inset ring-green-500/40 py-1 px-2 mr-2';
            case 'Izin':
                return 'bg-yellow-100 mb-1 text-xs text-yellow-700 rounded-md uppercase ring-1 ring-inset ring-yellow-500/40 py-1 px-2 mr-2';
            case 'Alpa':
                return 'bg-red-100 mb-1 text-xs text-red-700 rounded-md uppercase ring-1 ring-inset ring-red-500/40 py-1 px-2 mr-2';
            default:
                return '';
        }
    }

    // Get references to your buttons and content sections
    const agendaButton = document.getElementById('agendaButton');
    const jadwalButton = document.getElementById('jadwalButton');
    const arsipButton = document.getElementById('arsipButton');
    const agendaLine = document.getElementById('agendaLine');
    const jadwalLine = document.getElementById('jadwalLine');
    const arsipLine = document.getElementById('arsipLine');
    const agendaContent = document.getElementById('agendaContent');
    const jadwalContent = document.getElementById('jadwalContent');
    const arsipContent = document.getElementById('arsipContent');

    // Function to handle tab switching
    function switchTab(activeLine, activeContent) {
    agendaContent.classList.add('hidden');
    jadwalContent.classList.add('hidden');
    arsipContent.classList.add('hidden');
    agendaLine.classList.remove('bg-gray-800');
    jadwalLine.classList.remove('bg-gray-800');
    arsipLine.classList.remove('bg-gray-800');
    activeContent.classList.remove('hidden');
    activeLine.classList.add('bg-gray-800');
    }

    // Add click event listeners to the buttons
    agendaButton.addEventListener('click', () => {
    switchTab(agendaLine, agendaContent);
    });

    jadwalButton.addEventListener('click', () => {
    switchTab(jadwalLine, jadwalContent);
    });

    arsipButton.addEventListener('click', () => {
    switchTab(arsipLine, arsipContent);
    });
</script>
@endsection
