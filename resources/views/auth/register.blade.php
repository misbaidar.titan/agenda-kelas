@extends('layouts.auth')
@section('title', 'Register | Agenda Kelas')
@section('register')
<div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
    @if($errors->any())
        <div class="w-full sm:max-w-md mb-2 message-component flex justify-between bg-red-100 border border-red-400 text-red-700 px-2 py-3 rounded relative" role="alert">
            <div>
                <strong class="font-bold">Oops!</strong>
                <span class="block sm:inline">{{ $errors->first() }}</span>
            </div>
            <div class="flex items-center">
                <button type="button" onclick="this.closest('.message-component').remove()">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                    </svg>
                </button>
            </div>
        </div>
    @endif
    <form action="{{ route('register') }}" method="post">
        @csrf
        <div>
            <label class="block font-medium text-sm text-gray-700">Nama Sekolah</label>
            <input type="text" name="school_name" class="block mt-1 w-full input-text-primary">
        </div>
        <div class="mt-4">
            <label class="block font-medium text-sm text-gray-700">Nama Pengelola</label>
            <input type="text" name="name" class="block mt-1 w-full input-text-primary">
        </div>
        <div class="mt-4">
            <label class="block font-medium text-sm text-gray-700">Password</label>
            <input type="password" name="password" class="block mt-1 w-full input-text-primary">
        </div>
        <div class="flex justify-between mt-4">
            <div class="self-center">
                <a class="btn-secondary" href="/auth">Kembali</a>
            </div>
            <div class="self-center">
                <button type="submit" class="btn-primary">Register</button>
            </div>
        </div>
    </form>
</div>
@endsection
