@extends('layouts.auth')
@section('title','Log in | Agenda Kelas')
@section('login')
<style>
#logo-container {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    animation: fadeIn 1s ease;
    z-index: 2;
}

@keyframes fadeIn {
    0% {
        opacity: 0;
        transform: translate(-50%, -50%) scale(0.8);
    }
    100% {
        opacity: 1;
        transform: translate(-50%, -50%) scale(1);
    }
}
    #cover {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 3;
    transform: translateY(100%);
    }

    @keyframes slideDown {
    0% {
    transform: translateY(100%);
    }
    100% {
    transform: translateY(0);
    }
}
</style>
<div class="w-screen h-screen bg-white">
    <div id="logo-container">
        <img src="{{ asset('images/agendalogodefault.svg') }}" class="w-36" alt="agenda kelas digital">
    </div>
</div>
<div id="cover" class="min-h-screen flex flex-col sm:justify-center items-center sm:pt-0 bg-gray-100">
    <div class="w-full sm:max-w-5xl bg-white sm:rounded-lg overflow-hidden shadow-md">
        <div class="flex sm:flex-row sm:min-h-[500px] min-h-screen flex-col">
            <div class="flex flex-col sm:w-1/2 w-full p-9 bg-gray-800 justify-between">
                <div class="self-center mb-4">
                    <img src="{{ asset('images/agendalogo.svg') }}" class="fill-white w-32" alt="agenda kelas digital" style="user-drag: none; -webkit-user-drag: none;">
                </div>
                <p class="font-semibold tracking-[1px] text-white">Kami menyediakan pengalaman terbaik dalam pengelolaan agenda kelas anda</p>
            </div>
            <div class="flex flex-col grow px-6 py-4 justify-between">
                <h1 class="font-bold text-4xl text-center text-gray-800 my-4 hidden sm:block">Selamat Datang!</h1>
                <form action="{{ route('login') }}" method="post">
                    @csrf
                    @if($errors->any())
                        <div class="w-full mb-2 message-component flex justify-between bg-red-100 border border-red-400 text-red-700 px-2 py-3 rounded relative" role="alert">
                            <div>
                                <strong class="font-bold">Oops!</strong>
                                <span class="block sm:inline">{{ $errors->first() }}</span>
                            </div>
                            <div class="flex items-center">
                                <button type="button" onclick="this.closest('.message-component').remove()">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                                    </svg>
                                </button>
                            </div>
                        </div>
                    @endif
                    @if (session('message'))
                        <div class="w-full mb-2 message-component flex justify-between bg-green-100 border border-green-400 text-green-700 px-2 py-3 rounded relative" role="alert">
                            <div>
                                <strong class="font-bold">Selamat!</strong>
                                <span class="block sm:inline">{{ session('message') }}</span>
                            </div>
                            <div class="flex items-center">
                                <button type="button" onclick="this.closest('.message-component').remove()">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-green-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                                    </svg>
                                </button>
                            </div>
                        </div>
                    @endif
                    <div>
                        <label for="email" class="block font-medium text-sm text-gray-700">Email</label>
                        <div class="relative">
                            <input type="email" name="email" id="email" class="block mt-1 w-full input-text-primary" oninput="updateIndicator()">
                            <span id="indicator" class="absolute inset-y-0 right-4 flex items-center text-sm text-gray-400">0</span>
                        </div>
                    </div>
                    <div class="mt-4">
                        <label for="password" class="block font-medium text-sm text-gray-700">Password</label>
                        <input type="password" name="password" class="block mt-1 w-full input-text-primary">
                        <div class="flex justify-end">
                            <a href="{{ route('forgotForm') }}" class="hover:underline font-light text-gray-500 cursor-pointer text-sm">Lupa password?</a>
                        </div>
                    </div>
                    <div class="mb-2 flex items-center space-x-1">
                        <input type="checkbox" name="remember" id="remember" class="hover:cursor-pointer" value="1">
                        <label for="remember" class="text-sm text-gray-700 font-bold hover:cursor-pointer">Remember me</label>
                    </div>
                    <div class="flex mt-4">
                        <button type="submit" class="btn-primary justify-center w-full">Login</button>
                    </div>
                </form>
                <p class="text-end text-xs text-gray-500 mt-4">© Agenda Kelas Digital</p>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            var cover = document.getElementById("cover");

            var animationOccurred = localStorage.getItem("animationOccurred");

            if (!animationOccurred) {
                setTimeout(function() {
                    cover.style.animation = "slideDown 1s ease-in-out forwards";

                    localStorage.setItem("animationOccurred", "true");

                    setTimeout(function() {
                        localStorage.removeItem("animationOccurred");
                    }, 5 * 60 * 1000);
                }, 1500);
            } else {
                cover.style.transform = "translateY(0)";

                setTimeout(function() {
                    localStorage.removeItem("animationOccurred");
                }, 5 * 60 * 1000);
            }
        });
    </script>
</div>

@endsection
