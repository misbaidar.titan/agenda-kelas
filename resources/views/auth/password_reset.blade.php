@extends('layouts.auth')
@section('title', 'Reset Password | Agenda Kelas')
@section('reset')
<div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
    <img src="{{ asset('images/agendalogodefault.svg') }}" class="fill-white w-32 mx-auto mb-4" alt="agenda kelas digital" style="user-drag: none; -webkit-user-drag: none;">
    @if($errors->any())
        <div class="w-full sm:max-w-md mb-2 message-component flex justify-between bg-red-100 border border-red-400 text-red-700 px-2 py-3 rounded relative" role="alert">
            <div>
                <strong class="font-bold">Oops!</strong>
                <span class="block sm:inline">{{ $errors->first() }}</span>
            </div>
            <div class="flex items-center">
                <button type="button" onclick="this.closest('.message-component').remove()">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                    </svg>
                </button>
            </div>
        </div>
    @endif
    @if (session('message'))
        <div class="w-full sm:max-w-md mb-2 message-component flex justify-between bg-green-100 border border-green-400 text-green-700 px-2 py-3 rounded relative" role="alert">
            <div>
                <span class="block sm:inline">{{ session('message') }}</span>
            </div>
            <div class="flex items-center">
                <button type="button" onclick="this.closest('.message-component').remove()">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-green-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                    </svg>
                </button>
            </div>
        </div>
    @endif
    <form action="{{ route('password.update') }}" method="post">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        <input type="hidden" name="email" value="{{ $email }}">
        <div class="mb-4">
            <label for="password" class="block font-medium text-sm text-gray-700">Password Baru</label>
            <input type="password" name="password" class="block mt-1 w-full input-text-primary">
        </div>
        <div>
            <label for="password_confirmation" class="block font-medium text-sm text-gray-700">Konfirmasi Password</label>
            <input type="password" name="password_confirmation" class="block mt-1 w-full input-text-primary" required>
        </div>
        <div class="mt-4 self-center">
            <button type="submit" class="btn-primary w-full justify-center">Reset Password</button>
        </div>
    </form>
</div>
@endsection
