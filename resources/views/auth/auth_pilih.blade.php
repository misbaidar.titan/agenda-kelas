@extends('layouts.auth')
@section('title', 'Autentikasi | Agenda Kelas')
@section('auth_pilih')
<div class="w-full sm:max-w-md object-center mt-6">
    <div class="flex items-center">
      <a class="mx-auto mr-2" href="/register"><button class="btn-primary">Register</button></a>
      <a class="mx-auto ml-2" href="/login"><button class=" btn-primary ml-2">login</button></a>
    </div>
  </div>

@endsection
