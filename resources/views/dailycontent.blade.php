@extends('layouts.app')
@section('title', 'Isi Agenda Harian | Agenda Kelas')
@section('dailycontent')
    <style>
        .image-hover {
            display: none;
        }

        .a-hover:hover + .image-hover {
            display: block;
        }

    </style>
    <div class="py-6">
        <div class="max-w-6xl mx-auto pl-4 sm:px-6 lg:px-8 space-y-6">
            <nav class="flex items-center sm:pl-8 text-gray-500 text-lg font-medium" aria-label="Breadcrumb">
                <ol class="flex items-center space-x-2">
                    <li>
                        <a href="{{ route('class.show', $id) }}" class="hover:text-gray-900">Kelas</a>
                    </li>
                    <li>
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M5.707 14.707a1 1 0 1 1-1.414-1.414L7.586 10 4.293 6.707a1 1 0 1 1 1.414-1.414l4 4a1 1 0 0 1 0 1.414l-4 4z" clip-rule="evenodd" />
                        </svg>
                    </li>
                    @if(Auth::user()->hasRole('admin'))
                    <li>
                        <a href="{{ route('daily-agenda.show', [$id, $idClass]) }}" class="hover:text-gray-900">Tanggal agenda guru</a>
                    </li>
                    <li>
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M5.707 14.707a1 1 0 1 1-1.414-1.414L7.586 10 4.293 6.707a1 1 0 1 1 1.414-1.414l4 4a1 1 0 0 1 0 1.414l-4 4z" clip-rule="evenodd" />
                        </svg>
                    </li>
                    @endif
                    <li>
                        <a href="{{ route('daily-content.show', [$id, $idClass, $idDay]) }}" class="text-gray-900">Isi agenda guru</a>
                    </li>
                </ol>
            </nav>
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <div>
                    <div class="border-b pb-3 mb-4 border-gray-400">
                        @php
                            $day = \App\Models\DailyAgenda::find($idDay);
                        @endphp
                        @if($day)
                            <h2 class="text-lg font-medium text-gray-900">
                                {{ __('Isi agenda guru kelas ') }}{{ $day->class->class_name }}{{ __(' hari ') }}{{ $day->day }}{{ __(' tanggal ') }}{{ $day->date }}
                            </h2>
                        @endif
                    </div>
                    @if(Auth::user()->hasRole('teacher'))
                        <div align="right"><a href="{{ route('daily-content.create', [$id, $idClass, $idDay]) }}"><button class="btn-primary mb-4">tambah</button></a></div>
                    @endif
                    <div class="flex flex-col">
                        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                                <div class="shadow overflow-hidden border border-gray-400 sm:rounded-lg">
                                    <table class="min-w-full text-xs divide-y divide-gray-400">
                                        <thead class="bg-gray-100 text-left border-b border-gray-400 font-medium uppercase tracking-wider">
                                            <tr class="">
                                            <th class="px-2 py-3 w-1">Jam ke</th>
                                            <th class="px-2 py-3 ">Mata Pelajaran</th>
                                            <th class="px-2 py-3 ">Kompetensi Dasar/Indikator</th>
                                            <th class="px-2 py-3 ">Kegiatan Pembelajaran</th>
                                            <th class="px-2 py-3 ">Keterangan</th>
                                            <th class="px-2 py-3 ">Bukti Foto</th>
                                            <th class="px-2 py-3 ">Nama Guru</th>
                                            <th class=""></th>
                                            </tr>
                                        </thead>
                                        <tbody class="bg-white divide-y divide-gray-400 text-gray-700">
                                            @foreach ($dailycontents as $dailycontent)
                                                <tr>
                                                    <!--<td @if (Auth::user()->hasRole('admin') || (Auth::user()->teacher && $dailycontent->teacher->id == Auth::user()->teacher->id)) class="px-2">
                                                        <div class="flex items-center">
                                                            <input type="checkbox" name="selected[]" value="{{ $dailycontent->id }}" class="form-checkbox w-4 h-4 text-gray-600">
                                                        </div>
                                                        @endif
                                                    </td>-->
                                                    <td class="px-2 py-3 text-center    ">{{ $dailycontent->lesson_hours }}</td>
                                                    <td class="px-2 py-3">{{ $dailycontent->subject->subject_name }}</td>
                                                    <td class="px-2 py-3">{{ $dailycontent->indicator }}</td>
                                                    <td class="px-2 py-3">{{ $dailycontent->learning_activities }}</td>
                                                    <td class="px-2 py-3">{{ $dailycontent->description }}</td>
                                                    <td class="px-2 py-3">
                                                        @if ($dailycontent->confirmation_image)
                                                            <a href="{{ asset('storage/confirmation_images/'.$dailycontent->confirmation_image) }}" target="_blank" class="a-hover btn-primary px-1 py-1">
                                                                <img src="{{ asset('images/img.svg') }}" alt="Confirmation Image" width="20px">
                                                            </a>
                                                            <div class="image-hover hidden group-hover:block absolute z-10 mt-3 rounded-md bg-white p-2 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                                                                <img src="{{ asset('storage/confirmation_images/'.$dailycontent->confirmation_image) }}" alt="Confirmation Image Preview" width="200px">
                                                            </div>
                                                        @else
                                                            N/A
                                                        @endif
                                                    </td>
                                                    <td class="px-2 py-3">{{ $dailycontent->teacher->teacher_name }}</td>
                                                    <!--<td @if (Auth::user()->hasRole('admin') || (Auth::user()->teacher && $dailycontent->teacher->id == Auth::user()->teacher->id)) class="px-2 py-3">
                                                        <button class="p-2 btn-primary flex items-center justify-center" onclick="editIsi('{{ $dailycontent->id }}')">
                                                            <object data="{{ asset('images/edit.svg') }}" type="image/svg+xml" class="pointer-events-none" height="15px" width="15px">
                                                                <img src="fallback-image.png" alt="pilih">
                                                            </object>
                                                        </button>
                                                        @endif
                                                    </td>-->
                                                </tr>
                                                <tr id="edit-row-{{ $dailycontent->id }}" class="edit-row hidden">
                                                    <td colspan="9" class="px-6 py-4">
                                                    <div>
                                                        <form id="edit-form-{{ $dailycontent->id }}" action="{{ route('daily-content.update', [$id, $idClass, 'idContent' => $dailycontent->id]) }}" method="POST" enctype="multipart/form-data">
                                                            @csrf
                                                            @method('PUT')
                                                            <div class="mb-1">
                                                                <label for="lesson_hours" class="block text-gray-700 font-bold mb-2">Jam Pelajaran ke</label>
                                                                <div class="relative">
                                                                    <select name="lesson_hours" id="lesson_hours" class="w-full py-2 input-text-primary" style="-webkit-appearance: none;">
                                                                        @for ($j = 1; $j <= 10; $j++)
                                                                            @php
                                                                                $isHourOccupied = in_array($j, $dailycontents->where('daily_agenda_id', $idDay)->pluck('lesson_hours')->toArray());
                                                                            @endphp

                                                                            @if ($j == $dailycontent->lesson_hours)
                                                                                <option value="{{ $j }}" selected>{{ $j }}</option>
                                                                            @elseif (!$isHourOccupied)
                                                                                <option value="{{ $j }}">{{ $j }}</option>
                                                                            @endif
                                                                        @endfor
                                                                    </select>
                                                                    <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                                                                        <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                                                        </svg>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="mb-1">
                                                                <label for="subject_study" class="block text-gray-700 font-bold mb-2">Mata Pelajaran</label>
                                                                <div class="relative">
                                                                    <select class="w-full py-2 input-text-primary" style="-webkit-appearance: none;" id="subject_study" name="subject_study">
                                                                        @php
                                                                        $teacher = App\Models\DailyAgendaContent::findOrFail($dailycontent->id);
                                                                        $subjects = $teacher->teacher->subject;
                                                                        @endphp

                                                                        @foreach ($subjects as $subject)
                                                                            <option value="{{ $subject->id }}" {{ $dailycontent->subject_id == $subject->id ? 'selected' : '' }}>
                                                                                {{ $subject->subject_name }}
                                                                            </option>
                                                                        @endforeach

                                                                    </select>
                                                                    <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                                                                        <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                                                        </svg>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="mb-1">
                                                                <label for="indicator" class="block text-gray-700 font-bold mb-2">Indikator</label>
                                                                <input type="text" name="indicator" id="indicator" class="w-full input-text-primary" value="{{ $dailycontent->indicator }}">
                                                            </div>
                                                            <div class="mb-1">
                                                                <label for="learning_activities" class="block text-gray-700 font-bold mb-2">Kegiatan Pembelajaran</label>
                                                                <textarea name="learning_activities" id="learning_activities" class="w-full input-text-primary" rows="4">{{ $dailycontent->learning_activities }}</textarea>
                                                            </div>
                                                            <div class="mb-1">
                                                                <label for="description" class="block text-gray-700 font-bold mb-2">Deskripsi Kegiatan</label>
                                                                <textarea name="description" id="description" class="w-full input-text-primary" rows="4">{{ $dailycontent->description }}</textarea>
                                                            </div>
                                                            <div class="mb-4">
                                                                <label for="confirmation_image" class="block text-gray-700 font-bold mb-2">Bukti Foto</label>
                                                                @if ($dailycontent->confirmation_image)
                                                                    <img src="{{ asset('storage/confirmation_images/'.$dailycontent->confirmation_image) }}" alt="Confirmation Image" width="200px">
                                                                    <div class="my-2 flex items-center space-x-1">
                                                                        <input type="checkbox" name="remove_confirmation_image" id="remove_confirmation_image" class="hover:cursor-pointer" value="1">
                                                                        <label for="remove_confirmation_image" class="text-sm text-gray-700 hover:cursor-pointer">Hapus foto saat ini</label>
                                                                    </div>
                                                                @endif

                                                                <input type="file" name="confirmation_image" id="confirmation_image" accept="image/*" class="input-text-primary hover:cursor-pointer" accept="image/*">
                                                            </div>
                                                            <div class="flex items-center justify-end">
                                                                <button class="btn-secondary mr-6" type="button" onclick="cancelEdit(this)">
                                                                Batal
                                                                </button>
                                                                <button class="btn-primary" type="submit">
                                                                Simpan
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <form id="delete-form" action="{{ route('daily-content.delete-selected', [$id, $idClass, $idDay]) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            <tr id="delete-row" class="hidden">
                                                <td colspan="9" class="px-2 py-4 text-center">
                                                    <button type="submit" form="delete-form" class="btn-primary">Hapus</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <div>
                    <div class="border-b pb-3 mb-4 border-gray-400">
                        @php
                            $day = \App\Models\DailyAgenda::find($idDay);
                        @endphp
                        @if($day)
                            <h2 class="text-lg font-medium text-gray-900">
                                {{ __('Absensi siswa ') }}{{ $day->class->class_name }}{{ __(' hari ') }}{{ $day->day }}{{ __(' tanggal ') }}{{ $day->date }}
                            </h2>
                        @endif
                    </div>
                    @if(Auth::user()->hasRole('teacher'))
                        <div align="right"><a href="{{ route('daily-content.createAbsent', [$id, $idClass, $idDay]) }}"><button class="btn-primary mb-4">tambah</button></a></div>
                    @endif
                    <div class="flex flex-col">
                        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                                <div class="shadow overflow-hidden border border-gray-400 sm:rounded-lg">
                                    <table class="min-w-full divide-y divide-gray-400">
                                        <thead class="bg-gray-100 text-left border-b border-gray-400 text-sm font-medium uppercase tracking-wider">
                                            <tr>
                                                <th class="px-6 py-3">NIS</th>
                                                <th class="px-6 py-3 w-3/4">Nama Siswa</th>
                                                <th class="px-6 py-3 w-1/4">Alasan</th>
                                            </tr>
                                        </thead>
                                        <tbody class="bg-white divide-y divide-gray-400 text-gray-700">
                                            @foreach ($absentStudent as $a)
                                            <tr>
                                                <!--<td class="pl-5">
                                                    <div class="flex items-center">
                                                        <input type="checkbox" name="selectedAbsent[]" value="{{ $a->id }}" class="form-checkbox w-4 h-4 text-gray-600">
                                                    </div>
                                                </td>-->
                                                <td class="px-6 py-4">{{ $a->student->student_kode }}</td>
                                                <td class="px-6 py-4">{{ $a->student->student_name }}</td>
                                                <td class="px-6 py-4">{{ $a->reason }}</td>
                                                <!--<td class="pr-5 py-4">
                                                    <button class="p-2 btn-primary flex items-center justify-center" onclick="editIsiAbsent('{{ $a->id }}')">
                                                        <object data="{{ asset('images/edit.svg') }}" type="image/svg+xml" class="pointer-events-none" height="15px" width="15px">
                                                            <img src="fallback-image.png" alt="pilih">
                                                        </object>
                                                    </button>
                                                </td>-->
                                            </tr>
                                            <tr id="editAbsent-row-{{ $a->id }}" class="editAbsent-row- hidden">
                                                <td colspan="9" class="px-6 py-4">
                                                <div>
                                                    <form id="editAbsent-form-{{ $a->id }}" action="{{ route('daily-content.updateAbsent', [$id, $idClass, $a->id]) }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        @method('PUT')
                                                        <div class="mb-1">
                                                            <label for="student_name" class="block text-gray-700 font-bold mb-2">Nama Siswa</label>
                                                            <div class="relative">
                                                                <select class="w-full py-2 input-text-primary" style="-webkit-appearance: none;" id="student_name" name="student_name">
                                                                    @foreach ($students as $student)
                                                                    <option value="{{ $student->student_name }}" {{ $a->student_name == $student->student_name ? 'selected' : '' }}>
                                                                        {{ $student->student_name }}
                                                                    </option>
                                                                    @endforeach
                                                                </select>
                                                                <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                                                                    <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                                                    </svg>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mb-4">
                                                            <label for="reason" class="block text-gray-700 font-bold mb-2">Alasan</label>
                                                            <div class="relative">
                                                                <select class="w-full py-2 input-text-primary" style="-webkit-appearance: none;" id="reason" name="reason">
                                                                @php
                                                                    $reason = $a->reason;
                                                                @endphp

                                                                <option value="sakit" {{ $reason == 'sakit' ? 'selected' : '' }}>Sakit</option>
                                                                <option value="izin" {{ $reason == 'izin' ? 'selected' : '' }}>Izin</option>
                                                                <option value="alpa" {{ $reason == 'alpa' ? 'selected' : '' }}>Alpa</option>
                                                                </select>
                                                                <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                                                                    <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                                                    </svg>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="flex items-center justify-end">
                                                            <button class="btn-secondary mr-6" type="button" onclick="cancelEditAbsent(this)">
                                                            Batal
                                                            </button>
                                                            <button class="btn-primary" type="submit">
                                                            Simpan
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                            <form id="delete-formAbsent" action="{{ route('daily-content.delete-selectedAbsent', [$id, $idClass, $idDay]) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            <tr id="delete-rowAbsent" class="hidden">
                                                <td colspan="4" class="px-2 py-4 text-center">
                                                    <button type="submit" form="delete-formAbsent" class="btn-primary">Hapus</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function editIsi(id) {
          document.querySelectorAll('[id^="edit-row-"]').forEach(row => {
            row.classList.add('hidden');
          });
          document.getElementById('edit-row-' + id).classList.remove('hidden');
        }

        function cancelEdit(button) {
        if (!button) {
            console.error('Button parameter is null');
            return;
        }
        var editRow = button.closest('.edit-row');
        if (!editRow) {
            console.error('Edit row not found');
            return;
        }
        editRow.classList.add('hidden');
        location.reload();
        }
        function editIsiAbsent(id) {
          document.querySelectorAll('[id^="editAbsent-row-"]').forEach(row => {
            row.classList.add('hidden');
          });
          document.getElementById('editAbsent-row-' + id).classList.remove('hidden');
        }

        function cancelEditAbsent(button) {
        if (!button) {
            console.error('Button parameter is null');
            return;
        }
        var editRow = button.closest('.editAbsent-row');
        if (!editRow) {
            console.error('Edit row not found');
            return;
        }
        editRow.classList.add('hidden');
        location.reload();
        }
    </script>
    <script>
        const form = document.getElementById('delete-form');
        const dataList = document.getElementsByName('selected[]');
        const deleteRow = document.getElementById('delete-row');
        const deleteBtn = deleteRow.querySelectorAll('button')[0];

        dataList.forEach((checkbox) => {
        checkbox.addEventListener('change', () => {
            const checkedBoxes = document.querySelectorAll('input[name="selected[]"]:checked');
            if (checkedBoxes.length > 0) {
            deleteRow.classList.remove('hidden');
            const lastCheckedRow = checkedBoxes[checkedBoxes.length - 1].closest('tr');
            deleteRow.parentNode.insertBefore(deleteRow, lastCheckedRow.nextSibling);
            checkbox.closest('tr').classList.toggle('bg-red-100');
            } else {
            deleteRow.classList.add('hidden');
            checkbox.closest('tr').classList.remove('bg-red-100');
            }
        });
        });
        deleteBtn.addEventListener('click',  () => {
            const checkedBoxes = document.querySelectorAll('input[name="selected[]"]:checked');
            form.querySelectorAll('input[name="selected[]"]').forEach(c => c.remove())
            checkedBoxes.forEach((checkBox) => {
                form.appendChild(checkBox.cloneNode())
            })
        })

        const formAbsent = document.getElementById('delete-formAbsent');
        const dataListAbsent = document.getElementsByName('selectedAbsent[]');
        const deleteRowAbsent = document.getElementById('delete-rowAbsent');
        const deleteBtnAbsent = deleteRowAbsent.querySelectorAll('button')[0];

        dataListAbsent.forEach((checkbox) => {
        checkbox.addEventListener('change', () => {
            const checkedBoxesAbsent = document.querySelectorAll('input[name="selectedAbsent[]"]:checked');
            if (checkedBoxesAbsent.length > 0) {
            deleteRowAbsent.classList.remove('hidden');
            const lastCheckedRowAbsent = checkedBoxesAbsent[checkedBoxesAbsent.length - 1].closest('tr');
            deleteRowAbsent.parentNode.insertBefore(deleteRowAbsent, lastCheckedRowAbsent.nextSibling);
            checkbox.closest('tr').classList.toggle('bg-red-100');
            } else {
            deleteRowAbsent.classList.add('hidden');
            checkbox.closest('tr').classList.remove('bg-red-100');
            }
        });
        });
        deleteBtnAbsent.addEventListener('click',  () => {
            const checkedBoxesAbsent = document.querySelectorAll('input[name="selectedAbsent[]"]:checked');
            formAbsent.querySelectorAll('input[name="selectedAbsent[]"]').forEach(c => c.remove())
            checkedBoxesAbsent.forEach((checkBox) => {
                formAbsent.appendChild(checkBox.cloneNode())
            })
        })
    </script>
@endsection
