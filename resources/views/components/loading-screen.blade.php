<div id="loading-screen" class="fixed inset-0 flex items-center justify-center bg-white opacity-95 z-50">
    <div class="inline-block h-8 w-8 animate-spin rounded-full border-4 border-solid border-current border-r-transparent align-[-0.125em] motion-reduce:animate-[spin_1.5s_linear_infinite]"></div>
</div>
