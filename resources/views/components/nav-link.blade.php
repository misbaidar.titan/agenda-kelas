<a href="{{ $url }}"
    class="inline-flex items-center px-1 py-4 gap-1 {{ $active ? 'border-b-2 border-gray-900 text-gray-900 focus:border-gray-900' : 'border-b-2 border-transparent hover:border-b-2 text-gray-500 hover:text-gray-700 hover:border-gray-700 focus:text-gray-900 focus:border-gray-900' }} text-sm font-medium focus:outline-none transition-colors duration-200 ease-in-out">
    {!! $icon !!}
    {{ $slot }}
</a>
