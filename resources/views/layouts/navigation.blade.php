<style>
    .hover-container:hover img {
    border-color: black;
    }
</style>
<header class="fixed flex flex-wrap sm:justify-start sm:flex-nowrap z-50 w-full bg-white shadow-md rounded-md text-sm py-4">
  <nav class="max-w-[85rem] w-full mx-auto px-4 sm:flex sm:items-center sm:justify-between" aria-label="Global">
    <div class="flex items-center justify-between">
      <div class="self-center mr-3 text-gray-800">
        @if (auth()->user()->hasRole('teacher'))
            <a href="{{ route('dashboard-guru.show', $id) }}" class="flex items-center space-x-2 pr-3">
                <img src="{{ asset('images/agendanondigital.svg') }}" class="w-16" alt="agenda logo" style="user-drag: none; -webkit-user-drag: none;">
        @else
            <a href="{{ route('dashboard.show', $id) }}" class="flex items-center space-x-2 pr-3" style="user-drag: none; -webkit-user-drag: none;">
                <img src="{{ asset('images/agendanondigital.svg') }}" class="w-16" alt="agenda logo">
        @endif
                <p class="text-xs">{{ Auth::user()->school_name }}</p>
            </a>
      </div>
      <div class="sm:hidden">
        <button type="button" class="hs-collapse-toggle p-2 inline-flex justify-center items-center gap-2 rounded-md border font-medium bg-white text-gray-700 shadow-sm align-middle hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-white focus:ring-blue-600 transition-all text-sm" data-hs-collapse="#navbar-collapse-basic" aria-controls="navbar-collapse-basic" aria-label="Toggle navigation">
          <svg class="hs-collapse-open:hidden w-4 h-4" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
          </svg>
          <svg class="hs-collapse-open:block hidden w-4 h-4" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
          </svg>
        </button>
      </div>
    </div>
    <div id="navbar-collapse-basic" class="hs-collapse hidden overflow-hidden transition-all duration-300 basis-full grow sm:block sm:w-auto sm:basis-auto">
      <div class="flex flex-col gap-5 mt-5 sm:flex-row sm:items-center sm:justify-between sm:mt-0">
        <div class="hs-dropdown flex order-last [--placement:bottom-right]">
            <button id="hs-dropdown-unstyled" type="button" class="hs-dropdown-toggle hover-container relative flex cursor-pointer items-center">
                <div class="mr-3 text-gray-800">
                    @if (Auth::user()->hasRole('teacher'))
                    @if (Auth::user()->teacher)
                        <h1 class="text-xl font-bold">{{ Auth::user()->teacher->teacher_name }}</h1>
                        <p class="text-sm text-right">Guru</p>
                    @else
                        <h1 class="text-xl font-bold">Teacher not found</h1>
                        <p class="text-sm text-right">Guru</p>
                    @endif
                    @else
                        <h1 class="text-xl font-bold">{{ Auth::user()->name }}</h1>
                        <p class="text-sm text-right">Pengelola</p>
                    @endif
                    </div>
                    @if (Auth::user()->picture)
                    <img src="{{ asset('storage/pictures/'.Auth::user()->picture) }}" class="fill-current text-gray-300 inline-block rounded-full object-cover h-14 w-14 overflow-hidden border-transparent border-2 transition-colors duration-200" alt="Default Profile Photo">
                    @else
                    <img src="{{ asset('images/default-profile.svg') }}" alt="Default Profile Photo" class="fill-current text-gray-300 inline-block rounded-full object-cover h-14 w-14 overflow-hidden border-transparent border-2 transition-colors duration-200">
                    @endif
            </button>

            <div class="hs-dropdown-menu transition-[opacity,margin] duration hs-dropdown-open:opacity-100 opacity-0 z-10 mt-3 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none hidden" aria-labelledby="hs-dropdown-unstyled">
                <form action="{{ route('logout') }}" method="POST">
                @csrf
                    <button class="block text-left w-full px-4 py-2 text-sm hover:bg-gray-100 text-gray-700" type="submit">Log out</button>
                </form>
            </div>
        </div>
        <div class="flex flex-col gap-5 mt-5 sm:flex-row sm:items-center sm:justify-between sm:mt-0">
            @if (Auth::user()->hasRole('admin'))
            <a href="{{ route('dashboard.show', ['id' => $id]) }}" class="flex items-center font-medium {{ request()->routeIs('dashboard.show') ? 'text-black fill-black font-bold' : 'text-gray-500 fill-gray-500' }} hover:text-gray-700 hover:fill-gray-700 hover:border-gray-700 focus:text-gray-900 focus:fill-gray-900 focus:border-gray-900" href="#" aria-current="page">
                <svg xmlns="http://www.w3.org/2000/svg" class="mr-1 w-4" viewBox="0 0 576 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M575.8 255.5c0 18-15 32.1-32 32.1h-32l.7 160.2c.2 35.5-28.5 64.3-64 64.3H128.1c-35.3 0-64-28.7-64-64V287.6H32c-18 0-32-14-32-32.1c0-9 3-17 10-24L266.4 8c7-7 15-8 22-8s15 2 21 7L564.8 231.5c8 7 12 15 11 24zM352 224a64 64 0 1 0 -128 0 64 64 0 1 0 128 0zm-96 96c-44.2 0-80 35.8-80 80c0 8.8 7.2 16 16 16H384c8.8 0 16-7.2 16-16c0-44.2-35.8-80-80-80H256z"/></svg>
                Dashboard
            </a>
            <a href="{{ route('schedule.timetable', ['id' => $id]) }}" class="flex items-center font-medium {{ request()->routeIs('schedule.timetable', 'schedule.show', 'schedule.create', 'schedule.search',) ? 'text-black fill-black font-bold' : 'text-gray-500 fill-gray-500' }} hover:text-gray-700 hover:fill-gray-700 hover:border-gray-700 focus:text-gray-900 focus:fill-gray-900 focus:border-gray-900" href="#" aria-current="page">
                <svg xmlns="http://www.w3.org/2000/svg" class="mr-1 w-3" viewBox="0 0 448 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M96 32V64H48C21.5 64 0 85.5 0 112v48H448V112c0-26.5-21.5-48-48-48H352V32c0-17.7-14.3-32-32-32s-32 14.3-32 32V64H160V32c0-17.7-14.3-32-32-32S96 14.3 96 32zM448 192H0V464c0 26.5 21.5 48 48 48H400c26.5 0 48-21.5 48-48V192z"/></svg>
                Jadwal
            </a>
            <div class="hs-dropdown [--strategy:static] sm:[--strategy:absolute] [--adaptive:none]">
                <button type="button" class="flex items-center w-full font-medium {{ request()->routeIs(
                    'class.*',
                    'daily-agenda.*',
                    'daily-content.*',
                    'teachers.*',
                    'subject.*',
                    'student.*',
                ) ? 'text-black fill-black font-bold' : 'text-gray-500 fill-gray-500' }} hover:text-gray-700 hover:fill-gray-700 hover:border-gray-700 focus:text-gray-900 focus:fill-gray-900 focus:border-gray-900">
                    <svg xmlns="http://www.w3.org/2000/svg" class="mr-1 w-3" viewBox="0 0 448 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M448 80v48c0 44.2-100.3 80-224 80S0 172.2 0 128V80C0 35.8 100.3 0 224 0S448 35.8 448 80zM393.2 214.7c20.8-7.4 39.9-16.9 54.8-28.6V288c0 44.2-100.3 80-224 80S0 332.2 0 288V186.1c14.9 11.8 34 21.2 54.8 28.6C99.7 230.7 159.5 240 224 240s124.3-9.3 169.2-25.3zM0 346.1c14.9 11.8 34 21.2 54.8 28.6C99.7 390.7 159.5 400 224 400s124.3-9.3 169.2-25.3c20.8-7.4 39.9-16.9 54.8-28.6V432c0 44.2-100.3 80-224 80S0 476.2 0 432V346.1z"/></svg>
                    Bank Data
                    <svg class="ml-2 w-2.5 h-2.5" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M2 5L8.16086 10.6869C8.35239 10.8637 8.64761 10.8637 8.83914 10.6869L15 5" stroke="currentColor" stroke-width="2" stroke-linecap="round"></path>
                    </svg>
                </button>

            <div class="hs-dropdown-menu transition-[opacity,margin] sm:border duration-[0.1ms] sm:duration-[150ms] hs-dropdown-open:opacity-100 sm:border-t border-gray-400 opacity-0 hidden w-full z-10 top-full left-0 min-w-[15rem] bg-white sm:shadow-md rounded-lg">
                <div class="sm:flex sm:justify-center">
                    <div class="px-4">
                        @include('components.nav-link', [
                                    'url' => route('class.show', $id),
                                    'active' => request()->routeIs(
                                        'class.*',
                                        'daily-agenda.*',
                                        'daily-content.*',
                                        ),
                                    'slot' => __('Data Kelas'),
                                    'icon' => '',
                                ])
                    </div>

                    <div class="px-4">
                        @if(Auth::user()->hasRole('admin'))
                            @include('components.nav-link', [
                                'url' => route('teachers.showGuru', $id),
                                'active' => request()->routeIs('teachers.*', 'subject.*'),
                                'slot' => __('Data Guru'),
                                'icon' => '',
                            ])
                        @endif
                    </div>

                    <div class="px-4 sm:border-r border-0 border-gray-400">
                        @if(Auth::user()->hasRole('admin'))
                            @include('components.nav-link', [
                                'url' => route('student.show', $id),
                                'active' => request()->routeIs('student.*'),
                                'slot' => __('Data Siswa'),
                                'icon' => '',
                            ])
                        @endif
                    </div>

                    <div class="px-4">
                        @if(Auth::user()->hasRole('admin'))
                            @include('components.nav-link', [
                                'url' => route('database.dump'),
                                'active' => request()->routeIs('database.dump'),
                                'slot' => __('Backup Database'),
                                'icon' => '<svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512" fill="currentColor" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" ><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M288 32c0-17.7-14.3-32-32-32s-32 14.3-32 32V274.7l-73.4-73.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l128 128c12.5 12.5 32.8 12.5 45.3 0l128-128c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L288 274.7V32zM64 352c-35.3 0-64 28.7-64 64v32c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V416c0-35.3-28.7-64-64-64H346.5l-45.3 45.3c-25 25-65.5 25-90.5 0L165.5 352H64zm368 56a24 24 0 1 1 0 48 24 24 0 1 1 0-48z"/></svg>',
                            ])
                        @endif
                    </div>

                </div>
            </div>
          </div>
          @endif
        </div>
      </div>
    </div>
  </nav>
</header>
<div class="w-full py-11"></div>
