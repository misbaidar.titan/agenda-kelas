<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body class="font-sans antialiased bg-gray-100">
        <x-loading-screen />
        <div class="min-h-screen">
            @include('layouts.navigation')

            <!-- Page Content -->
            <main>
                @forelse ([ 'guru',
                            'guruTambah',
                            'mapel',
                            'mapelTambah',
                            'classes',
                            'classesTambah',
                            'dailyagenda',
                            'dailyagendaTambah',
                            'dailycontent',
                            'agendacontentTambah',
                            'absentTambah',
                            'student',
                            'studentTambah',
                            'schedule',
                            'scheduleTambah',
                            'timeTable',
                            'dashboard',
                            'reportGuru',
                            'dashboardGuru',
                            'reportStudent',
                            'logs',
                            ]
                            as $yieldName)
                    @yield($yieldName)
                @empty
                    <div class="alert alert-info">
                        There are no sections to display.
                    </div>
                @endforelse
            </main>
        </div>
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                document.querySelector('#loading-screen').style.display = 'none';
            });
        </script>
    </body>
</html>
