@extends('layouts.app')
@section('title', "Event Logs | Agenda Kelas")
@section('logs')
<div class="py-6">
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8 space-y-6">
        <div class="p-4 sm:p-8 bg-white overflow-auto shadow sm:rounded-lg">
            <div>
                <div class="border-b pb-3 mb-4 border-gray-400">
                    <h2 class="text-lg font-medium text-gray-900">
                        {{ __('Event Logs') }}
                    </h2>
                </div>
                <div class="flex flex-col">
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="shadow overflow-hidden border border-gray-400 sm:rounded-lg">
                                <table class="min-w-full divide-y divide-gray-400">
                                    <thead class="bg-gray-100 text-left border-b border-gray-400 text-sm font-medium uppercase tracking-wider">
                                        <tr class="whitespace-nowrap">
                                            <th class="px-2 py-4 text-center"></th>
                                            <th class="px-2 py-3">Tabel</th>
                                            <th class="px-6 py-3">Event</th>
                                            <th class="px-6 py-3">Nilai Lama</th>
                                            <th class="px-6 py-3">Nilai Baru</th>
                                            <th class="px-6 py-3">Waktu</th>
                                        </tr>
                                    </thead>
                                    <tbody class="bg-white divide-y divide-gray-400 text-gray-700">
                                        @foreach($logs as $log)
                                            <tr>
                                                <td class="px-2 py-1 text-center border-r border-gray-400">{{ $loop->iteration + $logs->firstItem() - 1 }}</td>
                                                <td class="px-2 py-1">{{ $log->table_name }}</td>
                                                <td class="px-6 py-1">{{ $log->event }}</td>
                                                <td class="px-6 py-1">{{ $log->old_values ?? '-'  }}</td>
                                                <td class="px-6 py-1">{{ $log->new_values ?? '-'  }}</td>
                                                <td class="px-6 py-1">{{ $log->created_at }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-4">
                              {{ $logs->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
