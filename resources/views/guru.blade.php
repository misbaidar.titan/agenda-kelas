@extends('layouts.app')
@section('title', "Guru | Agenda Kelas")
@section('guru')
<div class="py-6">
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8 space-y-6">
        <nav class="flex items-center pl-4 sm:pl-8 text-gray-500 text-lg font-medium" aria-label="Breadcrumb">
            <ol class="flex items-center space-x-2">
            <li>
                <a href="{{ route('teachers.showGuru', $id) }}" class="text-gray-900">Guru</a>
            </li>
        </nav>
        <div class="p-4 sm:p-8 bg-white overflow-auto shadow sm:rounded-lg">
            @if (session('success'))
                <div class="fixed left-1/2 -translate-x-1/2 z-50 message-component flex bg-green-100 border border-green-400 text-green-700 px-2 py-3 shadow-lg rounded" role="alert">
                    <div>
                        <strong class="font-bold">Selamat!</strong>
                        <span class="block sm:inline mr-4">{{ session('success') }}</span>
                    </div>
                    <div class="flex items-center">
                        <button type="button" onclick="this.closest('.message-component').remove()">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-green-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                            </svg>
                        </button>
                    </div>
                </div>
            @endif
            @if($errors->any())
                <div class="fixed left-1/2 -translate-x-1/2 z-50 message-component flex bg-red-100 border border-red-400 text-red-700 px-2 py-3 shadow-lg rounded" role="alert">
                    <div>
                        <strong class="font-bold">Oops!</strong>
                        <span class="block sm:inline">{{ $errors->first() }}</span>
                    </div>
                    <div class="flex items-center">
                        <button type="button" onclick="this.closest('.message-component').remove()">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                            </svg>
                        </button>
                    </div>
                </div>
            @endif
            <div>
                <div class="border-b pb-3 mb-4 border-gray-400">
                    <h2 class="text-lg font-medium text-gray-900">
                        {{ __('Data guru ') }}{{ Auth::user()->school_name }}
                    </h2>
                </div>
                <div class="flex my-4 justify-between">
                    <form action="{{ route('teachers.cariGuru', ['id' => $id]) }}" method="GET">
                        <div class="flex space-x-2">
                            <div class="relative w-48">
                                <select class="w-full py-2 input-text-primary" style="-webkit-appearance: none;" name="subject_filter">
                                    <option value="" {{ empty(request('subject_filter')) ? 'selected' : '' }}>Semua mata pelajaran</option>
                                    @foreach ($subjects as $subject)
                                        <option value="{{ $subject->id }}" {{ request('subject_filter') == $subject->id || request('idSubject') == $subject->id ? 'selected' : '' }}>
                                            {{ $subject->subject_name }}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                                    <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                    </svg>
                                </div>
                            </div>
                            <div class="relative w-48">
                                <select class="w-full py-2 input-text-primary" style="-webkit-appearance: none;" name="class_filter">
                                    <option value="" {{ empty(request('class_filter')) ? 'selected' : '' }}>Semua kelas</option>
                                    @foreach ($classes as $class)
                                        <option value="{{ $class->id }}" {{ request('class_filter') == $class->id || request('idClass') == $class->id ? 'selected' : '' }}>
                                            {{ $class->class_name }}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                                    <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                    </svg>
                                </div>
                            </div>
                            <button type="submit" class="btn-primary p-2 ml-2 self-center">
                                <svg height="18px" width="18px" stroke-width='1.5' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
                                    <path d='M13.447 20.905A.988.988 0 0 0 14 20.02V14.7c0-.262.105-.514.293-.7l6.121-6.054c.375-.371.586-.875.586-1.4V3.99A.994.994 0 0 0 20 3H4c-.553 0-1 .442-1 .99v2.556c0 .525.211 1.029.586 1.4L9.707 14A.984.984 0 0 1 10 14.7v6.31c0 .735.782 1.213 1.447.884l2-.989Z' fill='#F7F7F7FF'/>
                                </svg>
                            </button>
                        </div>
                    </form>
                    <div class="flex-end relative self-center">
                        <a href="{{ route('subject.show', $id) }}" class="btn-primary mr-2">Data MaPel</a>
                        <button data-hs-overlay="#hs-import-modal" class="btn-primary mr-2">Import Excel</button>
                        <a href="{{ route('teachers.create', $id) }}">
                            <button class="btn-primary">tambah</button>
                        </a>
                    </div>
                    <div id="hs-import-modal" class="hs-overlay hidden w-full h-full fixed top-0 left-0 z-[60] overflow-x-hidden overflow-y-auto">
                        <div class="hs-overlay-open:mt-7 hs-overlay-open:opacity-100 hs-overlay-open:duration-500 mt-0 opacity-0 ease-out transition-all sm:max-w-sm sm:w-full m-3 sm:mx-auto min-h-[calc(100%-3.5rem)] flex items-center">
                            <div class="min-w-full bg-white border p-4 border-gray-200 rounded-lg shadow">
                                <div class="flex justify-end items-center">
                                  <button type="button" class="hs-dropdown-toggle inline-flex flex-shrink-0 justify-center items-center h-4 w-4 rounded-md text-gray-500 hover:text-gray-400 focus:outline-none focus:ring-2 focus:ring-gray-400 focus:ring-offset-2 focus:ring-offset-white transition-all text-sm" data-hs-overlay="#hs-import-modal">
                                    <span class="sr-only">Close</span>
                                    <svg class="w-3.5 h-3.5" width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                      <path d="M0.258206 1.00652C0.351976 0.912791 0.479126 0.860131 0.611706 0.860131C0.744296 0.860131 0.871447 0.912791 0.965207 1.00652L3.61171 3.65302L6.25822 1.00652C6.30432 0.958771 6.35952 0.920671 6.42052 0.894471C6.48152 0.868271 6.54712 0.854471 6.61352 0.853901C6.67992 0.853321 6.74572 0.865971 6.80722 0.891111C6.86862 0.916251 6.92442 0.953381 6.97142 1.00032C7.01832 1.04727 7.05552 1.1031 7.08062 1.16454C7.10572 1.22599 7.11842 1.29183 7.11782 1.35822C7.11722 1.42461 7.10342 1.49022 7.07722 1.55122C7.05102 1.61222 7.01292 1.6674 6.96522 1.71352L4.31871 4.36002L6.96522 7.00648C7.05632 7.10078 7.10672 7.22708 7.10552 7.35818C7.10442 7.48928 7.05182 7.61468 6.95912 7.70738C6.86642 7.80018 6.74102 7.85268 6.60992 7.85388C6.47882 7.85498 6.35252 7.80458 6.25822 7.71348L3.61171 5.06702L0.965207 7.71348C0.870907 7.80458 0.744606 7.85498 0.613506 7.85388C0.482406 7.85268 0.357007 7.80018 0.264297 7.70738C0.171597 7.61468 0.119017 7.48928 0.117877 7.35818C0.116737 7.22708 0.167126 7.10078 0.258206 7.00648L2.90471 4.36002L0.258206 1.71352C0.164476 1.61976 0.111816 1.4926 0.111816 1.36002C0.111816 1.22744 0.164476 1.10028 0.258206 1.00652Z" fill="currentColor"/>
                                    </svg>
                                  </button>
                                </div>
                                <label for="file" class="block text-gray-700 font-bold">Import Data Guru</label>
                                <form action="{{ route('teachers.import', $id) }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="flex space-x-2 my-2 items-center">
                                        <p class="text-sm text-gray-600">Download template excel terlebih dahulu dan gunakan file tersebut untuk import.
                                            <a href="{{ route('teachers.export', $id) }}" class="underline hover:cursor-pointer">download disini</a>
                                        </p>
                                        <div class="hs-tooltip inline-block">
                                            <button type="button" class="hs-tooltip-toggle">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="fill-gray-600 " height="1em" viewBox="0 0 384 512" fill="currentColor" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" ><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M192 0c-41.8 0-77.4 26.7-90.5 64H64C28.7 64 0 92.7 0 128V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V128c0-35.3-28.7-64-64-64H282.5C269.4 26.7 233.8 0 192 0zm0 64a32 32 0 1 1 0 64 32 32 0 1 1 0-64zM72 272a24 24 0 1 1 48 0 24 24 0 1 1 -48 0zm104-16H304c8.8 0 16 7.2 16 16s-7.2 16-16 16H176c-8.8 0-16-7.2-16-16s7.2-16 16-16zM72 368a24 24 0 1 1 48 0 24 24 0 1 1 -48 0zm88 0c0-8.8 7.2-16 16-16H304c8.8 0 16 7.2 16 16s-7.2 16-16 16H176c-8.8 0-16-7.2-16-16z"/></svg>
                                                <span class="hs-tooltip-content hs-tooltip-shown:opacity-100 hs-tooltip-shown:visible opacity-0 transition-opacity inline-block absolute invisible z-10 py-1 px-2 text-start bg-gray-900 text-xs font-medium text-white rounded shadow-sm dark:bg-slate-700" role="tooltip">
                                                    <ul>
                                                        <li>Kolom pertama: Kode (NUPTK)</li>
                                                        <li>Kolom kedua: Nama</li>
                                                        <li>Kolom ketiga: Mata Pelajaran</li>
                                                        <li>Kolom keempat: Email</li>
                                                        <li>Kolom kelima: Password</li>
                                                    </ul>
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                    <input type="file" name="file" accept=".xlsx, .xls" class="hover:cursor-pointer mb-2 input-text-primary">
                                    <div class="flex items-center justify-end">
                                        <button type="submit" class="btn-primary">Import</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flex mt-4 mb-4">
                    <div class="flex-1">
                        <form action="{{ route('teachers.cariGuru', $id) }}" method="GET">
                            <div class="relative">
                                <input type="text" name="search" placeholder="Cari guru berdasarkan nama atau kode" class="py-2 pl-10 min-w-full pr-4 border border-gray-400 rounded-md shadow-sm focus:outline-none">
                                <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                    <img src="{{ asset('images/search.svg') }}" alt="cari" width="16">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="my-4">
                    <p class="mt-1 text-sm text-gray-600">Total {{ $teachers->total() }} hasil</p>
                    @isset ($search)
                        <p class="mt-1 text-sm text-gray-600">Menampilkan hasil pencarian dari "{{ $search }}"</p>
                    @endisset
                </div>
                <div class="flex flex-col">
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="shadow overflow-hidden border border-gray-400 sm:rounded-lg">
                                <table class="min-w-full divide-y divide-gray-400">
                                    <thead class="bg-gray-100 text-left border-b border-gray-400 text-sm font-medium uppercase tracking-wider">
                                        <tr class="whitespace-nowrap">
                                            <th class="px-2 py-4 text-center"></th>
                                            <th class="px-2 py-4">
                                                <div class="flex items-center">
                                                    <input type="checkbox" id="select-all-checkbox" class="form-checkbox w-5 h-5 text-gray-600">
                                                </div>
                                            </th>
                                            <th class="px-2 py-3 w-1/12">Foto Profil</th>
                                            <th class="px-2 py-3 w-3/12">Kode</th>
                                            <!--<th class="px-2 py-3 w-3/12">Email</th>-->
                                            <th class="px-6 py-3 w-3/12">Nama</th>
                                            <th class="px-6 py-3 w-2/12">Mata Pelajaran</th>
                                            <th class="px-6 py-3 w-5/12">Kelas</th>
                                            <!--<th class="px-6 py-3 w-2/12">Password</th>-->
                                            <th class="px-6 py-3 w-1/12"></th>
                                            <th class="px-6 py-3 w-1/12"></th>
                                        </tr>
                                    </thead>
                                    <tbody class="bg-white divide-y divide-gray-400 text-gray-700">
                                        @foreach ($teachers as $teacher)
                                        <tr>
                                            <td class="px-2 py-4 text-center border-r border-gray-400">{{ $loop->iteration + $teachers->firstItem() - 1 }}</td>
                                            <td class="px-2 py-4">
                                                <div class="flex items-center">
                                                    <input type="checkbox" name="selected[]" value="{{ $teacher->kode }}" class="form-checkbox w-5 h-5 text-gray-600">
                                                </div>
                                            </td>
                                            <td class="px-2 py-3 text-center">
                                                @if ($teacher->picture)
                                                    <img src="{{ asset('storage/pictures/'.$teacher->picture) }}" class="inline rounded-full object-cover h-12 w-12 " alt="picture">
                                                @else
                                                <img src="{{ asset('images/default-profile.svg') }}" alt="Default Profile Photo" class="inline rounded-full object-cover h-12 w-12 ">
                                                @endif
                                            </td>
                                            <td class="px-2 py-4 relative">
                                                <div class="absolute inset-y-0 w-full pr-3 flex items-center">
                                                    <div class="whitespace-nowrap text-ellipsis overflow-hidden" title="{{ $teacher->kode }}">{{ $teacher->kode }}</div>
                                                </div>
                                            </td>
                                            <!--<td class="px-2 py-4 relative">
                                                <div class="absolute inset-y-0 w-full pr-3 flex items-center">
                                                    <div class="whitespace-nowrap text-ellipsis overflow-hidden" title="{{ $teacher->email }}">{{ $teacher->email }}</div>
                                                </div>
                                            </td>-->
                                            <td class="px-6 py-4 relative">
                                                <div class="absolute inset-y-0 w-full pr-3 flex items-center">
                                                    <div class="whitespace-nowrap text-ellipsis overflow-hidden" title="{{ $teacher->teacher_name }}">{{ $teacher->teacher_name }}</div>
                                                </div>
                                            </td>
                                            <td class="px-6 py-4 relative">
                                                <div class="absolute inset-y-0 w-full pr-3 flex items-center">
                                                    <div class="whitespace-nowrap overflow-x-auto">
                                                        @foreach (explode(',', $teacher->subject_names) as $subject)
                                                            @php
                                                                $subjectNameMatchesFilter = ($idSubjectName !== null ? ($subject == $idSubjectName) : ($subject == $subjectFilterName));
                                                            @endphp
                                                            <span class="inline-flex items-center rounded-md bg-gray-100 px-2 py-1 my-0.5 text-xs font-medium uppercase text-gray-700 ring-1 ring-inset ring-gray-500/40
                                                                {{ $subjectNameMatchesFilter ? 'underline' : '' }}
                                                                px-2 py-1 my-0.5 text-xs font-medium uppercase">
                                                                {{ $subject }}
                                                            </span>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="px-6 py-4 relative">
                                                <div class="absolute inset-y-0 w-4/6 flex items-center">
                                                    <div class="whitespace-nowrap overflow-x-auto">
                                                        @foreach (explode(',', $teacher->class_names) as $class)
                                                            @php
                                                                $classNameMatchesFilter = ($idClassName !== null ? ($class == $idClassName) : ($class == $classFilterName));
                                                            @endphp
                                                            <span class="inline-flex items-center rounded-md bg-gray-100 px-2 py-1 my-0.5 text-xs font-medium uppercase text-gray-700 ring-1 ring-inset ring-gray-500/40
                                                                {{ $classNameMatchesFilter ? 'underline' : '' }}
                                                                px-2 py-1 my-0.5 text-xs font-medium uppercase">
                                                                {{ $class }}
                                                            </span>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </td>
                                            <!--<td class="px-6 py-4">
                                                <div class="group">
                                                    <p id="star-password" class="group-hover:hidden">
                                                        {{ str_repeat('•', strlen($teacher->password)) }}
                                                    </p>
                                                    <p id="show-password" class="hidden group-hover:block">
                                                        {{ $teacher->password }}
                                                    </p>
                                                </div>
                                            </td>-->
                                            <td>
                                                <button class="p-2 btn-primary flex items-center justify-center" data-hs-overlay="#edit-row-{{ $teacher->kode }}">
                                                    <img src="{{ asset('images/edit.svg') }}" alt="edit" width="15px">
                                                </button>
                                            </td>
                                            <td>
                                                <button type="button" class="p-2 link bg-white items-center justify-center" data-hs-overlay="#hs-basic-modal{{ $teacher->id }}">
                                                    <img src="{{ asset('images/link.svg') }}" alt="edit" width="15px">
                                                </button>
                                                <div id="hs-basic-modal{{ $teacher->id }}" class="hs-overlay hidden w-full h-full fixed top-0 left-0 z-[60] overflow-x-hidden overflow-y-auto">
                                                    <div class="hs-overlay-open:mt-7 hs-overlay-open:opacity-100 hs-overlay-open:duration-500 mt-0 opacity-0 ease-out transition-all sm:max-w-lg sm:w-full m-3 sm:mx-auto">
                                                    <div class="p-6 bg-white border border-gray-200 rounded-lg shadow">
                                                        <div class="flex justify-end">
                                                            <button type="button" class="hs-dropdown-toggle inline-flex flex-shrink-0 justify-center items-center h-8 w-8 rounded-md text-gray-500 hover:text-gray-400 focus:outline-none focus:ring-2 focus:ring-gray-400 focus:ring-offset-2 focus:ring-offset-white transition-all text-sm" data-hs-overlay="#hs-basic-modal{{ $teacher->id }}">
                                                              <span class="sr-only">Close</span>
                                                              <svg class="w-3.5 h-3.5" width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M0.258206 1.00652C0.351976 0.912791 0.479126 0.860131 0.611706 0.860131C0.744296 0.860131 0.871447 0.912791 0.965207 1.00652L3.61171 3.65302L6.25822 1.00652C6.30432 0.958771 6.35952 0.920671 6.42052 0.894471C6.48152 0.868271 6.54712 0.854471 6.61352 0.853901C6.67992 0.853321 6.74572 0.865971 6.80722 0.891111C6.86862 0.916251 6.92442 0.953381 6.97142 1.00032C7.01832 1.04727 7.05552 1.1031 7.08062 1.16454C7.10572 1.22599 7.11842 1.29183 7.11782 1.35822C7.11722 1.42461 7.10342 1.49022 7.07722 1.55122C7.05102 1.61222 7.01292 1.6674 6.96522 1.71352L4.31871 4.36002L6.96522 7.00648C7.05632 7.10078 7.10672 7.22708 7.10552 7.35818C7.10442 7.48928 7.05182 7.61468 6.95912 7.70738C6.86642 7.80018 6.74102 7.85268 6.60992 7.85388C6.47882 7.85498 6.35252 7.80458 6.25822 7.71348L3.61171 5.06702L0.965207 7.71348C0.870907 7.80458 0.744606 7.85498 0.613506 7.85388C0.482406 7.85268 0.357007 7.80018 0.264297 7.70738C0.171597 7.61468 0.119017 7.48928 0.117877 7.35818C0.116737 7.22708 0.167126 7.10078 0.258206 7.00648L2.90471 4.36002L0.258206 1.71352C0.164476 1.61976 0.111816 1.4926 0.111816 1.36002C0.111816 1.22744 0.164476 1.10028 0.258206 1.00652Z" fill="currentColor"/>
                                                              </svg>
                                                            </button>
                                                        </div>
                                                        <div class="flex flex-col items-center pb-4">
                                                            @if ($teacher->picture)
                                                                <img src="{{ asset('storage/pictures/'.$teacher->picture) }}" class="object-cover w-24 h-24 mb-3 rounded-full shadow-lg" alt="picture">
                                                            @else
                                                            <img src="{{ asset('images/default-profile.svg') }}" alt="Default Profile Photo" class="object-cover w-24 h-24 mb-3 rounded-full shadow-lg">
                                                            @endif
                                                        </div>
                                                        <table class="min-w-full divide-y divide-gray-400 text-left">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="px-3 py-3 border border-gray-400 text-sm">
                                                                        <div>
                                                                            <p class="font-light text-gray-500 text-xs">Nama</p>
                                                                            {{ $teacher->teacher_name }}
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-3 py-3 border border-gray-400 text-sm">
                                                                        <div>
                                                                            <p class="font-light text-gray-500 text-xs">Kode</p>
                                                                            {{ $teacher->kode }}
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="px-3 py-3 border border-gray-400 text-sm">
                                                                        <div>
                                                                            <p class="font-light text-gray-500 text-xs">Email</p>
                                                                            {{ $teacher->email }}
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-3 py-3 border border-gray-400 text-sm">
                                                                        <div>
                                                                            <p class="font-light text-gray-500 text-xs">Password</p>
                                                                            <div class="group">
                                                                                <p id="star-password" class="group-hover:hidden">
                                                                                    {{ str_repeat('•', strlen($teacher->password)) }}
                                                                                </p>
                                                                                <p id="show-password" class="hidden group-hover:block">
                                                                                    {{ $teacher->password }}
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" class="px-3 py-3 border border-gray-400 text-sm">
                                                                        <div>
                                                                            @php
                                                                                $totalSubjects = DB::select('CALL TotalSubjects(?)', [$teacher->id]);
                                                                            @endphp
                                                                            <p class="font-light text-gray-500 text-xs mb-1">Mata Pelajaran | Total : {{ $totalSubjects[0]->{'COUNT(*)'} }} mapel</p>
                                                                            @foreach (explode(',', $teacher->subject_names) as $subject)
                                                                                @php
                                                                                    $subjectNameMatchesFilter = ($idSubjectName !== null ? ($subject == $idSubjectName) : ($subject == $subjectFilterName));
                                                                                @endphp
                                                                                <p class="inline-flex items-center rounded-md bg-gray-100 px-2 py-1 my-0.5 text-xs font-medium uppercase text-gray-700 ring-1 ring-inset ring-gray-500/40
                                                                                    {{ $subjectNameMatchesFilter ? 'underline' : '' }}
                                                                                    px-2 py-1 my-0.5 text-xs font-medium uppercase">
                                                                                    {{ $subject }}
                                                                                </p>
                                                                            @endforeach
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" class="px-3 py-3 border border-gray-400 text-sm">
                                                                        <div>
                                                                            @php
                                                                                $totalClasses = DB::select('CALL TotalClasses(?)', [$teacher->id]);
                                                                            @endphp
                                                                            <p class="font-light text-gray-500 text-xs mb-1">Kelas | Total : {{ $totalClasses[0]->{'COUNT(*)'} }} kelas</p>
                                                                            @foreach (explode(',', $teacher->class_names) as $class)
                                                                                @php
                                                                                    $classNameMatchesFilter = ($idClassName !== null ? ($class == $idClassName) : ($class == $classFilterName));
                                                                                @endphp
                                                                                <p class="inline-flex items-center rounded-md bg-gray-100 px-2 py-1 my-0.5 text-xs font-medium uppercase text-gray-700 ring-1 ring-inset ring-gray-500/40
                                                                                    {{ $classNameMatchesFilter ? 'underline' : '' }}
                                                                                    px-2 py-1 my-0.5 text-xs font-medium uppercase">
                                                                                    {{ $class }}
                                                                                </p>
                                                                            @endforeach
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                  </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <div id="edit-row-{{ $teacher->kode }}" class="hs-overlay hidden w-full h-full fixed top-0 left-0 z-[60] overflow-x-hidden overflow-y-auto">
                                            <div class="hs-overlay-open:mt-7 hs-overlay-open:opacity-100 hs-overlay-open:duration-500 mt-0 opacity-0 ease-out transition-all sm:max-w-lg sm:w-full m-6 sm:mx-auto">
                                                <form id="edit-form-{{ $teacher->kode }}" action="{{ route('teachers.update', ['id' => $id, 'idTeacher' => $teacher->id]) }}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                @method('PUT')
                                                <div class="teacher-field-group p-6 bg-white border border-gray-200 rounded-lg shadow">
                                                    <div class="border-b pb-3 mb-3 border-gray-400">
                                                        <h2 class="text-xl text-left font-bold">Edit data {{ $teacher->teacher_name }}</h2>
                                                    </div>
                                                    <div class="mb-1">
                                                        <label for="teachers_kode" class="block text-gray-700 font-bold">Kode</label>
                                                        <div class="relative">
                                                            <input type="number" id="teachers_kode{{ $teacher->id }}" name="teachers_kode" class="w-full my-2 input-text-primary pr-2" required oninput="updateKodeIndicator({{ $teacher->id }})" value="{{ $teacher->kode }}">
                                                            <span id="kodeIndicator{{ $teacher->id }}" class="absolute inset-y-0 right-4 flex items-center pr-2 text-sm text-gray-400">0</span>
                                                        </div>
                                                    </div>
                                                    <div class="mb-1">
                                                        <label class="block text-gray-700 font-bold " for="name">
                                                        Nama
                                                        </label>
                                                        <input class="input-text-primary my-2 w-full" id="name" name="name" type="text" value="{{ $teacher->teacher_name }}">
                                                    </div>
                                                    <div class="mb-1">
                                                        <label class="block text-gray-700 font-bold " for="email">
                                                        Email
                                                        </label>
                                                        <input class="input-text-primary my-2 w-full" id="email" name="email" type="email" value="{{ $teacher->email }}">
                                                    </div>
                                                    <div class="mb-1">
                                                        <label class="block text-gray-700 font-bold " for="subject_name">
                                                            Mata Pelajaran
                                                        </label>
                                                        <div class="flex flex-col">
                                                            <div id="selectedSubjects{{ $teacher->id }}" class="flex flex-wrap mt-2">
                                                                @php
                                                                $teacherId = $teacher->id;
                                                                $teacherSelect = App\Models\Teacher::find($teacherId);
                                                                $subjectSelects = $teacherSelect->subject;
                                                                @endphp
                                                                @foreach ($subjectSelects as $subjectSelect)
                                                                <div id="tagSubject{{ $subjectSelect->id }}{{ $teacher->id }}" class="bg-gray-100 text-xs text-gray-700 rounded-md uppercase ring-1 ring-inset ring-gray-500/40 py-1 px-2 mr-2 mb-1">
                                                                    {{ $subjectSelect->subject_name }}
                                                                    <button type="button" class="ml-1 text-gray-700 cursor-pointer" onclick="removeTagSubject({{ $subjectSelect->id }}, {{ $teacher->id }})">×</button>
                                                                </div>
                                                            @endforeach
                                                            </div>
                                                            <input id="scrollSubjects{{ $teacher->id }}" type="text" class="input-text-norounded rounded-t-md" placeholder="Cari" oninput="filterSubjects({{ $teacher->id }})">
                                                        </div>
                                                        <div>
                                                            <div id="subjectDropdown{{ $teacher->id }}" class="flex flex-col h-20 bg-white input-text-norounded rounded-b-md overflow-auto mb-2">
                                                                @foreach ($subjects as $s)
                                                                <div class="flex mr-4 mb-2 space-x-1 text-sm items-center subject-item">
                                                                    @php
                                                                    $isChecked = false;

                                                                    foreach ($subjectSelects as $subjectSelect) {
                                                                        if ($subjectSelect->id === $s->id) {
                                                                            $isChecked = true;
                                                                            break;
                                                                        }
                                                                    }
                                                                    @endphp
                                                                    <input type="checkbox" id="subject{{ $teacher->id }}{{ $s->id }}" value="{{ $s->id }}" name="subjects[]" {{ $isChecked ? 'checked' : '' }} onchange="updateSelectedSubjects({{ $teacher->id }}); resetInputSubject({{ $teacher->id }})">
                                                                    <label for="subject{{ $teacher->id }}{{ $s->id }}">{{ $s->subject_name }}</label>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mb-1">
                                                        <label class="block text-gray-700 font-bold " for="class_name">
                                                            Kelas
                                                        </label>
                                                        <div class="flex flex-col">
                                                            <div id="selectedClasses{{ $teacher->id }}" class="flex flex-wrap mt-2">
                                                                @php
                                                                $teacherId = $teacher->id;
                                                                $teacherSelect = App\Models\Teacher::find($teacherId);
                                                                $classSelects = $teacherSelect->classes;
                                                                @endphp
                                                                @foreach ($classSelects as $classSelect)
                                                                <div id="tagClass{{ $classSelect->id }}{{ $teacher->id }}" class="bg-gray-100 text-xs text-gray-700 rounded-md uppercase ring-1 ring-inset ring-gray-500/40 py-1 px-2 mr-2 mb-1">
                                                                    {{ $classSelect->class_name }}
                                                                    <button type="button" class="ml-1 text-gray-700 cursor-pointer" onclick="removeTagClass({{ $classSelect->id }}, {{ $teacher->id }})">×</button>
                                                                </div>
                                                            @endforeach
                                                            </div>
                                                            <input id="scrollClasses{{ $teacher->id }}" type="text" class="input-text-norounded rounded-t-md" placeholder="Cari" oninput="filterClasses({{ $teacher->id }})">
                                                        </div>
                                                        <div>
                                                            <div id="classDropdown{{ $teacher->id }}" class="flex flex-col h-20 bg-white input-text-norounded rounded-b-md overflow-auto mb-2">
                                                                @foreach ($classes as $class)
                                                                <div class="flex mr-4 mb-2 space-x-1 text-sm items-center class-item">
                                                                    @php
                                                                    $isChecked = false;

                                                                    foreach ($classSelects as $classSelect) {
                                                                        if ($classSelect->id === $class->id) {
                                                                            $isChecked = true;
                                                                            break;
                                                                        }
                                                                    }
                                                                    @endphp
                                                                    <input type="checkbox" id="class{{ $teacher->id }}{{ $class->id }}" value="{{ $class->id }}" name="classes[]" {{ $isChecked ? 'checked' : '' }} onchange="updateSelectedClasses({{ $teacher->id }}); resetInputClass({{ $teacher->id }})">
                                                                    <label for="class{{ $teacher->id }}{{ $class->id }}">{{ $class->class_name }}</label>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mb-1">
                                                        <label class="block text-gray-700 font-bold " for="password">
                                                        Password
                                                        </label>
                                                        <input class="input-text-primary my-2 w-full" id="password" name="password" type="password" value="{{ $teacher->password }}">
                                                    </div>
                                                    <div class="mb-4">
                                                        <label for="picture" class="block text-gray-700 font-bold mb-2">Foto Profil</label>
                                                        @if ($teacher->picture)
                                                            <img src="{{ asset('storage/pictures/'.$teacher->picture) }}" alt="picture" width="70">
                                                            <div class="my-2 flex items-center space-x-1">
                                                                <input type="checkbox" name="remove_picture" id="remove_picture{{ $teacher->id }}" class="hover:cursor-pointer" value="1">
                                                                <label for="remove_picture{{ $teacher->id }}" class="text-sm text-gray-700 hover:cursor-pointer">Hapus foto saat ini</label>
                                                            </div>
                                                        @endif
                                                        <input type="file" name="picture" id="picture" accept="image/*" class="input-text-primary hover:cursor-pointer" accept="image/*">
                                                    </div>
                                                    <div class="flex items-center justify-end space-x-2">
                                                        <button class="btn-secondary" onclick="location.reload();" type="button" data-hs-overlay="#edit-row-{{ $teacher->kode }}">
                                                        Batal
                                                        </button>
                                                        <button class="btn-primary" type="submit">
                                                        Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        @endforeach
                                        <form id="delete-form" action="{{ route('teachers.delete-selected', $id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <div id="delete-row" class="fixed z-50 bg-white shadow-[0_0_15px_-2px_rgba(0,0,0,0.3)] rounded-md ">
                                            <div colspan="6" class="px-6 py-4 text-center">
                                                <span id="checkbox-count" class="mr-3"></span>
                                                <button data-hs-overlay="#hs-hapus-modal" class="btn-delete pointer-events-auto">Hapus</button>
                                            </div>
                                        </div>
                                        <div id="hs-hapus-modal" class="hs-overlay hidden w-full h-full fixed top-0 left-0 z-[60] overflow-x-hidden overflow-y-auto">
                                            <div class="hs-overlay-open:mt-7 hs-overlay-open:opacity-100 hs-overlay-open:duration-500 mt-0 opacity-0 ease-out transition-all sm:max-w-sm sm:w-full m-3 sm:mx-auto min-h-[calc(100%-3.5rem)] flex items-center">
                                                <div class="min-w-full bg-white border p-4 border-gray-200 rounded-lg shadow">
                                                    <div class="flex justify-end mb-3 items-center">
                                                      <button type="button" class="hs-dropdown-toggle inline-flex flex-shrink-0 justify-center items-center h-4 w-4 rounded-md text-gray-500 hover:text-gray-400 focus:outline-none focus:ring-2 focus:ring-gray-400 focus:ring-offset-2 focus:ring-offset-white transition-all text-sm" data-hs-overlay="#hs-hapus-modal">
                                                        <span class="sr-only">Close</span>
                                                        <svg class="w-3.5 h-3.5" width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                          <path d="M0.258206 1.00652C0.351976 0.912791 0.479126 0.860131 0.611706 0.860131C0.744296 0.860131 0.871447 0.912791 0.965207 1.00652L3.61171 3.65302L6.25822 1.00652C6.30432 0.958771 6.35952 0.920671 6.42052 0.894471C6.48152 0.868271 6.54712 0.854471 6.61352 0.853901C6.67992 0.853321 6.74572 0.865971 6.80722 0.891111C6.86862 0.916251 6.92442 0.953381 6.97142 1.00032C7.01832 1.04727 7.05552 1.1031 7.08062 1.16454C7.10572 1.22599 7.11842 1.29183 7.11782 1.35822C7.11722 1.42461 7.10342 1.49022 7.07722 1.55122C7.05102 1.61222 7.01292 1.6674 6.96522 1.71352L4.31871 4.36002L6.96522 7.00648C7.05632 7.10078 7.10672 7.22708 7.10552 7.35818C7.10442 7.48928 7.05182 7.61468 6.95912 7.70738C6.86642 7.80018 6.74102 7.85268 6.60992 7.85388C6.47882 7.85498 6.35252 7.80458 6.25822 7.71348L3.61171 5.06702L0.965207 7.71348C0.870907 7.80458 0.744606 7.85498 0.613506 7.85388C0.482406 7.85268 0.357007 7.80018 0.264297 7.70738C0.171597 7.61468 0.119017 7.48928 0.117877 7.35818C0.116737 7.22708 0.167126 7.10078 0.258206 7.00648L2.90471 4.36002L0.258206 1.71352C0.164476 1.61976 0.111816 1.4926 0.111816 1.36002C0.111816 1.22744 0.164476 1.10028 0.258206 1.00652Z" fill="currentColor"/>
                                                        </svg>
                                                      </button>
                                                    </div>
                                                    <div class="w-full mb-4 message-component flex justify-start items-center bg-yellow-100 border border-yellow-400 text-yellow-700 px-2 py-3 rounded relative" role="alert">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-11 w-11 text-yellow-500 mr-2" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                            <circle cx="12" cy="12" r="10" />
                                                            <line x1="12" y1="8" x2="12" y2="12" />
                                                            <line x1="12" y1="16" x2="12" y2="16" />
                                                        </svg>
                                                        <span class="block sm:inline">Apakah anda yakin?</span>
                                                    </div>
                                                    <div class="flex justify-end pt-4 border-t border-gray-400 space-x-1">
                                                        <button id="delete-button" class="btn-delete w-full justify-center" type="submit" form="delete-form">Hapus</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tbody>
                                </table>
                              </div>
                              <div class="mt-4">
                                {{ $teachers->withQueryString()->links('vendor.pagination.custom-pagination', ['perPage' => $perPage]) }}
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    #delete-row {
        /* Other styles for the delete-row element */

        /* Initial state */
        opacity: 0;
        transform: translateY(100%);
        pointer-events: none;

        /* Transition properties */
        transition-property: opacity, transform;
        transition-duration: 0.3s;
        transition-timing-function: ease-in-out;

        bottom: 35px;
        left: 50%;
        translate: -50%;
    }

    #delete-row.visible {
        /* Visible state */
        opacity: 1;
        transform: translateY(0);
    }
</style>
<script>
    function removeTagSubject(subjectId, teacherId) {
        const subjectTag = document.getElementById(`tagSubject${subjectId}${teacherId}`);
        const checkbox = document.getElementById(`subject${teacherId}${subjectId}`);

        if (subjectTag) {
            checkbox.checked = false;
            subjectTag.remove();
        }
    }
    function filterSubjects(teacherId) {
        const input = document.getElementById('scrollSubjects' + teacherId);
        const subjectDropdown = document.getElementById('subjectDropdown' + teacherId);
        const subjectItems = subjectDropdown.querySelectorAll('.subject-item');

        const searchText = input.value.toLowerCase();

        subjectItems.forEach(item => {
            const label = item.querySelector('label');
            const text = label.textContent.toLowerCase();

            if (text.includes(searchText)) {
                item.style.display = 'flex';
            } else {
                item.style.display = 'none';
            }
        });
    }
        function resetInputSubject(id) {
            const input = document.getElementById('scrollSubjects' + id);
            input.value = '';

            const subjectDropdown = document.getElementById('subjectDropdown' + id);
            const subjectItems = subjectDropdown.querySelectorAll('.subject-item');

            subjectItems.forEach(item => {
            item.style.display = 'flex';
        });
    }

    function updateSelectedSubjects(teacherId) {
        const subjectDropdown = document.getElementById('subjectDropdown' + teacherId);
        const selectedSubjectsContainer = document.getElementById('selectedSubjects' + teacherId);

        selectedSubjectsContainer.innerHTML = '';

        const selectedCheckboxes = Array.from(subjectDropdown.querySelectorAll('input[type="checkbox"]:checked'));

        selectedCheckboxes.forEach(checkbox => {
            const tag = document.createElement('div');
            tag.classList.add('bg-gray-100', 'text-xs', 'text-gray-700', 'rounded-md', 'uppercase', 'ring-1', 'ring-inset', 'ring-gray-500/40', 'py-1', 'px-2', 'mr-2', 'mb-1');
            tag.textContent = checkbox.nextElementSibling.textContent;

            const removeButton = document.createElement('button');
            removeButton.classList.add('ml-1', 'text-gray-700', 'cursor-pointer');
            removeButton.innerHTML = '×';

            removeButton.addEventListener('click', () => {
                checkbox.checked = false;
                tag.remove();
            });

            tag.appendChild(removeButton);
            selectedSubjectsContainer.appendChild(tag);
        });
    }

    function removeTagClass(classId, teacherId) {
        const classTag = document.getElementById(`tagClass${classId}${teacherId}`);
        const checkbox = document.getElementById(`class${teacherId}${classId}`);

        if (classTag) {
            checkbox.checked = false;
            classTag.remove();
        }
    }
    function filterClasses(teacherId) {
        const input = document.getElementById('scrollClasses' + teacherId);
        const classDropdown = document.getElementById('classDropdown' + teacherId);
        const classItems = classDropdown.querySelectorAll('.class-item');

        const searchText = input.value.toLowerCase();

        classItems.forEach(item => {
            const label = item.querySelector('label');
            const text = label.textContent.toLowerCase();

            if (text.includes(searchText)) {
                item.style.display = 'flex';
            } else {
                item.style.display = 'none';
            }
        });
    }
        function resetInputClass(id) {
            const input = document.getElementById('scrollClasses' + id);
            input.value = '';

            const classDropdown = document.getElementById('classDropdown' + id);
            const classItems = classDropdown.querySelectorAll('.class-item');

            classItems.forEach(item => {
            item.style.display = 'flex';
        });
    }

    function updateSelectedClasses(teacherId) {
        const classDropdown = document.getElementById('classDropdown' + teacherId);
        const selectedClassesContainer = document.getElementById('selectedClasses' + teacherId);

        selectedClassesContainer.innerHTML = '';

        const selectedCheckboxes = Array.from(classDropdown.querySelectorAll('input[type="checkbox"]:checked'));

        selectedCheckboxes.forEach(checkbox => {
            const tag = document.createElement('div');
            tag.classList.add('bg-gray-100', 'text-xs', 'text-gray-700', 'rounded-md', 'uppercase', 'ring-1', 'ring-inset', 'ring-gray-500/40', 'py-1', 'px-2', 'mr-2', 'mb-1');
            tag.textContent = checkbox.nextElementSibling.textContent;

            const removeButton = document.createElement('button');
            removeButton.classList.add('ml-1', 'text-gray-700', 'cursor-pointer');
            removeButton.innerHTML = '×';

            removeButton.addEventListener('click', () => {
                checkbox.checked = false;
                tag.remove();
            });

            tag.appendChild(removeButton);
            selectedClassesContainer.appendChild(tag);
        });
    }

    function showDeleteDialog() {
        const deleteDialog = document.getElementById('delete-dialog');
        deleteDialog.showModal();
    }

    function cancelDelete() {
        const deleteDialog = document.getElementById('delete-dialog');
        deleteDialog.close();
    }
    function editTeacher(kode) {
      document.querySelectorAll('[id^="edit-row-"]').forEach(row => {
        row.classList.add('hidden');
      });
      document.getElementById('edit-row-' + kode).classList.remove('hidden');
    }

    function cancelEdit(button) {
    if (!button) {
        console.error('Button parameter is null');
        return;
    }
    var editRow = button.closest('.edit-row');
    if (!editRow) {
        console.error('Edit row not found');
        return;
    }
    editRow.classList.add('hidden');
    location.reload();
    }

    const selectAllCheckbox = document.getElementById('select-all-checkbox');
    const deleteRow = document.getElementById('delete-row');
    const dataList = document.getElementsByName('selected[]');
    const form = document.getElementById('delete-form');
    const deleteBtn = document.getElementById('delete-button');

    // Function to handle checkbox changes
    function handleCheckboxChange() {
        const checkedBoxes = document.querySelectorAll('input[name="selected[]"]:checked');
        const totalChecked = checkedBoxes.length; // Count the selected checkboxes

        if (totalChecked > 0) {
            deleteRow.classList.add('visible');
        } else {
            deleteRow.classList.remove('visible');
        }

        // Display the count somewhere in your UI, e.g., in a <span> element with the id "checkbox-count"
        const checkboxCountElement = document.getElementById('checkbox-count');
        if (checkboxCountElement) {
            checkboxCountElement.textContent = `${totalChecked} data terpilih`;
        }

        dataList.forEach((checkbox) => {
            checkbox.closest('tr').classList.remove('bg-red-100');
        });

        checkedBoxes.forEach((checkbox) => {
            checkbox.closest('tr').classList.add('bg-red-100');
        });
    }

    // Event listener for the "Select All" checkbox
    selectAllCheckbox.addEventListener('change', () => {
        dataList.forEach((checkbox) => {
            checkbox.checked = selectAllCheckbox.checked;
        });

        handleCheckboxChange();
    });

        dataList.forEach((checkbox) => {
        checkbox.addEventListener('change', () => {
            if (!checkbox.checked) {
                // If any individual checkbox is unchecked, uncheck the "Select All" checkbox
                selectAllCheckbox.checked = false;
            }

            handleCheckboxChange();
        });
    });

    // Event listeners for individual checkboxes
    dataList.forEach((checkbox) => {
        checkbox.addEventListener('change', () => {
            handleCheckboxChange();
        });
    });

    // Function to handle deleting selected rows
    deleteBtn.addEventListener('click', () => {
        const checkedBoxes = document.querySelectorAll('input[name="selected[]"]:checked');
        form.querySelectorAll('input[name="selected[]"]').forEach((c) => c.remove());
        checkedBoxes.forEach((checkBox) => {
            form.appendChild(checkBox.cloneNode());
        });
    });

    function updateKodeIndicator(id) {
        const input = document.getElementById(`teachers_kode${id}`);
        const kodeIndicator = document.getElementById(`kodeIndicator${id}`);
        kodeIndicator.textContent = `${input.value.length}`;
    }

    // Call the function for each teacher element
    @foreach ($teachers as $teacher)
        updateKodeIndicator({{ $teacher->id }});
    @endforeach

</script>

@endsection
