@extends('layouts.app')
@section('title', "Timetable | Agenda Kelas")
@section('timeTable')
<div class="py-6">
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8 space-y-6">
        <nav class="flex items-center pl-4 sm:pl-8 text-gray-500 text-lg font-medium" aria-label="Breadcrumb">
            <ol class="flex items-center space-x-2">
                <li>
                    <a href="{{ route('schedule.timetable', [$id, $idClass]) }}" class="text-gray-900">Jadwal</a>
                </li>
            </ol>
        </nav>
        <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
            <div>
                <div class="border-b pb-3 mb-4 border-gray-400">
                    <h2 class="text-lg font-medium text-gray-900">
                        <div class="flex justify-between items-center">
                            <div class="flex relative items-center">
                                {{ __('Jadwal pelajaran kelas ') }}
                                <form action="{{ route('schedule.timetable', [$id, $idClass]) }}" class="ml-2" method="GET">
                                    <select onchange="this.form.submit()" class="w-full pl-2 pr-6 input-text-primary" style="-webkit-appearance: none;" name="selected_class">
                                        <option value="" selected disabled>Pilih kelas</option>
                                        @foreach ($classes as $class)
                                            <option value="{{ $class->id }}" {{ request('selected_class') == $class->id ? 'selected' : '' }}>
                                                {{ $class->class_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </form>
                                <div class="absolute inset-y-0 right-1 flex items-center pointer-events-none">
                                    <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                    </svg>
                                </div>
                            </div>
                            @if($idClass)
                            <div align="right" class="flex items-center gap-2">
                                <a href="{{ route('schedule.show', ['id' => $id, 'idClass' => $idClass]) }}">
                                    <button class="flex btn-primary py-1 px-2 fill-white">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5" viewBox="0 0 512 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.7 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z"/></svg>                                    </button>
                                </a>
                                <a href="{{ route('schedule.PDF', ['id' => $id]) }}">
                                    <button class="flex btn-primary py-1 px-2 fill-white">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5" viewBox="0 0 512 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M0 64C0 28.7 28.7 0 64 0H224V128c0 17.7 14.3 32 32 32H384V304H176c-35.3 0-64 28.7-64 64V512H64c-35.3 0-64-28.7-64-64V64zm384 64H256V0L384 128zM176 352h32c30.9 0 56 25.1 56 56s-25.1 56-56 56H192v32c0 8.8-7.2 16-16 16s-16-7.2-16-16V448 368c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24H192v48h16zm96-80h32c26.5 0 48 21.5 48 48v64c0 26.5-21.5 48-48 48H304c-8.8 0-16-7.2-16-16V368c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H320v96h16zm80-112c0-8.8 7.2-16 16-16h48c8.8 0 16 7.2 16 16s-7.2 16-16 16H448v32h32c8.8 0 16 7.2 16 16s-7.2 16-16 16H448v48c0 8.8-7.2 16-16 16s-16-7.2-16-16V432 368z"/></svg>
                                    </button>
                                </a>
                            </div>
                            @endif
                        </div>
                    </h2>
                </div>
                <div class="flex flex-col">
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="w-[1000px] overflow-x-auto m-auto">
                                <table class="min-w-full divide-y divide-gray-400 text-left table-fixed">
                                    <tr class="bg-gray-100">
                                        <th class="px-3 py-3 border border-gray-400 text-xs font-medium uppercase whitespace-nowrap">Jam ke</th>
                                        @foreach($weekDays as $day)
                                        <th class="px-3 py-3 w-1/5 border border-gray-400 text-xs font-medium uppercase">{{ $day }}</th>
                                        @endforeach
                                    </tr>@php
                                    $backgroundColors = [
                                        'border-red-600',
                                        'border-blue-600',
                                        'border-green-600',
                                        'border-yellow-600',
                                        'border-pink-600',
                                        'border-indigo-600',
                                    ];
                                    $colorIndex = 0;

                                    function getNextColor(&$colorIndex, $backgroundColors) {
                                        $colorClass = $backgroundColors[$colorIndex];
                                        $colorIndex = ($colorIndex + 1) % count($backgroundColors);
                                        return $colorClass;
                                    }
                                    @endphp

                                    @foreach($hours as $hour)
                                        <tr class="text-center">
                                            <th class="border border-gray-400 py-5 text-xs font-medium uppercase whitespace-nowrap">{{ $hour }}</th>
                                            @foreach($weekDays as $day)
                                                @if($remainingDuration[$day] > 0)
                                                    @php $remainingDuration[$day]--; @endphp
                                                @elseif(isset($schedules[$day][$hour]))
                                                    @php $colorClass = getNextColor($colorIndex, $backgroundColors); @endphp
                                                    <td rowspan="{{ $schedules[$day][$hour]->first()->duration ?? 1 }}" class="border border-gray-400 bg-gray-50 relative">
                                                        @foreach($schedules[$day][$hour] as $schedule)
                                                            <div class="absolute inset-0 pl-3 border-l-4 {{ $colorClass }} flex flex-col justify-center text-left">
                                                                <div class="font-bold">{{ $schedule->teacher->teacher_name }}</div>
                                                                <div class="text-sm">{{ $schedule->subject->subject_name }}</div>
                                                            </div>
                                                        @endforeach
                                                    </td>
                                                    @php $remainingDuration[$day] = $schedules[$day][$hour]->first()->duration - 1; @endphp
                                                @else
                                                    <td class="border border-gray-400 px-6 py-4"></td>
                                                @endif
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
