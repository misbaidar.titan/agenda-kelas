<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body style="font-family: 'Sans-serif'; background-color: #F7FAFC;">
        <div style="padding: 1rem; padding: 2rem; background-color: #FFFFFF; box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06); border-radius: 0.375rem;">
            <div style="border-bottom: 1px solid #000000; margin-bottom: 1rem;">
                <h2>
                    <div style="display: flex; align-items: center;">
                        {{ __('Jadwal pelajaran kelas ') }}{{ $className }}
                    </div>
                </h2>
            </div>
            <div style="display: flex; flex-direction: column;">
                <table style="min-width:100%; border-collapse:collapse;">
                    <tr style="background-color:#f7fafc;">
                        <th style="padding:0.75rem; border:1px solid #5e5e5e; font-size:0.75rem; text-transform:uppercase;">Jam ke</th>
                        @foreach($weekDays as $day)
                            <th style="padding:0.75rem; width:18.5%; border:1px solid #5e5e5e; font-size:0.75rem; text-transform:uppercase;">{{ $day }}</th>
                        @endforeach
                    </tr>
                    @foreach($hours as $hour)
                        <tr style="text-align:center;">
                            <th style="border:1px solid #5e5e5e; padding-top:1.25rem; padding-bottom:1.25rem; font-size:0.75rem; text-transform:uppercase;">{{ $hour }}</th>
                            @foreach($weekDays as $day)
                                @if($remainingDuration[$day] > 0)
                                    @php $remainingDuration[$day]--; @endphp
                                @elseif(isset($schedules[$day][$hour]))
                                    <td rowspan="{{ $schedules[$day][$hour]->first()->duration ?? 1 }}" style="border:1px solid #5e5e5e; background-color:#f7f7f7; position:relative;">
                                        @foreach($schedules[$day][$hour] as $schedule)
                                            <div style="position:absolute; top:0; right:0; bottom:0; left:0; padding-left:0.75rem; border-left-width:4px; display:flex; flex-direction:column; justify-content:center; text-align:left;">
                                                <div style="font-weight:bold;">{{ $schedule->teacher->teacher_name }}</div>
                                                <div style="font-size:0.875rem;">{{ $schedule->subject->subject_name }}</div>
                                            </div>
                                        @endforeach
                                    </td>
                                    @php $remainingDuration[$day] = $schedules[$day][$hour]->first()->duration - 1; @endphp
                                @else
                                    <td style="border:1px solid #5e5e5e; padding-left:1.5rem; padding-right:1.5rem; padding-top:1rem; padding-bottom:1rem;"></td>
                                @endif
                            @endforeach
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </body>
</html>
