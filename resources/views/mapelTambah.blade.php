@extends('layouts.app')
@section('title', "Tambah | Agenda Kelas")
@section('mapelTambah')
<div class="flex flex-col items-center">
    <div class="w-full sm:max-w-xl p-4 overflow-hidden">
        <nav class="flex items-center pb-4 sm:pl-8 text-gray-500 text-lg font-medium" aria-label="Breadcrumb">
            <ol class="flex items-center space-x-2">
                <li>
                    <a href="{{ route('teachers.showGuru', $id) }}" class="hover:text-gray-900">Guru</a>
                </li>
                <li>
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M5.707 14.707a1 1 0 1 1-1.414-1.414L7.586 10 4.293 6.707a1 1 0 1 1 1.414-1.414l4 4a1 1 0 0 1 0 1.414l-4 4z" clip-rule="evenodd" />
                    </svg>
                </li>
                <li>
                    <a href="{{ route('subject.show', $id) }}" class="hover:text-gray-900">Mata Pelajaran</a>
                </li>
                <li>
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M5.707 14.707a1 1 0 1 1-1.414-1.414L7.586 10 4.293 6.707a1 1 0 1 1 1.414-1.414l4 4a1 1 0 0 1 0 1.414l-4 4z" clip-rule="evenodd" />
                    </svg>
                </li>
                <li>
                    <a href="#" class="text-gray-900">Tambah data</a>
                </li>
            </ol>
        </nav>
        <div class="container mx-auto">
            <form id="mapelForm" action="{{ route('subject.store', $id) }}" method="POST">
                @csrf
                <div class="bg-white shadow-md rounded px-8 py-8 mb-8">
                    <label for="mapelAmount" class="block mb-2">Jumlah mata pelajaran yang akan ditambahkan:</label>
                    <input type="number" id="mapelAmount" name="mapelAmount" min="1" class="mb-2 input-text-primary pr-2" onchange="updateMapelFields()">
                </div>
                <div id="mapelFields" class="block"></div>

                <div class="flex justify-end">
                <button type="submit" class="btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function updateMapelFields() {
        const mapelAmount = document.getElementById('mapelAmount').value;
        let mapelFields = '';

        for (let i = 1; i <= mapelAmount; i++) {
            mapelFields += `
                <div class="class-field-group bg-white shadow-md rounded px-8 pt-6 pb-8 mb-8">
                    <div class="border-b pb-3 mb-3 border-gray-400">
                        <h3 class="text-1xl text-center font-bold">Mata Pelajaran baru ${i}</h3>
                    </div>
                    <label for="mapel${i}_name" class="block text-gray-700 font-bold">Nama</label>
                    <input type="text" id="mapel${i}_name" name="mapel${i}_name" class="w-full my-2 input-text-primary" required>
                </div>`;
        }

        document.getElementById('mapelFields').innerHTML = mapelFields;
    }
</script>


@endsection
