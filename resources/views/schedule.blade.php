@extends('layouts.app')
@section('title', "Jadwal | Agenda Kelas")
@section('schedule')
<div class="py-6">
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8 space-y-6">
        <nav class="flex items-center pl-4 sm:pl-8 text-gray-500 text-lg font-medium" aria-label="Breadcrumb">
            <ol class="flex items-center space-x-2">
                <li>
                    <a href="{{ route('schedule.timetable', [$id, 'selected_class' => $idClass]) }}" class="hover:text-gray-900">Jadwal</a>
                </li>
                <li>
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M5.707 14.707a1 1 0 1 1-1.414-1.414L7.586 10 4.293 6.707a1 1 0 1 1 1.414-1.414l4 4a1 1 0 0 1 0 1.414l-4 4z" clip-rule="evenodd" />
                    </svg>
                </li>
                <li>
                    <a href="{{ route('schedule.show', [$id, $idClass]) }}" class="text-gray-900">Kelola Jadwal</a>
                </li>
            </ol>
        </nav>
        <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
            @if (session('success'))
                <div class="fixed left-1/2 -translate-x-1/2 z-50 message-component flex bg-green-100 border border-green-400 text-green-700 px-2 py-3 shadow-lg rounded" role="alert">
                    <div>
                        <strong class="font-bold">Selamat!</strong>
                        <span class="block sm:inline mr-4">{{ session('success') }}</span>
                    </div>
                    <div class="flex items-center">
                        <button type="button" onclick="this.closest('.message-component').remove()">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-green-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                            </svg>
                        </button>
                    </div>
                </div>
            @endif
            @if($errors->any())
                <div class="fixed left-1/2 -translate-x-1/2 z-50 message-component flex bg-red-100 border border-red-400 text-red-700 px-2 py-3 shadow-lg rounded" role="alert">
                    <div>
                        <strong class="font-bold">Oops!</strong>
                        <span class="block sm:inline">{{ $errors->first() }}</span>
                    </div>
                    <div class="flex items-center">
                        <button type="button" onclick="this.closest('.message-component').remove()">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                            </svg>
                        </button>
                    </div>
                </div>
            @endif
            <div>
                <div class="border-b pb-3 mb-4 border-gray-400">
                    <h2 class="text-lg font-medium text-gray-900">
                        {{ __('Data jadwal kelas ') }}{{ $className }}
                    </h2>
                </div>
                <div class="flex my-4 justify-between">
                    <form action="{{ route('schedule.search', [$id, $idClass]) }}" method="GET">
                        <div class="flex">
                            <div class="relative w-48">
                                <select class="w-full py-2 input-text-primary" onchange="this.form.submit()" style="-webkit-appearance: none;" name="selected_day">
                                    <option value="" selected>Semua hari</option>
                                    <option value="senin" {{ request('selected_day') == 'senin' ? 'selected' : '' }}>senin</option>
                                    <option value="selasa" {{ request('selected_day') == 'selasa' ? 'selected' : '' }}>selasa</option>
                                    <option value="rabu" {{ request('selected_day') == 'rabu' ? 'selected' : '' }}>rabu</option>
                                    <option value="kamis" {{ request('selected_day') == 'kamis' ? 'selected' : '' }}>kamis</option>
                                    <option value="jumat" {{ request('selected_day') == 'jumat' ? 'selected' : '' }}>jumat</option>
                                </select>
                                <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                                    <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div align="right">
                        <a href="{{ route('schedule.create', ['id' => $id, 'idClass' => $idClass]) }}"><button class="btn-primary">tambah</button></a>
                    </div>
                </div>
                <div class="flex mt-4 mb-4">
                    <div class="flex-1">
                        <form action="{{ route('schedule.search', [$id, $idClass]) }}" method="GET">
                            <div class="relative">
                                <input type="text" name="search" placeholder="Cari" class="py-2 pl-10 min-w-full pr-4 border border-gray-400 rounded-md shadow-sm focus:outline-none">
                                <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                    <img src="{{ asset('images/search.svg') }}" alt="cari" width="16">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="my-4">
                    <p class="mt-1 text-sm text-gray-600">Total {{ $schedules->total() }} hasil</p>
                    @isset ($search)
                        <p class="mt-1 text-sm text-gray-600">Menampilkan hasil pencarian dari "{{ $search }}"</p>
                    @endisset
                </div>
                <div class="flex flex-col">
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="shadow overflow-hidden border border-gray-400 sm:rounded-lg">
                                <table class="min-w-full divide-y divide-gray-400">
                                    <thead class="bg-gray-100 text-left border-b border-gray-400 text-sm font-medium uppercase tracking-wider">
                                        <tr class="whitespace-nowrap">
                                            <th class="px-2 py-4 text-center"></th>
                                            <th class="px-2 py-4">
                                                <div class="flex items-center">
                                                    <input type="checkbox" id="select-all-checkbox" class="form-checkbox w-5 h-5 text-gray-600">
                                                </div>
                                            </th>
                                            <th class="px-2 py-3 w-2/12">Hari</th>
                                            <th class="px-2 py-3 w-4/12">Nama Guru</th>
                                            <th class="px-2 py-3 w-4/12">Mata Pelajaran</th>
                                            <th class="px-2 py-3 w-1/12">Jam Mulai</th>
                                            <th class="px-2 py-3 w-1/12">Jam Akhir</th>
                                            <th class="px-6 py-3"></th>
                                        </tr>
                                    </thead>
                                    <tbody class="bg-white divide-y divide-gray-400 text-gray-700">
                                        @foreach($schedules as $s)
                                            <tr>
                                                <td class="px-2 py-4 text-center border-r border-gray-400">{{ $loop->iteration + $schedules->firstItem() - 1 }}</td>
                                                <td class="px-2 py-4">
                                                    <div class="flex items-center">
                                                        <input type="checkbox" name="selected[]" value="{{ $s->id }}" class="form-checkbox w-5 h-5 text-gray-600">
                                                    </div>
                                                </td>
                                                <td class="px-2 py-4 capitalize">{{ $s->day }}</td>
                                                <td class="px-2 py-4">{{ $s->teacher->teacher_name }}</td>
                                                <td class="px-2 py-4">{{ $s->subject->subject_name }}</td>
                                                <td class="px-2 py-4 text-center">{{ $s->start_hour }}</td>
                                                <td class="px-2 py-4 text-center">{{ $s->end_hour }}</td>
                                                <td>
                                                    <button class="p-2 btn-primary flex items-center justify-center" onclick="editClass('{{ $s->id }}')">
                                                        <img src="{{ asset('images/edit.svg') }}" alt="edit" width="15px">
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr id="edit-row-{{ $s->id }}" class="edit-row hidden">
                                                <td colspan="8" class="px-6 py-4">
                                                <div>
                                                    <form id="edit-schedule-form-{{ $s->id }}" action="{{ route('schedule.update', ['id' => $id, 'idClass' => $idClass, 'idSchedule' => $s->id]) }}" method="POST">
                                                        @csrf
                                                        @method('PUT')
                                                        <div class="mb-4">
                                                            <label for="day" class="block text-gray-700 font-bold">Hari</label>
                                                            <div class="relative">
                                                                <select id="day" name="day" class="w-full py-2 my-2 input-text-primary" style="-webkit-appearance: none;" required>
                                                                    <option value="senin" {{ $s->day === 'senin' ? 'selected' : '' }}>Senin</option>
                                                                    <option value="selasa" {{ $s->day === 'selasa' ? 'selected' : '' }}>Selasa</option>
                                                                    <option value="rabu" {{ $s->day === 'rabu' ? 'selected' : '' }}>Rabu</option>
                                                                    <option value="kamis" {{ $s->day === 'kamis' ? 'selected' : '' }}>Kamis</option>
                                                                    <option value="jumat" {{ $s->day === 'jumat' ? 'selected' : '' }}>Jumat</option>
                                                                </select>
                                                                <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                                                                    <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                                                    </svg>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mb-4">
                                                            <label for="teacher" class="block text-gray-700 font-bold">Guru</label>
                                                            <div class="relative">
                                                                <select id="teacher{{ $s->teacher_id }}" name="teacher" class="w-full py-2 my-2 input-text-primary" style="-webkit-appearance: none;" required onchange="updateSubjectDropdown(this, 'subject{{ $s->subject_id }}')">
                                                                    <option value="" disabled>Pilih Guru</option>
                                                                    @foreach ($teachers as $teacher)
                                                                        <option value="{{ $teacher->id }}" {{ $s->teacher_id === $teacher->id ? 'selected' : '' }}>{{ $teacher->teacher_name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                                                                    <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                                                    </svg>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mb-4">
                                                            <label for="subject" class="block text-gray-700 font-bold">Mata Pelajaran</label>
                                                            <div class="relative">
                                                                <select id="subject{{ $s->subject_id }}" name="subject" class="w-full py-2 my-2 input-text-primary" style="-webkit-appearance: none;" required>
                                                                    <option value="" disabled>Pilih Guru terlebih dahulu</option>
                                                                    @foreach ($availableSubjects[$s->teacher_id] as $subject)
                                                                        <option value="{{ $subject->id }}" {{ $s->subject_id === $subject->id ? 'selected' : '' }}>{{ $subject->subject_name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                                                                    <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                                                    </svg>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mb-4">
                                                            <label for="start_hour" class="block text-gray-700 font-bold">Jam Pelajaran</label>
                                                            <div class="my-2 flex">
                                                                <div class="relative">
                                                                    <select class="input-text-norounded rounded-l-md pr-6" style="-webkit-appearance: none;" id="start_hour" name="start_hour" required onchange="populateEndHour()">
                                                                        <option value="">Jam Mulai</option>
                                                                        @for ($i = 1; $i <= 10; $i++)
                                                                            @if ($i == $s->start_hour)
                                                                            <option value="{{ $i }}" selected>{{ $i }}</option>
                                                                            @else
                                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                                            @endif
                                                                        @endfor
                                                                    </select>
                                                                    <div class="absolute inset-y-0 right-1 flex items-center pointer-events-none">
                                                                        <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                                                        </svg>
                                                                    </div>
                                                                </div>
                                                                <div class="relative">
                                                                    <select class="input-text-norounded rounded-r-md pr-6" style="-webkit-appearance: none;" id="end_hour" name="end_hour" required>
                                                                        <option value="">Jam Akhir</option>
                                                                        @for ($i = 1; $i <= 10; $i++)
                                                                            @if ($i == $s->end_hour)
                                                                            <option value="{{ $i }}" selected>{{ $i }}</option>
                                                                            @else
                                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                                            @endif
                                                                        @endfor
                                                                    </select>
                                                                    <div class="absolute inset-y-0 right-1 flex items-center pointer-events-none">
                                                                        <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                                                        </svg>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="flex justify-end">
                                                            <button class="btn-secondary mr-6" type="button" onclick="cancelEdit(this)">
                                                                Batal
                                                            </button>
                                                            <button class="btn-primary" type="submit">
                                                                Simpan
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        <form id="delete-form" action="{{ route('schedule.delete-selected', $id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <div id="delete-row" class="fixed z-50 bg-white shadow-[0_0_15px_-2px_rgba(0,0,0,0.3)] rounded-md ">
                                            <div colspan="6" class="px-6 py-4 text-center">
                                                <span id="checkbox-count" class="mr-3"></span>
                                                <button onclick="showDeleteDialog()" class="btn-delete pointer-events-auto">Hapus</button>
                                            </div>
                                        </div>
                                        <dialog id="delete-dialog" class="dialog-component rounded-md">
                                            <div class="dialog-content">
                                                <div class="w-full mb-4 message-component flex justify-center items-center bg-yellow-100 border border-yellow-400 text-yellow-700 px-2 py-3 rounded relative" role="alert">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-11 w-11 text-yellow-500 mr-2" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                        <circle cx="12" cy="12" r="10" />
                                                        <line x1="12" y1="8" x2="12" y2="12" />
                                                        <line x1="12" y1="16" x2="12" y2="16" />
                                                    </svg>
                                                    <span class="block sm:inline">Apakah anda yakin?</span>
                                                </div>
                                            </div>
                                            <div class="dialog-actions flex justify-center">
                                                <button class="btn-secondary mr-6" onclick="cancelDelete()">Batal</button>
                                                <button id="delete-button" class="btn-primary" type="submit" form="delete-form">Yakin</button>
                                            </div>
                                        </dialog>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-4">
                              {{ $schedules->withQueryString()->links('vendor.pagination.custom-pagination', ['perPage' => $perPage]) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    #delete-row {
        /* Other styles for the delete-row element */

        /* Initial state */
        opacity: 0;
        transform: translateY(100%);
        pointer-events: none;

        /* Transition properties */
        transition-property: opacity, transform;
        transition-duration: 0.3s;
        transition-timing-function: ease-in-out;

        bottom: 35px;
        left: 50%;
        translate: -50%;
    }

    #delete-row.visible {
        /* Visible state */
        opacity: 1;
        transform: translateY(0);
    }
</style>
<script>
    function populateEndHour() {
        var startHourSelect = document.getElementById('start_hour');
        var endHourSelect = document.getElementById('end_hour');
        var startHour = startHourSelect.options[startHourSelect.selectedIndex].value;

        endHourSelect.innerHTML = '';

        var defaultOption = document.createElement('option');
        defaultOption.value = '';
        defaultOption.text = 'Jam Akhir';
        endHourSelect.add(defaultOption);

        for (var i = parseInt(startHour); i <= 10; i++) {
            var option = document.createElement('option');
            option.value = i;
            option.text = i;
            endHourSelect.add(option);
        }
    }

    function updateSubjectDropdown(teacherDropdown, subjectDropdownId) {
        const selectedTeacherId = teacherDropdown.value;
        const subjectDropdown = document.getElementById(subjectDropdownId);
        subjectDropdown.innerHTML = '';

        if (selectedTeacherId) {
            const availableSubjects = @json($availableSubjects);
            const teacherSubjects = availableSubjects[selectedTeacherId];

            if (teacherSubjects) {
                for (const subject of teacherSubjects) {
                    const option = document.createElement('option');
                    option.value = subject.id;
                    option.textContent = subject.subject_name;
                    subjectDropdown.appendChild(option);
                }
            }
            subjectDropdown.removeAttribute('disabled');
        } else {
            subjectDropdown.innerHTML = '<option value="" disabled selected>Pilih Guru terlebih dahulu</option>';
            subjectDropdown.setAttribute('disabled', 'disabled');
        }
    }
    function editClass(id) {
      document.querySelectorAll('[id^="edit-row-"]').forEach(row => {
        row.classList.add('hidden');
      });
      document.getElementById('edit-row-' + id).classList.remove('hidden');
    }

    function cancelEdit(button) {
    if (!button) {
        console.error('Button parameter is null');
        return;
    }
    var editRow = button.closest('.edit-row');
    if (!editRow) {
        console.error('Edit row not found');
        return;
    }
    editRow.classList.add('hidden');
    location.reload();
    }
</script>

<script>
    function showDeleteDialog() {
        const deleteDialog = document.getElementById('delete-dialog');
        deleteDialog.showModal();
    }

    function cancelDelete() {
        const deleteDialog = document.getElementById('delete-dialog');
        deleteDialog.close();
    }
    const selectAllCheckbox = document.getElementById('select-all-checkbox');
    const deleteRow = document.getElementById('delete-row');
    const dataList = document.getElementsByName('selected[]');
    const form = document.getElementById('delete-form');
    const deleteBtn = document.getElementById('delete-button');

    // Function to handle checkbox changes
    function handleCheckboxChange() {
        const checkedBoxes = document.querySelectorAll('input[name="selected[]"]:checked');
        const totalChecked = checkedBoxes.length; // Count the selected checkboxes

        if (totalChecked > 0) {
            deleteRow.classList.add('visible');
        } else {
            deleteRow.classList.remove('visible');
        }

        // Display the count somewhere in your UI, e.g., in a <span> element with the id "checkbox-count"
        const checkboxCountElement = document.getElementById('checkbox-count');
        if (checkboxCountElement) {
            checkboxCountElement.textContent = `${totalChecked} data terpilih`;
        }

        dataList.forEach((checkbox) => {
            checkbox.closest('tr').classList.remove('bg-red-100');
        });

        checkedBoxes.forEach((checkbox) => {
            checkbox.closest('tr').classList.add('bg-red-100');
        });
    }

    // Event listener for the "Select All" checkbox
    selectAllCheckbox.addEventListener('change', () => {
        dataList.forEach((checkbox) => {
            checkbox.checked = selectAllCheckbox.checked;
        });

        handleCheckboxChange();
    });

        dataList.forEach((checkbox) => {
        checkbox.addEventListener('change', () => {
            if (!checkbox.checked) {
                // If any individual checkbox is unchecked, uncheck the "Select All" checkbox
                selectAllCheckbox.checked = false;
            }

            handleCheckboxChange();
        });
    });

    // Event listeners for individual checkboxes
    dataList.forEach((checkbox) => {
        checkbox.addEventListener('change', () => {
            handleCheckboxChange();
        });
    });

    // Function to handle deleting selected rows
    deleteBtn.addEventListener('click', () => {
        const checkedBoxes = document.querySelectorAll('input[name="selected[]"]:checked');
        form.querySelectorAll('input[name="selected[]"]').forEach((c) => c.remove());
        checkedBoxes.forEach((checkBox) => {
            form.appendChild(checkBox.cloneNode());
        });
    });
</script>
@endsection
