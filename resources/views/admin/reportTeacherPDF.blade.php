<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

    <!-- Styles -->
    <style>
        body {
            font-family: 'Sans-serif';
            padding: 1rem;
        }

        .container {
            background-color: #FFFFFF;
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);
            border-radius: 0.375rem;
            padding: 2rem;
            margin-bottom: 2rem;
        }

        .header {
            border-bottom: 1px solid #000000;
            margin-bottom: 1rem;
        }

        .title {
            display: flex;
            align-items: center;
            margin: 0;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            text-align: left;
        }

        th, td {
            border: 1px solid #CBD5E0;
        }

        th {
            padding: 0.75rem;
            font-size: 0.75rem;
            font-weight: bold;
            text-transform: uppercase;
            white-space: nowrap;
            background-color: #E2E8F0;
        }

        td {
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="header">
            <h2 class="title">
                {{ __('Laporan agenda kelas ') }}<span class="inline font-bold">{{ $teacher->first()->teacher_name }} - {{ $teacher->first()->kode }}</span>{{ __(' (') }}<span class="inline font-bold">{{ $start_date }}</span>{{ __(' - ') }}<span class="inline font-bold">{{ $end_date }}{{ __(')') }}</span>
            </h2>
        </div>
        <table>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Hari</th>
                    <th>Tanggal</th>
                    <th>Kelas</th>
                    <th>Mata Pelajaran</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $sortedReport = collect($report)->flatMap(function ($subjectReport) {
                        return collect($subjectReport);
                    })->sortBy('date')->all();

                    $rowNumber = 1;
                @endphp

                @foreach($sortedReport as $subjectId => $item)
                    <tr>
                        <td>{{ $rowNumber++ }}</td>
                        <td>{{ \Carbon\Carbon::parse($item['date'])->locale('id')->isoFormat('dddd') }}</td>
                        <td>{{ \Carbon\Carbon::parse($item['date'])->format('d-m-Y') }}</td>
                        <td>{{ $item['class']->class_name }}</td>
                        <td>{{ $item['subject']->subject_name }}</td>
                        <td style="{{  $item['status'] == 'Tidak mengisi' ? 'background-color: #fee2e2;' : 'background-color: #dcfce7;' }}">{{ $item['status'] }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>
