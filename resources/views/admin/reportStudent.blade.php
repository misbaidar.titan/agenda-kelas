@extends('layouts.app')
@section('title', "Rekap Absensi Siswa | Agenda Kelas")
@section('reportStudent')
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="{{ asset('vendor/larapex-charts/app.js') }}"></script>
<div class="py-6">
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8 space-y-6">
        <nav class="flex items-center pl-4 sm:pl-8 text-gray-500 text-lg font-medium" aria-label="Breadcrumb">
            <ol class="flex items-center space-x-2">
                <li>
                    <a href="{{ route('dashboard.show', $id) }}" class="hover:text-gray-900">Dashboard</a>
                </li>
                <li>
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M5.707 14.707a1 1 0 1 1-1.414-1.414L7.586 10 4.293 6.707a1 1 0 1 1 1.414-1.414l4 4a1 1 0 0 1 0 1.414l-4 4z" clip-rule="evenodd" />
                    </svg>
                </li>
                <li>
                    <a href="#" class="text-gray-900">Rekap</a>
                </li>
            </ol>
        </nav>
        <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
            <div>
                <div class="border-b pb-3 mb-4 border-gray-400">
                    <h2 class="text-lg font-medium text-gray-900">
                        <div class="flex justify-between items-center">
                            <div>
                                {{ __('Rekap absensi siswa kelas ') }}<span class="inline font-bold">{{ $class->class_name }}</span>{{ __(' (') }}<span class="inline font-bold">{{ $startDate }}</span>{{ __(' - ') }}<span class="inline font-bold">{{ $endDate }}{{ __(')') }}</span>
                            </div>
                            <div align="right" class="self-center">
                                <a href="{{ route('dashboard.reportStudentPDF', ['id' => $id]) }}">
                                    <button class="btn-primary py-1 px-2 fill-white">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5" viewBox="0 0 512 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M0 64C0 28.7 28.7 0 64 0H224V128c0 17.7 14.3 32 32 32H384V304H176c-35.3 0-64 28.7-64 64V512H64c-35.3 0-64-28.7-64-64V64zm384 64H256V0L384 128zM176 352h32c30.9 0 56 25.1 56 56s-25.1 56-56 56H192v32c0 8.8-7.2 16-16 16s-16-7.2-16-16V448 368c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24H192v48h16zm96-80h32c26.5 0 48 21.5 48 48v64c0 26.5-21.5 48-48 48H304c-8.8 0-16-7.2-16-16V368c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H320v96h16zm80-112c0-8.8 7.2-16 16-16h48c8.8 0 16 7.2 16 16s-7.2 16-16 16H448v32h32c8.8 0 16 7.2 16 16s-7.2 16-16 16H448v48c0 8.8-7.2 16-16 16s-16-7.2-16-16V432 368z"/></svg>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </h2>
                </div>
                <div class="flex flex-col">
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="w-[1000px] overflow-x-auto m-auto">
                                <table class="min-w-full divide-y divide-gray-400 text-left">
                                    <tr class="bg-gray-100">
                                        <th class="px-1 py-3 border border-gray-400 text-center text-xs font-medium uppercase whitespace-nowrap">No</th>
                                        <th class="px-3 py-3 border border-gray-400 text-xs font-medium uppercase whitespace-nowrap">Kode</th>
                                        <th class="px-3 py-3 w-3/6  border border-gray-400 text-xs font-medium uppercase whitespace-nowrap">Nama</th>
                                        <th class="px-3 py-3 border border-gray-400 text-xs font-medium uppercase whitespace-nowrap">Sakit</th>
                                        <th class="px-3 py-3 border border-gray-400 text-xs font-medium uppercase whitespace-nowrap">Izin</th>
                                        <th class="px-3 py-3 border border-gray-400 text-xs font-medium uppercase whitespace-nowrap">Alpa</th>
                                        <th class="px-3 py-3 border border-gray-400 text-xs font-medium uppercase whitespace-nowrap">Total</th>
                                        <th class="border border-gray-400 text-xs font-medium uppercase whitespace-nowrap"></th>
                                    </tr>
                                    @foreach($students as $index => $student)
                                        <tr class="text-center">
                                            <td class="px-1 border text-center border-gray-400">{{ $index + 1 }}</td>
                                            <td class="px-3 border text-right border-gray-400">{{ $student->student_kode }}</td>
                                            <td class="px-3 border text-left border-gray-400">{{ $student->student_name }}</td>
                                            <td class="px-3 border border-gray-400">{{ $student->sakit_total }}<span class="text-xs ml-1 font-light">hari</span></td>
                                            <td class="px-3 border border-gray-400">{{ $student->izin_total }}<span class="text-xs ml-1 font-light">hari</span></td>
                                            <td class="px-3 border border-gray-400">{{ $student->alpa_total }}<span class="text-xs ml-1 font-light">hari</span></td>
                                            <td class="px-3 border border-gray-400">{{ $student->total }}<span class="text-xs ml-1 font-light">hari</span></td>
                                            <td class="border border-gray-400">
                                                <div class="justify-between">
                                                    <button data-hs-overlay="#hs-chart-modal{{ $student->student_kode }}" class="btn-primary p-1 fill-white">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="w-3" viewBox="0 0 512 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M32 32c17.7 0 32 14.3 32 32V400c0 8.8 7.2 16 16 16H480c17.7 0 32 14.3 32 32s-14.3 32-32 32H80c-44.2 0-80-35.8-80-80V64C0 46.3 14.3 32 32 32zM160 224c17.7 0 32 14.3 32 32v64c0 17.7-14.3 32-32 32s-32-14.3-32-32V256c0-17.7 14.3-32 32-32zm128-64V320c0 17.7-14.3 32-32 32s-32-14.3-32-32V160c0-17.7 14.3-32 32-32s32 14.3 32 32zm64 32c17.7 0 32 14.3 32 32v96c0 17.7-14.3 32-32 32s-32-14.3-32-32V224c0-17.7 14.3-32 32-32zM480 96V320c0 17.7-14.3 32-32 32s-32-14.3-32-32V96c0-17.7 14.3-32 32-32s32 14.3 32 32z"/></svg>
                                                    </button>
                                                    <button data-hs-overlay="#hs-date-modal{{ $student->student_kode }}" class="btn-primary p-1 fill-white">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="w-3" viewBox="0 0 512 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M64 256V160H224v96H64zm0 64H224v96H64V320zm224 96V320H448v96H288zM448 256H288V160H448v96zM64 32C28.7 32 0 60.7 0 96V416c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V96c0-35.3-28.7-64-64-64H64z"/></svg>
                                                     </button>
                                                </div>
                                                <div id="hs-chart-modal{{ $student->student_kode }}" class="hs-overlay hidden w-full h-full fixed top-0 left-0 z-[60] overflow-x-hidden overflow-y-auto">
                                                    <div class="hs-overlay-open:mt-7 hs-overlay-open:opacity-100 hs-overlay-open:duration-500 mt-0 opacity-0 ease-out transition-all sm:max-w-sm sm:w-full m-3 sm:mx-auto min-h-[calc(100%-3.5rem)] flex items-center">
                                                        <div class="min-w-full bg-white border p-4 border-gray-200 rounded-lg shadow">
                                                            <div class="flex justify-end items-center">
                                                                <button type="button" class="hs-dropdown-toggle inline-flex flex-shrink-0 justify-center items-center h-4 w-4 rounded-md text-gray-500 hover:text-gray-400 focus:outline-none focus:ring-2 focus:ring-gray-400 focus:ring-offset-2 focus:ring-offset-white transition-all text-sm" data-hs-overlay="#hs-chart-modal{{ $student->student_kode }}">
                                                                    <span class="sr-only">Close</span>
                                                                    <svg class="w-3.5 h-3.5" width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M0.258206 1.00652C0.351976 0.912791 0.479126 0.860131 0.611706 0.860131C0.744296 0.860131 0.871447 0.912791 0.965207 1.00652L3.61171 3.65302L6.25822 1.00652C6.30432 0.958771 6.35952 0.920671 6.42052 0.894471C6.48152 0.868271 6.54712 0.854471 6.61352 0.853901C6.67992 0.853321 6.74572 0.865971 6.80722 0.891111C6.86862 0.916251 6.92442 0.953381 6.97142 1.00032C7.01832 1.04727 7.05552 1.1031 7.08062 1.16454C7.10572 1.22599 7.11842 1.29183 7.11782 1.35822C7.11722 1.42461 7.10342 1.49022 7.07722 1.55122C7.05102 1.61222 7.01292 1.6674 6.96522 1.71352L4.31871 4.36002L6.96522 7.00648C7.05632 7.10078 7.10672 7.22708 7.10552 7.35818C7.10442 7.48928 7.05182 7.61468 6.95912 7.70738C6.86642 7.80018 6.74102 7.85268 6.60992 7.85388C6.47882 7.85498 6.35252 7.80458 6.25822 7.71348L3.61171 5.06702L0.965207 7.71348C0.870907 7.80458 0.744606 7.85498 0.613506 7.85388C0.482406 7.85268 0.357007 7.80018 0.264297 7.70738C0.171597 7.61468 0.119017 7.48928 0.117877 7.35818C0.116737 7.22708 0.167126 7.10078 0.258206 7.00648L2.90471 4.36002L0.258206 1.71352C0.164476 1.61976 0.111816 1.4926 0.111816 1.36002C0.111816 1.22744 0.164476 1.10028 0.258206 1.00652Z" fill="currentColor"/>
                                                                    </svg>
                                                                </button>
                                                            </div>
                                                            <div>
                                                                {!! $student->chart->container() !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="hs-date-modal{{ $student->student_kode }}" class="hs-overlay hidden w-full h-full fixed top-0 left-0 z-[60] overflow-x-hidden overflow-y-auto">
                                                    <div class="hs-overlay-open:mt-7 hs-overlay-open:opacity-100 hs-overlay-open:duration-500 mt-0 opacity-0 ease-out transition-all sm:max-w-sm sm:w-full m-3 sm:mx-auto min-h-[calc(100%-3.5rem)] flex items-center">
                                                        <div class="min-w-full bg-white border p-4 border-gray-200 rounded-lg shadow">
                                                            <div class="flex justify-end items-center mb-4">
                                                                <button type="button" class="hs-dropdown-toggle inline-flex flex-shrink-0 justify-center items-center h-4 w-4 rounded-md text-gray-500 hover:text-gray-400 focus:outline-none focus:ring-2 focus:ring-gray-400 focus:ring-offset-2 focus:ring-offset-white transition-all text-sm" data-hs-overlay="#hs-date-modal{{ $student->student_kode }}">
                                                                    <span class="sr-only">Close</span>
                                                                    <svg class="w-3.5 h-3.5" width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M0.258206 1.00652C0.351976 0.912791 0.479126 0.860131 0.611706 0.860131C0.744296 0.860131 0.871447 0.912791 0.965207 1.00652L3.61171 3.65302L6.25822 1.00652C6.30432 0.958771 6.35952 0.920671 6.42052 0.894471C6.48152 0.868271 6.54712 0.854471 6.61352 0.853901C6.67992 0.853321 6.74572 0.865971 6.80722 0.891111C6.86862 0.916251 6.92442 0.953381 6.97142 1.00032C7.01832 1.04727 7.05552 1.1031 7.08062 1.16454C7.10572 1.22599 7.11842 1.29183 7.11782 1.35822C7.11722 1.42461 7.10342 1.49022 7.07722 1.55122C7.05102 1.61222 7.01292 1.6674 6.96522 1.71352L4.31871 4.36002L6.96522 7.00648C7.05632 7.10078 7.10672 7.22708 7.10552 7.35818C7.10442 7.48928 7.05182 7.61468 6.95912 7.70738C6.86642 7.80018 6.74102 7.85268 6.60992 7.85388C6.47882 7.85498 6.35252 7.80458 6.25822 7.71348L3.61171 5.06702L0.965207 7.71348C0.870907 7.80458 0.744606 7.85498 0.613506 7.85388C0.482406 7.85268 0.357007 7.80018 0.264297 7.70738C0.171597 7.61468 0.119017 7.48928 0.117877 7.35818C0.116737 7.22708 0.167126 7.10078 0.258206 7.00648L2.90471 4.36002L0.258206 1.71352C0.164476 1.61976 0.111816 1.4926 0.111816 1.36002C0.111816 1.22744 0.164476 1.10028 0.258206 1.00652Z" fill="currentColor"/>
                                                                    </svg>
                                                                </button>
                                                            </div>
                                                            <table class="min-w-full divide-y divide-gray-400 text-left">
                                                                <thead>
                                                                    <tr class="bg-gray-100">
                                                                        <th class="px-3 py-3 border border-gray-400 text-xs font-medium uppercase whitespace-nowrap">Tanggal</th>
                                                                        <th class="px-3 py-3 border border-gray-400 text-xs font-medium uppercase whitespace-nowrap">Alasan</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach($student->absentStudents as $absentStudent)
                                                                        <tr>
                                                                            <td class="px-3 py-3 border border-gray-400 text-sm">
                                                                                {{ \Carbon\Carbon::parse($absentStudent->dailyAgenda->date)->format('d-m-Y') }}
                                                                            </td>
                                                                            <td class="px-3 py-3 border border-gray-400 text-sm">
                                                                                {{ $absentStudent->reason }}
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@foreach($students as $student)
    {!! $student->chart->script() !!}
@endforeach

@endsection
