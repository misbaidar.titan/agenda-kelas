<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

    <!-- Styles -->
    <style>
        body {
            font-family: 'Sans-serif';
            padding: 1rem;
        }

        .container {
            background-color: #FFFFFF;
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);
            border-radius: 0.375rem;
            padding: 2rem;
            margin-bottom: 2rem;
        }

        .header {
            border-bottom: 1px solid #000000;
            margin-bottom: 1rem;
        }

        .title {
            display: flex;
            align-items: center;
            margin: 0;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            text-align: left;
        }

        th, td {
            border: 1px solid #CBD5E0;
        }

        th {
            padding: 0.75rem;
            font-size: 0.75rem;
            font-weight: bold;
            text-transform: uppercase;
            white-space: nowrap;
            background-color: #E2E8F0;
        }

        td {
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="header">
            <h2 class="title">
                {{ __('Rekap absensi siswa kelas ') }}<span class="inline font-bold">{{ $class->class_name }}</span>{{ __(' (') }}<span class="inline font-bold">{{ $startDate }}</span>{{ __(' - ') }}<span class="inline font-bold">{{ $endDate }}{{ __(')') }}</span>
            </h2>
        </div>
        <table>
            <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Nama</th>
                <th>Sakit</th>
                <th>Izin</th>
                <th>Alpa</th>
                <th>Total</th>
            </tr>
            @foreach($students as $index => $student)
                <tr>
                    <td>{{ $index + 1 }}</td>
                    <td>{{ $student->student_kode }}</td>
                    <td style="text-align: left;">{{ $student->student_name }}</td>
                    <td>{{ $student->sakit_total }}<span style="font-size: 0.625rem; margin-left: 0.125rem; font-weight: lighter;">hari</span></td>
                    <td>{{ $student->izin_total }}<span style="font-size: 0.625rem; margin-left: 0.125rem; font-weight: lighter;">hari</span></td>
                    <td>{{ $student->alpa_total }}<span style="font-size: 0.625rem; margin-left: 0.125rem; font-weight: lighter;">hari</span></td>
                    <td>{{ $student->total }}<span style="font-size: 0.625rem; margin-left: 0.125rem; font-weight: lighter;">hari</span></td>
                </tr>
            @endforeach
        </table>
    </div>
</body>
</html>
