@extends('layouts.app')
@section('title', "Dashboard | Agenda Kelas")
@section('dashboard')
<div class="py-6">
@if (session('success'))
    <div class="fixed left-1/2 -translate-x-1/2 z-50 message-component flex bg-green-100 border border-green-400 text-green-700 px-2 py-3 shadow-lg rounded" role="alert">
        <div>
            <strong class="font-bold">Selamat!</strong>
            <span class="block sm:inline mr-4">{{ session('success') }}</span>
        </div>
        <div class="flex items-center">
            <button type="button" onclick="this.closest('.message-component').remove()">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-green-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                </svg>
            </button>
        </div>
    </div>
@endif
@if($errors->any())
    <div class="fixed left-1/2 -translate-x-1/2 z-50 message-component flex bg-red-100 border border-red-400 text-red-700 px-2 py-3 shadow-lg rounded" role="alert">
        <div>
            <strong class="font-bold">Oops!</strong>
            <span class="block sm:inline">{{ $errors->first() }}</span>
        </div>
        <div class="flex items-center">
            <button type="button" onclick="this.closest('.message-component').remove()">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                </svg>
            </button>
        </div>
    </div>
@endif
<div id="hs-edit-modal" class="hs-overlay hidden w-full h-full fixed top-0 left-0 z-[60] overflow-x-hidden overflow-y-auto">
    <div class="hs-overlay-open:mt-7 hs-overlay-open:opacity-100 hs-overlay-open:duration-500 mt-0 opacity-0 ease-out transition-all sm:max-w-lg sm:w-full mb-6 sm:mx-auto">
        <form id="edit-form" action="{{ route('dashboard.update', ['id' => $id]) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="p-6 bg-white border border-gray-200 rounded-lg shadow">
            <div class="border-b pb-3 mb-3 border-gray-400">
                <h2 class="text-xl text-left font-bold">Edit Profil</h2>
            </div>
            <div class="mb-1">
                <label for="picture" class="block text-gray-700 font-bold">Foto Profil</label>
                @if (Auth::user()->picture)
                    <img src="{{ asset('storage/pictures/'.Auth::user()->picture) }}" alt="picture" class="mt-2 object-cover w-24 rounded-md">
                    <div class="my-2 flex items-center space-x-1">
                        <input type="checkbox" name="remove_picture" id="remove_picture" class="hover:cursor-pointer" value="1">
                        <label for="remove_picture" class="text-sm text-gray-700 hover:cursor-pointer">Hapus foto saat ini</label>
                    </div>
                @endif
                <input type="file" name="picture" id="picture" accept="image/*" class="input-text-primary mb-2 hover:cursor-pointer" accept="image/*">
            </div>
            <div class="mb-1">
                <label class="block text-gray-700 font-bold " for="email">
                Email
                </label>
                <input class="input-text-primary my-2 w-full" id="email" name="email" type="email" value="{{ Auth::user()->email }}">
            </div>
            <div class="mb-1">
                <label class="block text-gray-700 font-bold " for="name">
                Nama Pengelola
                </label>
                <input class="input-text-primary my-2 w-full" id="name" name="name" type="text" value="{{ Auth::user()->name }}">
            </div>
            <div class="mb-1">
                <label class="block text-gray-700 font-bold " for="school_name">
                Nama Sekolah
                </label>
                <input class="input-text-primary my-2 w-full" id="school_name" name="school_name" type="text" value="{{ Auth::user()->school_name }}">
            </div>
            <div class="mb-4">
                <label class="block text-gray-700 font-bold " for="password">
                Reset Password
                </label>
                <input class="input-text-primary my-2 w-full" id="current_password" name="current_password" type="password" placeholder="Password sekarang">
                <input class="input-text-primary my-2 w-full" id="new_password" name="new_password" type="password" placeholder="Password baru">
            </div>
            <div class="flex items-center justify-end space-x-2">
                <button class="btn-secondary" onclick="location.reload();" type="button" data-hs-overlay="#hs-edit-modal">
                Batal
                </button>
                <button class="btn-primary" type="submit">
                Simpan
                </button>
            </div>
        </div>
        </form>
    </div>
</div>
<div id="hs-teacher-modal" class="hs-overlay hidden w-full h-full fixed top-0 left-0 z-[60] overflow-x-hidden overflow-y-auto">
    <div class="hs-overlay-open:mt-7 hs-overlay-open:opacity-100 hs-overlay-open:duration-500 mt-0 opacity-0 ease-out transition-all sm:max-w-md sm:w-full m-3 sm:mx-auto min-h-[calc(100%-3.5rem)] flex items-center">
        <div class="min-w-full bg-white border p-4 border-gray-200 rounded-lg shadow">
            <div class="flex justify-end items-center">
              <button type="button" class="hs-dropdown-toggle inline-flex flex-shrink-0 justify-center items-center h-4 w-4 rounded-md text-gray-500 hover:text-gray-400 focus:outline-none focus:ring-2 focus:ring-gray-400 focus:ring-offset-2 focus:ring-offset-white transition-all text-sm" data-hs-overlay="#hs-teacher-modal">
                <span class="sr-only">Close</span>
                <svg class="w-3.5 h-3.5" width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M0.258206 1.00652C0.351976 0.912791 0.479126 0.860131 0.611706 0.860131C0.744296 0.860131 0.871447 0.912791 0.965207 1.00652L3.61171 3.65302L6.25822 1.00652C6.30432 0.958771 6.35952 0.920671 6.42052 0.894471C6.48152 0.868271 6.54712 0.854471 6.61352 0.853901C6.67992 0.853321 6.74572 0.865971 6.80722 0.891111C6.86862 0.916251 6.92442 0.953381 6.97142 1.00032C7.01832 1.04727 7.05552 1.1031 7.08062 1.16454C7.10572 1.22599 7.11842 1.29183 7.11782 1.35822C7.11722 1.42461 7.10342 1.49022 7.07722 1.55122C7.05102 1.61222 7.01292 1.6674 6.96522 1.71352L4.31871 4.36002L6.96522 7.00648C7.05632 7.10078 7.10672 7.22708 7.10552 7.35818C7.10442 7.48928 7.05182 7.61468 6.95912 7.70738C6.86642 7.80018 6.74102 7.85268 6.60992 7.85388C6.47882 7.85498 6.35252 7.80458 6.25822 7.71348L3.61171 5.06702L0.965207 7.71348C0.870907 7.80458 0.744606 7.85498 0.613506 7.85388C0.482406 7.85268 0.357007 7.80018 0.264297 7.70738C0.171597 7.61468 0.119017 7.48928 0.117877 7.35818C0.116737 7.22708 0.167126 7.10078 0.258206 7.00648L2.90471 4.36002L0.258206 1.71352C0.164476 1.61976 0.111816 1.4926 0.111816 1.36002C0.111816 1.22744 0.164476 1.10028 0.258206 1.00652Z" fill="currentColor"/>
                </svg>
              </button>
            </div>
            <div class="border-b pb-2 mb-1 border-gray-400">
                <h2 class="text-lg font-medium text-gray-900">
                    Laporan agenda kelas guru
                </h2>
            </div>
            <form action="{{ route('dashboard.reportTeacher',[$id]) }}" method="GET">
                @csrf
                <label for="start_date_teacher" class="block font-medium text-sm text-gray-700 self-center mr-2 my-2">Tanggal</label>
                <div class="flex mb-1">
                    <input type="date" class="input-text-primary hover:cursor-text focus:outline-none focus:shadow-outline" id="start_date_teacher" name="start_date_teacher" value="{{ request('start_date_teacher') }}" required>
                    <label for="end_date_teacher" class="block font-medium text-sm text-gray-700 self-center mx-2">sampai</label>
                    <input type="date" class="input-text-primary hover:cursor-text focus:outline-none focus:shadow-outline" id="end_date_teacher" name="end_date_teacher" value="{{ request('end_date_teacher') }}" @if(request()->has('start_date_teacher')) {{ 'min='.request('start_date_teacher') }} @else {{ 'disabled' }} @endif required>
                </div>
                <label for="selected_guru" class="block font-medium text-sm text-gray-700 self-center mr-2 my-2">Guru</label>
                <div class="relative w-48">
                    <select class="w-full py-2 input-text-primary" style="-webkit-appearance: none;" name="selected_guru" required>
                        <option value="" selected>Pilih guru</option>
                        @foreach ($teachers as $teacher)
                            <option value="{{ $teacher->id }}" {{ request('selected_guru') == $teacher->id }}>
                                {{ $teacher->teacher_name }}
                                 - {{ $teacher->kode }}
                            </option>
                        @endforeach
                    </select>
                    <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                        <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                        </svg>
                    </div>
                </div>
                <div class="flex justify-end items-center">
                    <button type="submit" class="btn-primary">
                        <svg xmlns="http://www.w3.org/2000/svg" class="fill-white w-3 mr-2" viewBox="0 0 384 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V160H256c-17.7 0-32-14.3-32-32V0H64zM256 0V128H384L256 0zM112 256H272c8.8 0 16 7.2 16 16s-7.2 16-16 16H112c-8.8 0-16-7.2-16-16s7.2-16 16-16zm0 64H272c8.8 0 16 7.2 16 16s-7.2 16-16 16H112c-8.8 0-16-7.2-16-16s7.2-16 16-16zm0 64H272c8.8 0 16 7.2 16 16s-7.2 16-16 16H112c-8.8 0-16-7.2-16-16s7.2-16 16-16z"/></svg>
                        cetak
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="hs-student-modal" class="hs-overlay hidden w-full h-full fixed top-0 left-0 z-[60] overflow-x-hidden overflow-y-auto">
    <div class="hs-overlay-open:mt-7 hs-overlay-open:opacity-100 hs-overlay-open:duration-500 mt-0 opacity-0 ease-out transition-all sm:max-w-md sm:w-full m-3 sm:mx-auto min-h-[calc(100%-3.5rem)] flex items-center">
        <div class="min-w-full bg-white border p-4 border-gray-200 rounded-lg shadow">
            <div class="flex justify-end items-center">
                <button type="button" class="hs-dropdown-toggle inline-flex flex-shrink-0 justify-center items-center h-4 w-4 rounded-md text-gray-500 hover:text-gray-400 focus:outline-none focus:ring-2 focus:ring-gray-400 focus:ring-offset-2 focus:ring-offset-white transition-all text-sm" data-hs-overlay="#hs-student-modal">
                    <span class="sr-only">Close</span>
                    <svg class="w-3.5 h-3.5" width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.258206 1.00652C0.351976 0.912791 0.479126 0.860131 0.611706 0.860131C0.744296 0.860131 0.871447 0.912791 0.965207 1.00652L3.61171 3.65302L6.25822 1.00652C6.30432 0.958771 6.35952 0.920671 6.42052 0.894471C6.48152 0.868271 6.54712 0.854471 6.61352 0.853901C6.67992 0.853321 6.74572 0.865971 6.80722 0.891111C6.86862 0.916251 6.92442 0.953381 6.97142 1.00032C7.01832 1.04727 7.05552 1.1031 7.08062 1.16454C7.10572 1.22599 7.11842 1.29183 7.11782 1.35822C7.11722 1.42461 7.10342 1.49022 7.07722 1.55122C7.05102 1.61222 7.01292 1.6674 6.96522 1.71352L4.31871 4.36002L6.96522 7.00648C7.05632 7.10078 7.10672 7.22708 7.10552 7.35818C7.10442 7.48928 7.05182 7.61468 6.95912 7.70738C6.86642 7.80018 6.74102 7.85268 6.60992 7.85388C6.47882 7.85498 6.35252 7.80458 6.25822 7.71348L3.61171 5.06702L0.965207 7.71348C0.870907 7.80458 0.744606 7.85498 0.613506 7.85388C0.482406 7.85268 0.357007 7.80018 0.264297 7.70738C0.171597 7.61468 0.119017 7.48928 0.117877 7.35818C0.116737 7.22708 0.167126 7.10078 0.258206 7.00648L2.90471 4.36002L0.258206 1.71352C0.164476 1.61976 0.111816 1.4926 0.111816 1.36002C0.111816 1.22744 0.164476 1.10028 0.258206 1.00652Z" fill="currentColor"/>
                    </svg>
                </button>
            </div>
            <div class="border-b pb-2 mb-1 border-gray-400">
                <h2 class="text-lg font-medium text-gray-900">
                    Rekap absensi siswa
                </h2>
            </div>
            <form action="{{ route('dashboard.reportStudent',[$id]) }}" method="GET">
                @csrf
                <label for="start_date" class="block font-medium text-sm text-gray-700 self-center mr-2 my-2">Tanggal</label>
                <div class="flex mb-1">
                    <input type="date" class="input-text-primary hover:cursor-text focus:outline-none focus:shadow-outline" id="start_date" name="start_date" value="{{ request('start_date') }}" required>
                    <label for="end_date" class="block font-medium text-sm text-gray-700 self-center mx-2">sampai</label>
                    <input type="date" class="input-text-primary hover:cursor-text focus:outline-none focus:shadow-outline" id="end_date" name="end_date" value="{{ request('end_date') }}" @if(request()->has('start_date')) {{ 'min='.request('start_date') }} @else {{ 'disabled' }} @endif required>
                </div>
                <label for="selected_class" class="block font-medium text-sm text-gray-700 self-center mr-2 my-2">Kelas</label>
                <div class="relative w-48">
                    <select class="w-full py-2 input-text-primary" style="-webkit-appearance: none;" name="selected_class" required>
                        <option value="" selected>Pilih kelas</option>
                        @foreach ($classes as $class)
                            <option value="{{ $class->id }}" {{ request('selected_class') == $class->id || request('class_id') == $class->id ? 'selected' : '' }}>
                                {{ $class->class_name }}
                            </option>
                        @endforeach
                    </select>
                    <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                        <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                        </svg>
                    </div>
                </div>
                <div class="flex justify-end items-center">
                    <button type="submit" class="btn-primary">
                        <svg xmlns="http://www.w3.org/2000/svg" class="fill-white w-3 mr-2" viewBox="0 0 384 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V160H256c-17.7 0-32-14.3-32-32V0H64zM256 0V128H384L256 0zM112 256H272c8.8 0 16 7.2 16 16s-7.2 16-16 16H112c-8.8 0-16-7.2-16-16s7.2-16 16-16zm0 64H272c8.8 0 16 7.2 16 16s-7.2 16-16 16H112c-8.8 0-16-7.2-16-16s7.2-16 16-16zm0 64H272c8.8 0 16 7.2 16 16s-7.2 16-16 16H112c-8.8 0-16-7.2-16-16s7.2-16 16-16z"/></svg>
                        cetak
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8 space-y-6">
        <nav class="flex items-center pl-4 sm:pl-8 text-gray-500 text-lg font-medium" aria-label="Breadcrumb">
            <ol class="flex items-center space-x-2">
            <li>
                <a href="{{ route('dashboard.show', $id) }}" class="text-gray-900">Dashboard</a>
            </li>
            </ol>
        </nav>
        <div class="sm:flex sm:gap-4 space-y-4 sm:space-y-0">
            <div class="flex flex-col sm:w-[250px] gap-4">
                <div class="p-4 bg-white sm:mb-0 sm:rounded-lg shadow" style="color:#949494">
                    <div class="flex justify-end">
                        <div class="hs-dropdown [--placement:right-top]">
                            <button id="hs-dropdown-edit" class="hs-dropdown-toggle inline-flex flex-shrink-0 justify-center items-center rounded-md text-gray-500 hover:text-gray-400 focus:outline-none focus:ring-2 focus:ring-gray-400 focus:ring-offset-2 focus:ring-offset-white transition-all text-sm">
                                <img src="{{ asset('images/there-dots.svg') }}" class="w-4 h-4" alt="picture">
                            </button>
                            <div class="hs-dropdown-menu transition-[opacity,margin] duration hs-dropdown-open:opacity-100 opacity-0 z-10 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none hidden" aria-labelledby="hs-dropdown-edit">
                                <button class="block text-left w-full px-4 py-2 text-sm hover:bg-gray-100 text-gray-700" type="submit" data-hs-overlay="#hs-edit-modal">Edit</button>
                            </div>
                        </div>
                    </div>
                    <div class="row-start-1 flex flex-col items-center overflow-hidden">
                        @if (Auth::user()->picture)
                            <img src="{{ asset('storage/pictures/'.Auth::user()->picture) }}" class="object-cover w-24 rounded-md mb-3" alt="picture">
                        @else
                        <img src="{{ asset('images/default-profile.svg') }}" alt="Default Profile Photo" class="object-cover w-24 h-24 mb-3 rounded-full shadow-lg">
                        @endif
                        <div class="self-start">
                            <label class="text-sm  text-gray-500">Sekolah</label>
                            <p class="text-xl text-gray-800 mb-5">{{ Auth::user()->school_name }}</p>
                            <label class="text-sm  text-gray-500">Pengelola</label>
                            <p class="text-xl  text-gray-800 mb-5">{{ Auth::user()->name }}</p>
                            <label class="text-sm  text-gray-500">Email</label>
                            <div class="sm:max-w-[90%]">
                                <p class="text-sm text-gray-800 whitespace-nowrap text-ellipsis overflow-hidden" title="{{ Auth::user()->email }}">{{ Auth::user()->email }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="p-4 bg-white sm:mb-0 sm:rounded-lg shadow" style="color:#949494">
                    <div class="border-b pb-3 mb-4 border-gray-400">
                        <h2 class="text-lg font-medium text-gray-900">
                            {{ __('Laporan ') }}
                        </h2>
                    </div>
                    <div class="flex flex-col gap-2">
                        <div>
                            <button type="button" class="btn-primary w-full justify-center" data-hs-overlay="#hs-teacher-modal">Laporan Guru</button>
                        </div>
                        <div>
                            <button type="button" class="btn-primary w-full justify-center" data-hs-overlay="#hs-student-modal">Rekap Absensi Siswa</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex-grow flex-col space-y-4">
                <div class="flex flex-col sm:flex-row gap-4">
                    <div class="sm:w-full p-4 group sm:border-0 border-b border-gray-300 bg-white shadow sm:rounded-lg">
                        <div class="bg-sky-700 h-20 fill-white mb-2 rounded-lg flex items-center justify-center shadow group-hover:shadow-sky-700 group-hover:transform group-hover:-translate-y-0.5 group-hover:shadow-md transition-all">
                            <svg xmlns="http://www.w3.org/2000/svg" class="w-10" viewBox="0 0 448 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M224 256A128 128 0 1 0 224 0a128 128 0 1 0 0 256zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H418.3c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304H178.3z"/></svg>
                        </div>
                        <div class="flex justify-between items-center ">
                            <div class="">
                                <p class="font-bold text-4xl">{{ $totalTeachers }}</p>
                                <span class="text-sm font-semibold whitespace-nowrap">Total Guru</span>
                            </div>
                            <a href="{{ route('teachers.showGuru', $id) }}" class="flex items-center text-primary">
                                <button class="flex items-center justify-center rounded-md text-gray-500 hover:bg-gray-200 hover:p-1.5 focus:outline-none focus:ring-2 focus:ring-gray-400 focus:ring-offset-2 focus:ring-offset-white transition-all text-sm">
                                    <img src="{{ asset('images/link.svg') }}" alt="pilih">
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="sm:w-full p-4 group sm:border-0 border-b border-gray-300 bg-white shadow sm:rounded-lg">
                        <div class="bg-rose-700 h-20 fill-white mb-2 rounded-lg flex items-center justify-center shadow group-hover:shadow-rose-700 group-hover:transform group-hover:-translate-y-0.5 group-hover:shadow-md transition-all">
                            <svg xmlns="http://www.w3.org/2000/svg" class="w-10" viewBox="0 0 448 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M96 0C43 0 0 43 0 96V416c0 53 43 96 96 96H384h32c17.7 0 32-14.3 32-32s-14.3-32-32-32V384c17.7 0 32-14.3 32-32V32c0-17.7-14.3-32-32-32H384 96zm0 384H352v64H96c-17.7 0-32-14.3-32-32s14.3-32 32-32zm32-240c0-8.8 7.2-16 16-16H336c8.8 0 16 7.2 16 16s-7.2 16-16 16H144c-8.8 0-16-7.2-16-16zm16 48H336c8.8 0 16 7.2 16 16s-7.2 16-16 16H144c-8.8 0-16-7.2-16-16s7.2-16 16-16z"/></svg>
                        </div>
                        <div class="flex justify-between items-center ">
                            <div class="">
                                <p class="font-bold text-4xl">{{ $totalSubjects }}</p>
                                <span class="text-sm font-semibold whitespace-nowrap">Total Mata Pelajaran</span>
                            </div>
                            <a href="{{ route('subject.show', $id) }}" class="flex items-center text-primary">
                                <button class="flex items-center justify-center rounded-md text-gray-500 hover:bg-gray-200 hover:p-1.5 focus:outline-none focus:ring-2 focus:ring-gray-400 focus:ring-offset-2 focus:ring-offset-white transition-all text-sm">
                                    <img src="{{ asset('images/link.svg') }}" alt="pilih">
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="sm:w-full p-4 group sm:border-0 border-b border-gray-300 bg-white shadow sm:rounded-lg">
                        <div class="bg-teal-700 h-20 fill-white mb-2 rounded-lg flex items-center justify-center shadow group-hover:shadow-teal-700 group-hover:transform group-hover:-translate-y-0.5 group-hover:shadow-md transition-all">
                            <svg xmlns="http://www.w3.org/2000/svg" class="w-10" viewBox="0 0 640 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M320 32c-8.1 0-16.1 1.4-23.7 4.1L15.8 137.4C6.3 140.9 0 149.9 0 160s6.3 19.1 15.8 22.6l57.9 20.9C57.3 229.3 48 259.8 48 291.9v28.1c0 28.4-10.8 57.7-22.3 80.8c-6.5 13-13.9 25.8-22.5 37.6C0 442.7-.9 448.3 .9 453.4s6 8.9 11.2 10.2l64 16c4.2 1.1 8.7 .3 12.4-2s6.3-6.1 7.1-10.4c8.6-42.8 4.3-81.2-2.1-108.7C90.3 344.3 86 329.8 80 316.5V291.9c0-30.2 10.2-58.7 27.9-81.5c12.9-15.5 29.6-28 49.2-35.7l157-61.7c8.2-3.2 17.5 .8 20.7 9s-.8 17.5-9 20.7l-157 61.7c-12.4 4.9-23.3 12.4-32.2 21.6l159.6 57.6c7.6 2.7 15.6 4.1 23.7 4.1s16.1-1.4 23.7-4.1L624.2 182.6c9.5-3.4 15.8-12.5 15.8-22.6s-6.3-19.1-15.8-22.6L343.7 36.1C336.1 33.4 328.1 32 320 32zM128 408c0 35.3 86 72 192 72s192-36.7 192-72L496.7 262.6 354.5 314c-11.1 4-22.8 6-34.5 6s-23.5-2-34.5-6L143.3 262.6 128 408z"/></svg>
                        </div>
                        <div class="flex justify-between items-center ">
                            <div class="">
                                <p class="font-bold text-4xl">{{ $totalStudents }}</p>
                                <span class="text-sm font-semibold whitespace-nowrap">Total Siswa</span>
                            </div>
                            <a href="{{ route('student.show', $id) }}" class="flex items-center text-primary">
                                <button class="flex items-center justify-center rounded-md text-gray-500 hover:bg-gray-200 hover:p-1.5 focus:outline-none focus:ring-2 focus:ring-gray-400 focus:ring-offset-2 focus:ring-offset-white transition-all text-sm">
                                    <img src="{{ asset('images/link.svg') }}" alt="pilih">
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="sm:w-full p-4 group sm:border-0 border-b border-gray-300 bg-white shadow sm:rounded-lg">
                        <div class="bg-yellow-700 h-20 fill-white mb-2 rounded-lg flex items-center justify-center shadow group-hover:shadow-yellow-700 group-hover:transform group-hover:-translate-y-0.5 group-hover:shadow-md transition-all">
                            <svg xmlns="http://www.w3.org/2000/svg" class="w-10" viewBox="0 0 576 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M96 32C60.7 32 32 60.7 32 96V384H96V96l384 0V384h64V96c0-35.3-28.7-64-64-64H96zM224 384v32H32c-17.7 0-32 14.3-32 32s14.3 32 32 32H544c17.7 0 32-14.3 32-32s-14.3-32-32-32H416V384c0-17.7-14.3-32-32-32H256c-17.7 0-32 14.3-32 32z"/></svg>
                        </div>
                        <div class="flex justify-between items-center ">
                            <div class="">
                                <p class="font-bold text-4xl">{{ $totalClasses }}</p>
                                <span class="text-sm font-semibold whitespace-nowrap">Total Kelas</span>
                            </div>
                            <a href="{{ route('class.show', $id) }}" class="flex items-center text-primary">
                                <button class="flex items-center justify-center rounded-md text-gray-500 hover:bg-gray-200 hover:p-1.5 focus:outline-none focus:ring-2 focus:ring-gray-400 focus:ring-offset-2 focus:ring-offset-white transition-all text-sm">
                                    <img src="{{ asset('images/link.svg') }}" alt="pilih">
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="flex flex-col p-4 bg-white w-full sm:rounded-lg shadow">
                    <div class="border-b flex justify-between items-center pb-3 mb-4 border-gray-400">
                        <h2 class="text-lg font-medium text-gray-900">
                            Jadwal agenda guru hari ini
                        </h2>
                        <div class="relative w-fit">
                            <select class="w-full pl-3 pr-8 input-text-primary" style="-webkit-appearance: none;" id="classFilter" required>
                                <option value="" selected>Semua kelas</option>
                                @php
                                    $uniqueClasses = $schedules->unique('class.id');
                                @endphp

                                @foreach ($uniqueClasses as $schedule)
                                    <option value="{{ $schedule->class->class_name }}">
                                        {{ $schedule->class->class_name }}
                                    </option>
                                @endforeach
                            </select>
                            <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                                <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="space-y-2">
                        @foreach($schedules as $schedule)
                        <div role="list" data-class="{{ $schedule->class->class_name }}" class="schedule divide-y px-4 divide-gray-100 rounded-lg border border-gray-300 hover:transform hover:-translate-y-1 hover:shadow-md transition-all">
                            <div class="flex justify-between gap-x-6 py-5">
                                <div class="flex min-w-0 gap-x-4">
                                @if ($schedule->teacher->picture)
                                <img class="object-cover h-12 w-12 flex-none rounded-full bg-gray-50" src="{{ asset('storage/pictures/'.$schedule->teacher->picture) }}" alt="">
                                @else
                                <img src="{{ asset('images/default-profile.svg') }}" alt="Default Profile Photo" class="object-cover h-12 w-12 flex-none rounded-full bg-gray-50">
                                @endif
                                <div class="min-w-0 flex-auto">
                                    <div class="flex flex-wrap items-center gap-2">
                                        <a href="{{ route('teachers.cariGuru', [$id, 'search' => $schedule->teacher->teacher_name]) }}" class="hover:underline">
                                            <p class="text-sm font-semibold leading-6 text-gray-900">{{ $schedule->teacher->teacher_name }}</p>
                                        </a>
                                        <span class="flex-shrink-0 w-max-content rounded-md px-2 text-xs font-thin leading-loose border {{ $schedule->status === 'Belum mengisi' ? 'bg-red-100 text-red-700 border-red-500/40' : ($schedule->status === 'Sudah mengisi' ? 'bg-green-100 text-green-700 border-green-500/40' : '') }}">{{ $schedule->status }}</span>
                                    </div>
                                    <p class="mt-1 truncate text-xs leading-5 text-gray-500">{{ $schedule->subject->subject_name }}</p>
                                </div>
                                </div>
                                <div class="hidden shrink-0 sm:flex sm:flex-col sm:items-center">
                                <p class="text-sm leading-6 text-gray-900">{{ $schedule->class->class_name }}</p>
                                <p class="mt-1 text-xs leading-5 text-gray-500">
                                    Jam pelajaran ke
                                    @if ($schedule->start_hour === $schedule->end_hour)
                                        {{ $schedule->start_hour }}
                                    @else
                                        {{ $schedule->start_hour }} sampai {{ $schedule->end_hour }}
                                    @endif
                                </p>                                </div>
                                <div class="shrink-0 self-center">
                                @if ($schedule->status === 'Sudah mengisi')
                                    <a href="{{ route('daily-content.show', [$id, 'idClass' => $schedule->class->id, 'idDay' => $schedule->day_id->id]) }}" class="flex items-center justify-center rounded-md text-gray-500 hover:text-gray-400 focus:outline-none focus:ring-2 focus:ring-gray-400 focus:ring-offset-2 focus:ring-offset-white transition-all text-sm">
                                        <img src="{{ asset('images/link.svg') }}" alt="pilih">
                                    </a>
                                @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.getElementById('start_date').addEventListener('change', function() {
        document.getElementById('end_date').disabled = !this.value
        if (!this.value) {
            document.getElementById('end_date').value = ''
        } else {
            document.getElementById('end_date').min = this.value
        }
    });
    document.getElementById('start_date_teacher').addEventListener('change', function() {
        document.getElementById('end_date_teacher').disabled = !this.value
        if (!this.value) {
            document.getElementById('end_date_teacher').value = ''
        } else {
            document.getElementById('end_date_teacher').min = this.value
        }
    });
    document.getElementById('classFilter').addEventListener('change', function() {
        var selectedClass = this.value;
        var schedules = document.querySelectorAll('.schedule');

        schedules.forEach(function(schedule) {
            var scheduleClass = schedule.getAttribute('data-class');
            schedule.style.display = (selectedClass === '' || selectedClass === scheduleClass) ? 'block' : 'none';
        });
    });
</script>
@endsection
