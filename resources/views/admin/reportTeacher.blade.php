@extends('layouts.app')
@section('title', "Laporan Guru | Agenda Kelas")
@section('reportGuru')
<div class="py-6">
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8 space-y-6">
        <nav class="flex items-center pl-4 sm:pl-8 text-gray-500 text-lg font-medium" aria-label="Breadcrumb">
            <ol class="flex items-center space-x-2">
                <li>
                    <a href="{{ route('dashboard.show', $id) }}" class="hover:text-gray-900">Dashboard</a>
                </li>
                <li>
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M5.707 14.707a1 1 0 1 1-1.414-1.414L7.586 10 4.293 6.707a1 1 0 1 1 1.414-1.414l4 4a1 1 0 0 1 0 1.414l-4 4z" clip-rule="evenodd" />
                    </svg>
                </li>
                <li>
                    <a href="#" class="text-gray-900">Laporan</a>
                </li>
            </ol>
        </nav>
        <div class="sm:flex sm:gap-4 space-y-4 sm:space-y-0">
            <div class="flex flex-col sm:gap-4 sm:min-w-fit sm:h-full justify-between">
                <div class="p-6 w-full sm:max-w-sm bg-white border border-gray-200 rounded-lg shadow">
                    @foreach ($teacher as $t)
                    <div class="flex flex-col items-center pb-4">
                        @if ($t->picture)
                            <img src="{{ asset('storage/pictures/'.$t->picture) }}" class="object-cover w-24 h-24 mb-3 rounded-full shadow-lg" alt="picture">
                        @else
                        <img src="{{ asset('images/default-profile.svg') }}" alt="Default Profile Photo" class="object-cover w-24 h-24 mb-3 rounded-full shadow-lg">
                        @endif
                    </div>
                    <table class="min-w-full divide-y divide-gray-400 text-left">
                        <tbody>
                            <tr>
                                <td class="px-3 py-3 border border-gray-400 text-sm">
                                    <div>
                                        <p class="font-light text-gray-500 text-xs">Nama</p>
                                        {{ $t->teacher_name }}
                                    </div>
                                </td>
                                <td class="px-3 py-3 border border-gray-400 text-sm">
                                    <div>
                                        <p class="font-light text-gray-500 text-xs">Kode</p>
                                        {{ $t->kode }}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="px-3 py-3 border border-gray-400 text-sm">
                                    <div>
                                        <p class="font-light text-gray-500 text-xs">Email</p>
                                        {{ $t->email }}
                                    </div>
                                </td>
                                <td class="px-3 py-3 border border-gray-400 text-sm">
                                    <div>
                                        <p class="font-light text-gray-500 text-xs">Password</p>
                                        <div class="group">
                                            <p id="star-password" class="group-hover:hidden">
                                                {{ str_repeat('•', strlen($t->password)) }}
                                            </p>
                                            <p id="show-password" class="hidden group-hover:block">
                                                {{ $t->password }}
                                            </p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="px-3 py-3 border border-gray-400 text-sm">
                                    <div>
                                        <p class="font-light text-gray-500 text-xs mb-1">Mata Pelajaran</p>
                                        @foreach (explode(',', $t->subject_names) as $subject)
                                            <p class="inline-flex items-center rounded-md bg-gray-100 px-2 py-1 my-0.5 text-xs font-medium uppercase text-gray-700 ring-1 ring-inset ring-gray-500/40">
                                                {{ $subject }}
                                            </p>
                                        @endforeach
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="px-3 py-3 border border-gray-400 text-sm">
                                    <div>
                                        <p class="font-light text-gray-500 text-xs mb-1">Kelas</p>
                                        @foreach (explode(',', $t->class_names) as $class)
                                            <p class="inline-flex items-center rounded-md bg-gray-100 px-2 py-1 my-0.5 text-xs font-medium uppercase text-gray-700 ring-1 ring-inset ring-gray-500/40">
                                                {{ $class }}
                                            </p>
                                        @endforeach
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    @endforeach
                </div>
            </div>
            <div class="flex-grow flex-col space-y-4">
                <div class="flex flex-col p-4 overflow-x-auto bg-white w-full sm:rounded-lg shadow ease-in-out transition-all">
                    <div class="border-b pb-3 mb-4 border-gray-400">
                        <h2 class="text-lg font-medium text-gray-900">
                            <div class="flex justify-between items-center gap-2">
                                <div>
                                    {{ __('Laporan agenda kelas ') }}<span class="inline font-bold">{{ $teacher->first()->teacher_name }} - {{ $teacher->first()->kode }}</span>{{ __(' (') }}<span class="inline font-bold">{{ $start_date }}</span>{{ __(' - ') }}<span class="inline font-bold">{{ $end_date }}{{ __(')') }}</span>
                                </div>
                                <div align="right" class="self-center">
                                    <a href="{{ route('dashboard.reportTeacherPDF', ['id' => $id]) }}">
                                        <button class="btn-primary py-1 px-2 fill-white">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="w-5" viewBox="0 0 512 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M0 64C0 28.7 28.7 0 64 0H224V128c0 17.7 14.3 32 32 32H384V304H176c-35.3 0-64 28.7-64 64V512H64c-35.3 0-64-28.7-64-64V64zm384 64H256V0L384 128zM176 352h32c30.9 0 56 25.1 56 56s-25.1 56-56 56H192v32c0 8.8-7.2 16-16 16s-16-7.2-16-16V448 368c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24H192v48h16zm96-80h32c26.5 0 48 21.5 48 48v64c0 26.5-21.5 48-48 48H304c-8.8 0-16-7.2-16-16V368c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H320v96h16zm80-112c0-8.8 7.2-16 16-16h48c8.8 0 16 7.2 16 16s-7.2 16-16 16H448v32h32c8.8 0 16 7.2 16 16s-7.2 16-16 16H448v48c0 8.8-7.2 16-16 16s-16-7.2-16-16V432 368z"/></svg>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </h2>
                    </div>
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="w-[650px] overflow-x-auto m-auto">
                                <table class="min-w-full divide-y divide-gray-400 text-left">
                                    <thead>
                                        <tr class="bg-gray-100">
                                            <th class="px-1 py-3 border border-gray-400 text-center text-xs font-medium uppercase whitespace-nowrap">No</th>
                                            <th class="px-3 py-3 border border-gray-400 text-xs font-medium uppercase whitespace-nowrap">Hari</th>
                                            <th class="px-3 py-3 border border-gray-400 text-xs font-medium uppercase whitespace-nowrap">Tanggal</th>
                                            <th class="px-3 py-3 border border-gray-400 text-xs font-medium uppercase whitespace-nowrap">Kelas</th>
                                            <th class="px-3 py-3 border border-gray-400 text-xs font-medium uppercase whitespace-nowrap">Mata Pelajaran</th>
                                            <th class="px-3 py-3 border border-gray-400 text-xs font-medium uppercase whitespace-nowrap">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $sortedReport = collect($report)->flatMap(function ($subjectReport) {
                                                return collect($subjectReport);
                                            })->sortBy('date')->all();

                                            $rowNumber = 1;
                                        @endphp
                                        @foreach($sortedReport as $subjectId => $item)
                                        <tr class="text-center">
                                            <td class="px-1 border text-center border-gray-400">{{ $rowNumber++ }}</td>
                                            <td class="px-3 border border-gray-400">{{ \Carbon\Carbon::parse($item['date'])->locale('id')->isoFormat('dddd') }}</td>
                                            <td class="px-3 border border-gray-400">{{ \Carbon\Carbon::parse($item['date'])->format('d-m-Y') }}</td>
                                            <td class="px-3 border border-gray-400">{{ $item['class']->class_name }}</td>
                                            <td class="px-3 border border-gray-400">{{ $item['subject']->subject_name }}</td>
                                            <td class="px-3 border border-gray-400">
                                                <div class="flex justify-center items-center ">
                                                    <a @if ($item['status'] === 'Mengisi') href="{{ route('daily-content.show', [$id, 'idClass' => $item['class']->id, 'idDay' => $item['dailyAgenda']->id]) }}" @endif class="flex items-center justify-center rounded-md">
                                                        <p class="inline-flex items-center rounded-md px-2 py-1 my-0.5 text-xs font-medium uppercase gap-1 {{ $item['status'] == 'Tidak mengisi' ? 'bg-red-100 text-red-700 ring-1 ring-inset ring-red-500/40' : 'bg-green-100 text-green-700 ring-1 ring-inset ring-green-500/40' }}">
                                                            {{ $item['status'] }}
                                                            @if ($item['status'] === 'Mengisi') <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512" fill="currentColor" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" ><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M320 0c-17.7 0-32 14.3-32 32s14.3 32 32 32h82.7L201.4 265.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L448 109.3V192c0 17.7 14.3 32 32 32s32-14.3 32-32V32c0-17.7-14.3-32-32-32H320zM80 32C35.8 32 0 67.8 0 112V432c0 44.2 35.8 80 80 80H400c44.2 0 80-35.8 80-80V320c0-17.7-14.3-32-32-32s-32 14.3-32 32V432c0 8.8-7.2 16-16 16H80c-8.8 0-16-7.2-16-16V112c0-8.8 7.2-16 16-16H192c17.7 0 32-14.3 32-32s-14.3-32-32-32H80z"/></svg> @endif
                                                        </p>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
