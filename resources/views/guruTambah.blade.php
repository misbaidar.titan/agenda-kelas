@extends('layouts.app')
@section('title', "Tambah | Agenda Kelas")
@section('guruTambah')
<div class="flex flex-col items-center">
    <div class="w-full sm:max-w-xl p-4 overflow-hidden">
        <nav class="flex items-center pb-4 sm:pl-8 text-gray-500 text-lg font-medium" aria-label="Breadcrumb">
            <ol class="flex items-center space-x-2">
            <li>
                <a href="{{ route('teachers.showGuru', $id) }}" class="hover:text-gray-900">Guru</a>
            </li>
            <li>
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                <path fill-rule="evenodd" d="M5.707 14.707a1 1 0 1 1-1.414-1.414L7.586 10 4.293 6.707a1 1 0 1 1 1.414-1.414l4 4a1 1 0 0 1 0 1.414l-4 4z" clip-rule="evenodd" />
                </svg>
            </li>
            <li>
                <a href="#" class="text-gray-900">Tambah data</a>
            </li>
            </ol>
        </nav>
        @if (session('success'))
            <div class="fixed left-1/2 -translate-x-1/2 z-50 message-component flex bg-green-100 border border-green-400 text-green-700 px-2 py-3 shadow-lg rounded" role="alert">
                <div>
                    <strong class="font-bold">Selamat!</strong>
                    <span class="block sm:inline mr-4">{{ session('success') }}</span>
                </div>
                <div class="flex items-center">
                    <button type="button" onclick="this.closest('.message-component').remove()">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-green-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                        </svg>
                    </button>
                </div>
            </div>
        @endif
        @if($errors->any())
            <div class="fixed left-1/2 -translate-x-1/2 z-50 message-component flex bg-red-100 border border-red-400 text-red-700 px-2 py-3 shadow-lg rounded" role="alert">
                <div>
                    <strong class="font-bold">Oops!</strong>
                    <span class="block sm:inline">{{ $errors->first() }}</span>
                </div>
                <div class="flex items-center">
                    <button type="button" onclick="this.closest('.message-component').remove()">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-700 cursor-pointer" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M5.293 5.293a1 1 0 0 1 1.414 0L10 8.586l3.293-3.293a1 1 0 1 1 1.414 1.414L11.414 10l3.293 3.293a1 1 0 1 1-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 1 1-1.414-1.414L8.586 10 5.293 6.707a1 1 0 0 1 0-1.414z" clip-rule="evenodd" />
                        </svg>
                    </button>
                </div>
            </div>
        @endif
        <div class="container mx-auto">
            <form id="teacherForm" action="{{ route('teachers.store', $id) }}" method="POST">
                @csrf
                <div class="bg-white shadow-md rounded px-8 py-8 mb-8">
                    <label for="teacherAmount" class="block mb-2">Jumlah guru yang akan ditambahkan:</label>
                    <input type="number" id="teacherAmount" name="teacherAmount" min="1" class="mb-5 input-text-primary pr-2" onchange="updateTeacherFields()">
                </div>
                <div id="teacherFields" class="block"></div>

                <div class="flex justify-end">
                <button type="submit" class="btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
 function updateTeacherFields() {
    const teacherAmount = document.getElementById('teacherAmount').value;
    let teacherFields = '';

    for (let i = 1; i <= teacherAmount; i++) {
        teacherFields += `
            <div class="teacher-field-group bg-white shadow-md rounded px-8 pt-6 pb-8 mb-8">
                <div class="border-b pb-3 mb-3 border-gray-400">
                    <h3 class="text-1xl text-center font-bold">Guru baru ${i}</h3>
                </div>
                <label for="teacher${i}_kode" class="block text-gray-700 font-bold">Kode</label>
                <p class="text-xs text-red-600">Gunakan nomor induk unik seperti NUPTK</p>
                <div class="relative">
                    <input type="number" id="teacher${i}_kode" name="teacher${i}_kode" class="w-full my-2 input-text-primary pr-2" required oninput="updateKodeIndicator(${i})">
                    <span id="kodeIndicator${i}" class="absolute inset-y-0 right-4 flex items-center pr-2 text-sm text-gray-400">0</span>
                </div>
                <label for="teacher${i}_email" class="block text-gray-700 font-bold">Email</label>
                <input type="email" id="teacher${i}_email" name="teacher${i}_email" class="w-full my-2 input-text-primary" required>
                <label for="teacher${i}_name" class="block text-gray-700 font-bold">Nama</label>
                <input type="text" id="teacher${i}_name" name="teacher${i}_name" class="w-full my-2 input-text-primary" required>
                <label class="block text-gray-700 font-bold" for="subject${i}_name">
                    Mata Pelajaran
                </label>
                <div id="subjectContainer${i}_name">
                    <div class="flex flex-col">
                        <div id="selectedSubjects${i}" class="flex flex-wrap mt-2"></div>
                        <input id="scrollSubjects${i}" type="text" class="input-text-norounded rounded-t-md" placeholder="Cari" oninput="filterSubjects(${i})">
                    </div>
                    <div>
                        <div id="subjectDropdown${i}" class="flex flex-col h-20 bg-white input-text-norounded rounded-b-md overflow-auto mb-2">
                            @foreach ($subjects as $s)
                            <div class="flex mr-4 mb-2 space-x-1 text-sm items-center subject-item">
                                <input type="checkbox" id="subject${i}{{ $s->id }}" value="{{ $s->id }}" name="subjects${i}[]" onchange="updateSelectedSubjects(${i}); resetInputSubject(${i})">
                                <label for="subject${i}{{ $s->id }}">{{ $s->subject_name }}</label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div id="subjectbaru${i}_container" class="hidden">
                    <input type="text" placeholder="ex: Matematika,Bahasa Indonesia,IPA" id="subjectbaru${i}_name" name="subjectbaru${i}_name" class="w-full my-2 input-text-primary">
                </div>
                <label for="useSubjectBaru${i}" class="inline-flex space-x-1 text-gray-700 text-sm font-bold">
                    <input type="checkbox" id="useSubjectBaru${i}" onclick="toggleSubjectBaruField(${i})">
                    <p>Buat Mata Pelajaran Baru <span class="text-xs font-medium">(jika tidak ada mata pelajaran)</span></p>
                </label>
                <label for="teacher${i}_password" class="block text-gray-700 font-bold">Password</label>
                <input type="password" id="teacher${i}_password" name="teacher${i}_password" class="w-full my-2 input-text-primary" required>
            </div>`;
    }

    document.getElementById('teacherFields').innerHTML = teacherFields;
}

    function updateKodeIndicator(index) {
        const input = document.getElementById(`teacher${index}_kode`);
        const kodeIndicator = document.getElementById(`kodeIndicator${index}`);
        kodeIndicator.textContent = `${input.value.length}`;
    }

    function toggleSubjectBaruField(i) {
        const checkbox = document.getElementById(`useSubjectBaru${i}`);
        const inputContainer = document.getElementById(`subjectbaru${i}_container`);
        const inputField = document.getElementById(`subjectbaru${i}_name`);
        const SubjectCheckboxContainer = document.getElementById(`subjectDropdown${i}`);
        const SubjectContainer = document.getElementById(`subjectContainer${i}_name`);
        const SubjectTags = document.getElementById(`selectedSubjects${i}`);

        if (checkbox.checked) {
            inputContainer.classList.remove("hidden");
            inputField.setAttribute("required", true);
            SubjectContainer.classList.add("hidden");

            const checkboxes = SubjectCheckboxContainer.querySelectorAll("input[type='checkbox']");
            checkboxes.forEach((checkbox) => {
                checkbox.checked = false;
            });
            while (SubjectTags.firstChild) {
                SubjectTags.removeChild(SubjectTags.firstChild);
            }
        } else {
            inputContainer.classList.add("hidden");
            inputField.removeAttribute("required");
            inputField.value = "";
            SubjectContainer.classList.remove("hidden");
        }
    }
    function filterSubjects(teacherId) {
        const input = document.getElementById('scrollSubjects' + teacherId);
        const subjectDropdown = document.getElementById('subjectDropdown' + teacherId);
        const subjectItems = subjectDropdown.querySelectorAll('.subject-item');

        const searchText = input.value.toLowerCase();

        subjectItems.forEach(item => {
            const label = item.querySelector('label');
            const text = label.textContent.toLowerCase();

            if (text.includes(searchText)) {
                item.style.display = 'flex';
            } else {
                item.style.display = 'none';
            }
        });
    }
        function resetInputSubject(id) {
            const input = document.getElementById('scrollSubjects' + id);
            input.value = '';

            const subjectDropdown = document.getElementById('subjectDropdown' + id);
            const subjectItems = subjectDropdown.querySelectorAll('.subject-item');

            subjectItems.forEach(item => {
            item.style.display = 'flex';
        });
    }

    function updateSelectedSubjects(teacherId) {
        const subjectDropdown = document.getElementById('subjectDropdown' + teacherId);
        const selectedSubjectsContainer = document.getElementById('selectedSubjects' + teacherId);

        selectedSubjectsContainer.innerHTML = '';

        const selectedCheckboxes = Array.from(subjectDropdown.querySelectorAll('input[type="checkbox"]:checked'));

        selectedCheckboxes.forEach(checkbox => {
            const tag = document.createElement('div');
            tag.classList.add('bg-gray-100', 'text-xs', 'text-gray-700', 'rounded-md', 'uppercase', 'ring-1', 'ring-inset', 'ring-gray-500/40', 'py-1', 'px-2', 'mr-2', 'mb-1');
            tag.textContent = checkbox.nextElementSibling.textContent;

            const removeButton = document.createElement('button');
            removeButton.classList.add('ml-1', 'text-gray-700', 'cursor-pointer');
            removeButton.innerHTML = '×';

            removeButton.addEventListener('click', () => {
                checkbox.checked = false;
                tag.remove();
            });

            tag.appendChild(removeButton);
            selectedSubjectsContainer.appendChild(tag);
        });
    }
</script>




@endsection
