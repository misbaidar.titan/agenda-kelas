@extends('layouts.app')
@section('title', "Tambah | Agenda Kelas")
@section('absentTambah')
<div class="flex flex-col items-center">
    <div class="w-full sm:max-w-xl p-4 overflow-hidden">
        <nav class="flex items-center pb-4 sm:pl-8 text-gray-500 text-sm font-medium" aria-label="Breadcrumb">
            <ol class="flex items-center space-x-2">
            <li>
                <a href="{{ route('class.show', $id) }}" class="hover:text-gray-900">Kelas</a>
            </li>
            <li>
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                <path fill-rule="evenodd" d="M5.707 14.707a1 1 0 1 1-1.414-1.414L7.586 10 4.293 6.707a1 1 0 1 1 1.414-1.414l4 4a1 1 0 0 1 0 1.414l-4 4z" clip-rule="evenodd" />
                </svg>
            </li>
            <li>
                <a href="{{ route('daily-agenda.show', [$id, $idClass]) }}" class="hover:text-gray-900">Agenda harian</a>
            </li>
            <li>
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                <path fill-rule="evenodd" d="M5.707 14.707a1 1 0 1 1-1.414-1.414L7.586 10 4.293 6.707a1 1 0 1 1 1.414-1.414l4 4a1 1 0 0 1 0 1.414l-4 4z" clip-rule="evenodd" />
                </svg>
            </li>
            <li>
                <a href="{{ route('daily-content.show', [$id, $idClass, $idDay]) }}" class="hover:text-gray-900">Agenda isi</a>
            </li>
            <li>
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                <path fill-rule="evenodd" d="M5.707 14.707a1 1 0 1 1-1.414-1.414L7.586 10 4.293 6.707a1 1 0 1 1 1.414-1.414l4 4a1 1 0 0 1 0 1.414l-4 4z" clip-rule="evenodd" />
                </svg>
            </li>
            <li>
                <a href="#" class="text-gray-900">Tambah data</a>
            </li>
            </ol>
        </nav>
        <div class="container mx-auto">
            <form id="absentForm" action="{{ route('daily-content.storeAbsent', [$id, $idClass, $idDay]) }}" method="POST">
                @csrf
                <div class="bg-white shadow-md rounded px-8 py-8 mb-8">
                    <label for="absentAmount" class="block mb-2">Jumlah siswa absen yang akan ditambahkan:</label>
                    <input type="number" id="absentAmount" name="absentAmount" min="1" class="mb-5 input-text-primary pr-2" onchange="updateAbsentFields()">
                </div>
                <div id="absentFields" class="block"></div>

                <div class="flex justify-end">
                <button type="submit" class="btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function updateAbsentFields() {
        const absentAmount = document.getElementById('absentAmount').value;
        let absentFields = '';

        for (let i = 1; i <= absentAmount; i++) {
            absentFields += `
                <div class="absent-field-group bg-white shadow-md rounded px-8 pt-6 pb-8 mb-8">
                    <div class="border-b pb-3 mb-3 border-gray-400">
                        <h3 class="text-1xl text-center font-bold">Siswa Absen ${i}</h3>
                    </div>
                    <label class="block text-gray-700 font-bold" for="student${i}_name">
                        Nama Siswa
                    </label>
                    <div class="relative">
                        <select class="w-full py-2 my-2 input-text-primary" style="-webkit-appearance: none;" id="student${i}_name" name="student${i}_name">
                            <option value="" selected>Pilih siswa</option>
                            @foreach ($students as $student)
                                <option value="{{ $student->student_name }}">
                                    {{ $student->student_name }}
                                </option>
                            @endforeach
                        </select>
                        <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                            <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                            </svg>
                        </div>
                    </div>
                    <label class="block text-gray-700 font-bold" for="absent${i}_reason">
                        Alasan
                    </label>
                    <div class="relative">
                        <select class="w-full py-2 my-2 input-text-primary" style="-webkit-appearance: none;" id="absent${i}_reason" name="absent${i}_reason">
                            <option value="" selected>Pilih alasan</option>
                            <option value="sakit">Sakit</option>
                            <option value="izin">Izin</option>
                            <option value="alpa">Alpa</option>
                        </select>
                        <div class="absolute inset-y-0 right-2 flex items-center pointer-events-none">
                            <svg class="w-4 h-4 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                            </svg>
                        </div>
                    </div>
                </div>`;
        }

        document.getElementById('absentFields').innerHTML = absentFields;
    }
</script>
<script>
    const form = document.getElementById('absentForm');

    form.addEventListener('submit', (event) => {
    const reasonSelect = document.getElementById('reason');
    const selectedValue = reasonSelect.value;

    if (selectedValue === '') {
        event.preventDefault();
        alert('Tolong untuk memilih alasan.');
    }
    });
</script>
@endsection
