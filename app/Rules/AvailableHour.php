<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Schedule;

class AvailableHour implements Rule
{
    protected $day;
    protected $scheduleId;
    protected $teacherId;

    public function __construct($day, $scheduleId, $teacherId, $idClass)
    {
        $this->day = $day;
        $this->scheduleId = $scheduleId;
        $this->teacherId = $teacherId;
        $this->idClass = $idClass;
    }

    public function passes($attribute, $value)
    {
        $startHour = request()->input('start_hour');
        $endHour = request()->input('end_hour');
        if ($startHour > $endHour) {
            return false;
        }
        $isOccupied = Schedule::where('day', $this->day)
        ->where('class_id', $this->idClass)
        ->where('id', '<>', $this->scheduleId)
        ->where(function ($query) use ($startHour, $endHour) {
            $query->where(function ($q) use ($startHour, $endHour) {
                $q->whereBetween('start_hour', [$startHour, $endHour])
                    ->orWhereBetween('end_hour', [$startHour, $endHour]);
            })->orWhere(function ($q) use ($startHour, $endHour) {
                $q->where('start_hour', '<=', $startHour)
                    ->where('end_hour', '>=', $endHour);
            });
        })
        ->orWhereHas('class', function ($query) use ($startHour, $endHour) {
            $query->where('teacher_id', $this->teacherId)
                ->where('day', $this->day)
                ->where('class_id', '<>', $this->idClass)
                ->where(function ($query) use ($startHour, $endHour) {
                    $query->where(function ($q) use ($startHour, $endHour) {
                        $q->whereBetween('start_hour', [$startHour, $endHour])
                            ->orWhereBetween('end_hour', [$startHour, $endHour]);
                    })->orWhere(function ($q) use ($startHour, $endHour) {
                        $q->where('start_hour', '<=', $startHour)
                            ->where('end_hour', '>=', $endHour);
                    });
                });
        })
        ->exists();

        return !$isOccupied;
    }

    public function message()
    {
        return 'The selected hours are not available.';
    }
}
