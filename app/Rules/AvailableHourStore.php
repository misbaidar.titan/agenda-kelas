<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Schedule;

class AvailableHourStore implements Rule
{
    protected $day;
    protected $startHour;
    protected $endHour;
    protected $teacherId;
    protected $idClass;

    public function __construct($day, $startHour, $endHour, $teacherId, $idClass)
    {
        $this->day = $day;
        $this->startHour = $startHour;
        $this->endHour = $endHour;
        $this->teacherId = $teacherId;
        $this->idClass = $idClass;
    }

    public function passes($attribute, $value)
    {
        if ($this->startHour > $this->endHour) {
            return false;
        }
        $teacherId = $this->teacherId;
        $isOccupied = Schedule::where('day', $this->day)
        ->where('class_id', $this->idClass)
        ->where(function ($query) {
            $query->where(function ($q) {
                $q->whereBetween('start_hour', [$this->startHour, $this->endHour])
                    ->orWhereBetween('end_hour', [$this->startHour, $this->endHour]);
            })->orWhere(function ($q) {
                $q->where('start_hour', '<=', $this->startHour)
                    ->where('end_hour', '>=', $this->endHour);
            });
        })
        ->orWhereHas('class', function ($query) {
            $query->where('teacher_id', $this->teacherId)
                ->where('day', $this->day)
                ->where('class_id', '<>', $this->idClass)
                ->where(function ($query) {
                    $query->where(function ($q) {
                        $q->whereBetween('start_hour', [$this->startHour, $this->endHour])
                            ->orWhereBetween('end_hour', [$this->startHour, $this->endHour]);
                    })->orWhere(function ($q) {
                        $q->where('start_hour', '<=', $this->startHour)
                            ->where('end_hour', '>=', $this->endHour);
                    });
                });
        })
        ->exists();

        return !$isOccupied;
    }

    public function message()
    {
        return 'The selected hours are not available.';
    }
}
