<?php

namespace app\Interfaces;

use Illuminate\Http\Request;

interface ReportGenerator
{
    public function report(Request $request, $id);
    public function reportPDF();
}
