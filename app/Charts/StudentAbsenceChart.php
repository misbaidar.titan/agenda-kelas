<?php

namespace App\Charts;

use ArielMejiaDev\LarapexCharts\LarapexChart;

class StudentAbsenceChart
{
    protected $chart;

    public function __construct()
    {
        $this->chart = new LarapexChart;
    }

    public function build($student): \ArielMejiaDev\LarapexCharts\BarChart
    {
        return $this->chart->barChart()
            ->setTitle('Rekap Absensi ' . $student->student_name)
            ->addData('Sakit', [$student->sakit_total])
            ->addData('Izin', [$student->izin_total])
            ->addData('Alpa', [$student->alpa_total])
            ->setColors(['#4ade80', '#facc15', '#ef4444'])
            ->setXAxis(['Alasan']);
    }
}
