<?php

namespace App\Imports;

use App\Models\Student;
use App\Models\Classes;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Auth;

class StudentsImport implements ToModel
{
    public function model(array $row)
    {
        $existingStudent = Student::where('student_kode', $row[0])->first();
        if ($existingStudent) {
            throw new \Exception("Terdapat kode siswa yang sama, proses import dibatalkan");
        }
        $className = $row[2];
        $class = Classes::where('class_name', $className)
            ->where('user_id', Auth::user()->id)
            ->first();

        if (!$class) {
            $class = new Classes();
            $class->class_name = $row[2];
            $class->user_id = Auth::user()->id;
            $class->save();
        }

        $student = new Student();
        $student->student_kode = $row[0];
        $student->student_name = $row[1];
        $className = $row[2];
        $student->class_id = $class->id;
        $student->user_id = Auth::user()->id;

        return $student;
    }
}
