<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\Teacher;
use App\Models\Subject;
use App\Models\User;
use Spatie\Permission\Models\Role;

class TeachersImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $schoolName = Auth::user()->school_name;

        // Check if the teacher with the same kode or email already exists
        $existingTeacher = Teacher::where('kode', intval($row[0]))
            ->whereHas('user', function ($query) use ($row) {
                $query->where('email', $row[3]);
            })
            ->first();

        if ($existingTeacher) {
            throw new \Exception("Ada guru dengan kode atau email yang sama sudah ada. Proses import dibatalkan.");
        }

        // Process subjects
        $subjects = explode(',', $row[2]);
        $subjects = array_map(function ($str) {
            return preg_replace('/^\s+|\s+$/', '', $str);
        }, $subjects);
        $subjectIds = [];

        foreach ($subjects as $subjectName) {
            $subject = Subject::where('subject_name', $subjectName)
                ->where('user_id', Auth::user()->id)
                ->first();

            if (!$subject) {
                $subject = new Subject();
                $subject->subject_name = $subjectName;
                $subject->user_id = Auth::user()->id;
                $subject->save();
            }

            $subjectIds[] = $subject->id;
        }

        // Create a new user
        $teacherUser = new User();
        $teacherUser->name = $row[1];
        $teacherUser->school_name = $schoolName;
        $teacherUser->password = Hash::make($row[4]);
        $teacherUser->email = $row[3];
        $teacherUser->save();

        // Create a new teacher
        $teacher = new Teacher();
        $teacher->kode = intval($row[0]);
        $teacher->user_id = $teacherUser->id;
        $teacher->teacher_name = $row[1];
        $teacher->password = $row[4];
        $teacher->save();
        $teacher->subject()->sync($subjectIds);

        // Assign role to the user
        $teacherUserRole = Role::where('name', 'teacher')->where('guard_name', Auth::getDefaultDriver())->first();
        $teacherUser->assignRole($teacherUserRole);

        return $teacher;
    }
}
