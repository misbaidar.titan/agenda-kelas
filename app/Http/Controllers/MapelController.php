<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subject;

class MapelController extends Controller
{
    public function show(Request $request, $id)
    {
        $perPage = $request->input('perPage', 10);
        if ($perPage === 'semua') {
            $perPage = 9999;
        }
        $mapel = Subject::where('user_id', $id)
            ->withCount('teachers')
            ->paginate($perPage);

        if ($mapel->isEmpty()) {
            return redirect()->route('subject.create', ['id' => $id]);
        }

        return view('mapel', compact('mapel', 'id', 'perPage'));
    }

    public function create($id)
    {
        $mapel = Subject::where('user_id', $id)->get();
        return view('mapelTambah', compact('mapel', 'id'));
    }

    public function store(Request $request, $id)
    {
        $validatedData = $request->validate([
            'mapelAmount' => 'required|integer|min:1',
        ]);

        $mapelAmount = $validatedData['mapelAmount'];


        for ($i = 1; $i <= $mapelAmount; $i++) {
            $validatedMapelData = $request->validate([
                "mapel{$i}_name" => 'required|string|max:255',
            ]);

            $mapelName = $validatedMapelData["mapel{$i}_name"];

            $existingMapel = Subject::where('user_id', auth()->user()->id)
                ->where('subject_name', $mapelName)
                ->first();

            if ($existingMapel) {
                return redirect()->back();
            }

            $mapel = new Subject();
            $mapel->subject_name = $mapelName;
            $mapel->user_id = auth()->user()->id;
            $mapel->save();
        }

        return redirect()->route('subject.show', $id);
    }

    public function deleteSelected(Request $request)
    {
        $ids = $request->input('selected');

         Subject::whereIn('id', $ids)->delete();

         return back()->with('success', 'Selected rows have been deleted.');
    }

    public function update(Request $request, $idSubject)
    {
        $mapel = Subject::where('id', $idSubject)->firstOrFail();
        $mapel->subject_name = $request->input('mapel_name');
        $mapel->save();

        return redirect()->back()->with('success', 'Class information updated successfully.');
    }

    public function search(Request $request, $id)
    {
        $perPage = $request->input('perPage', 9999);
        if ($perPage === 'semua') {
            $perPage = 9999;
        }
        $search = $request->input('search');

        $mapel = Subject::where('user_id', $id)
            ->withCount('teachers')
            ->where(function ($query) use ($search) {
                $query->where('subject_name', 'LIKE', '%' . $search . '%');
            })
            ->paginate($perPage);

        return view('mapel', compact('mapel', 'id', 'search', 'perPage'));
    }
}
