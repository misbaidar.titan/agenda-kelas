<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\DailyAgendaContent;
use App\Models\AbsentStudent;
use App\Models\DailyAgenda;
use App\Models\Teacher;
use App\Models\Subject;
use App\Models\Student;

class DailyContentController extends Controller
{
    public function show($id, $idClass, $idDay)
    {
        $dailycontents = DailyAgendaContent::where('daily_agenda_id', $idDay)->get()->sortBy('lesson_hours');
        $absentStudent = AbsentStudent::where('daily_agenda_id', $idDay)->get();
        $dailyagenda = DailyAgenda::findOrFail($idDay);
        $students = Student::where('class_id', $idClass)->get();

        if ($dailycontents->isEmpty() && $absentStudent->isEmpty() && auth()->user()->hasRole('teacher')) {
            return redirect()->route('daily-content.create', [$id, $idClass, $idDay]);
        }

        return view('dailycontent', compact('dailycontents', 'students', 'absentStudent', 'id', 'idClass', 'idDay'));
    }


    public function create($id, $idClass, $idDay)
    {
        $count = DailyAgendaContent::where('daily_agenda_id', $idDay)
                                   ->where('lesson_hours', '<=', 10)
                                   ->count();
        if ($count >= 10) {
            return redirect()->back()->with('error', 'Jam pelajaran sudah lebih dari 10');
        }

        $dailycontents = DailyAgendaContent::where('daily_agenda_id', $idDay)->get();
        $dailyagenda = DailyAgenda::findOrFail($idDay);

        $teacher = Auth::user()->teacher;

        $subjects = $teacher->subject;

        return view('dailycontentTambah', compact('dailycontents', 'subjects', 'id', 'idClass', 'idDay'));
    }

    public function createAbsent($id, $idClass, $idDay)
    {
        $absentStudent = AbsentStudent::where('daily_agenda_id', $idDay)->get();
        $students = Student::where('class_id', $idClass)->get();
        return view('absentTambah', compact('absentStudent', 'students', 'id', 'idClass', 'idDay'));
    }

    public function store(Request $request, $id, $idClass, $idDay)
    {
        $validatedData = $request->validate([
            'dailyContentAmount' => 'required|integer|min:1',
        ]);

        $dailyContentAmount = $validatedData['dailyContentAmount'];

        for ($i = 1; $i <= $dailyContentAmount; $i++) {
            $validatedContentData = $request->validate([
                "content{$i}_lesson_hours" => [
                    'required',
                    'integer',
                    function ($attribute, $value, $fail) use ($idDay) {
                        $isHourOccupied = DailyAgendaContent::where('daily_agenda_id', $idDay)
                            ->where('lesson_hours', $value)
                            ->exists();

                        if ($isHourOccupied) {
                            $fail("The lesson hour $value is already occupied.");
                        }
                    }
                ],
                "subject{$i}_name" => 'required|exists:subjects,id',
                "content{$i}_indicator" => 'nullable|string|max:255',
                "content{$i}_learning_activities" => 'nullable|string',
                "content{$i}_description" => 'nullable|string',
                "content{$i}_confirmation_image" => 'nullable|image'
            ]);

            $dailyAgendaContent = new DailyAgendaContent([
                'lesson_hours' => $request->input("content{$i}_lesson_hours"),
                'subject_id' => $request->input("subject{$i}_name"),
                'indicator' => $request->input("content{$i}_indicator"),
                'learning_activities' => $request->input("content{$i}_learning_activities"),
                'description' => $request->input("content{$i}_description"),
                'daily_agenda_id' => $idDay,
                'teacher_kode' => Auth::user()->teacher->id
            ]);

            if ($request->hasFile("content{$i}_confirmation_image")) {
                $image = $request->file("content{$i}_confirmation_image");
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $image->storeAs('public/confirmation_images', $filename);
                $dailyAgendaContent->confirmation_image = $filename;
            }
            $dailyAgendaContent->save();
        }

        return redirect()
            ->route('daily-content.show', [$id, $idClass, $idDay])
            ->with('success', 'Data berhasil disimpan');
    }

    public function storeAbsent(Request $request, $id, $idClass, $idDay)
    {
        $validatedData = $request->validate([
            'absentAmount' => 'required|integer|min:1',
        ]);

        $absentAmount = $validatedData['absentAmount'];


        for ($i = 1; $i <= $absentAmount; $i++) {
            $validatedAbsentData = $request->validate([
                "student{$i}_name" => 'required|string|max:255',
                "absent{$i}_reason" => 'required|string|max:255',
            ]);

            $absent = new AbsentStudent();
            $studentName = $validatedAbsentData["student{$i}_name"];
            $student = Student::where('student_name', $studentName)
            ->where('class_id', $idClass)
            ->firstOrFail();
            $absent->student_kode = $student->student_kode;
            $absent->student_name = $student->student_name;
            $absent->reason = $validatedAbsentData["absent{$i}_reason"];
            $absent->daily_agenda_id = $idDay;
            $absent->save();
        }
        return redirect()->route('daily-content.show', [$id, $idClass, $idDay]);
    }

    public function deleteSelected(Request $request)
    {
        $ids = $request->input('selected');

         DailyAgendaContent::whereIn('id', $ids)->delete();

         return back()->with('success', 'Selected rows have been deleted.');
    }

    public function deleteSelectedAbsent(Request $request)
    {
        $ids = $request->input('selectedAbsent');

         AbsentStudent::whereIn('id', $ids)->delete();

         return back()->with('success', 'Selected rows have been deleted.');
    }
    public function update(Request $request, $id, $idClass, $idContent)
    {
        $dailycontent = DailyAgendaContent::findOrFail($idContent);
        $dailycontent->lesson_hours = $request->input('lesson_hours');
        $dailycontent->subject_id = $request->input('subject_study');
        $dailycontent->indicator = $request->input('indicator');
        $dailycontent->learning_activities = $request->input('learning_activities');
        $dailycontent->description = $request->input('description');
        $removeConfirmationImage = $request->input('remove_confirmation_image');

        if ($removeConfirmationImage) {
            Storage::delete('public/confirmation_images/'.$dailycontent->confirmation_image);
            $dailycontent->confirmation_image = null;
        }

        if ($request->hasFile("confirmation_image")) {
            if (!is_null($dailycontent->confirmation_image)) {
                Storage::delete('public/confirmation_images/'.$dailycontent->confirmation_image);
            }

            $image = $request->file("confirmation_image");
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('public/confirmation_images', $filename);
            $dailycontent->confirmation_image = $filename;
        }

        $dailycontent->save();

        return redirect()->route('daily-content.show', [$id, $idClass, $dailycontent->daily_agenda_id])
                         ->with('success','Konten Harian berhasil diperbarui.');
    }

    public function updateAbsent(Request $request, $id, $idClass, $idStudent)
    {
        $absent = AbsentStudent::findOrFail($idStudent);
        $studentName = $request->input('student_name');
        $student = Student::where('student_name', $studentName)
        ->where('class_id', $idClass)
        ->firstOrFail();
        $absent->student_kode = $student->student_kode;
        $absent->student_name = $student->student_name;
        $absent->reason = $request->input('reason');
        $absent->save();

        return redirect()->route('daily-content.show', [$id, $idClass, $absent->daily_agenda_id]);
    }
}
