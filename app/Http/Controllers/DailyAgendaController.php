<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Models\DailyAgenda;
use Carbon\Carbon;

class DailyAgendaController extends Controller
{
    public function show(Request $request, $id, $idClass)
    {
        $perPage = $request->input('perPage', 10);
        if ($perPage === 'semua') {
            $perPage = 9999;
        }

        $today = Carbon::now()->toDateString();
        $dailyagendas = DailyAgenda::where('class_id', $idClass)->orderByDesc('date')->paginate($perPage);

        $isTodayExist = $dailyagendas->contains('date', $today);

        if (Auth::user()->hasRole('teacher') && !$isTodayExist) {
            $dailyClass = new DailyAgenda;
            $dailyClass->day = Carbon::now()->locale('id')->dayName;
            $dailyClass->date = $today;
            $dailyClass->class_id = $idClass;
            $dailyClass->save();

            return redirect()->route('daily-content.show', ['id' => $id, 'idClass' => $idClass, 'idDay' => $dailyClass->id]);
        } else if (Auth::user()->hasRole('teacher')) {
            $today = Carbon::now()->toDateString();
            $existingDailyAgenda = DailyAgenda::where('class_id', $idClass)
                ->where('date', $today)
                ->first();

            if ($existingDailyAgenda) {
                return redirect()->route('daily-content.show', ['id' => $id, 'idClass' => $idClass, 'idDay' => $existingDailyAgenda->id]);
            }
        }

        return view('dailyagenda', compact('dailyagendas', 'id', 'idClass', 'perPage'));
    }


    public function search(Request $request, $id, $idClass)
    {
        $perPage = $request->input('perPage', 9999);
        if ($perPage === 'semua') {
            $perPage = 9999;
        }

        $search = $request->input('search');
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        $dailyagendas = DailyAgenda::where('class_id', $idClass)
            ->where(function ($query) use ($search) {
                $query->where('day', 'LIKE', '%'.$search.'%')
                    ->orWhere('date', 'LIKE', '%'.$search.'%')
                    ->orWhere('description', 'LIKE', '%'.$search.'%');
            });

        if ($startDate && $endDate) {
            $dailyagendas->whereBetween('date', [$startDate, $endDate]);
        }

        $dailyagendas = $dailyagendas->paginate($perPage);

        return view('dailyagenda', compact('dailyagendas', 'id', 'idClass', 'search', 'perPage'));
    }



    public function create($id, $idClass)
    {
        $dailyagenda = DailyAgenda::where('class_id', $idClass);
        return view('dailyagendaTambah', compact('dailyagenda', 'id', 'idClass'));
    }

    public function store(Request $request, $id, $idClass)
    {
        $validatedData = $request->validate([
            'date' => [
                'required',
                Rule::unique('daily_agendas')->where(function ($query) use ($idClass) {
                    return $query->where('class_id', $idClass);
                })
            ],
            'description' => 'nullable',
        ]);

        $date = Carbon::parse($validatedData['date'])->locale('id');
        $dayOfWeek = $date->dayName;

        $dailyClass = new DailyAgenda;
        $dailyClass->day = $dayOfWeek;
        $dailyClass->date = $validatedData['date'];
        $dailyClass->description = $validatedData['description'];
        $dailyClass->class_id = $idClass;
        $dailyClass->save();

        return redirect()->route('daily-agenda.show', [$id, $idClass]);
    }

    public function deleteSelected(Request $request)
    {
        $ids = $request->input('selected');

        DailyAgenda::whereIn('id', $ids)->delete();

         return back()->with('success', 'Selected rows have been deleted.');
    }

    public function update(Request $request, $id, $idDay)
    {
        $dailyClass = DailyAgenda::findOrFail($idDay);

        $validatedData = $request->validate([
            'description' => 'nullable',
        ]);

        $dailyClass->description = $validatedData['description'];
        $dailyClass->save();

        return redirect()->route('daily-agenda.show', [$id, $dailyClass->class_id]);
    }
}
