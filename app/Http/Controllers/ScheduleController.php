<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Schedule;
use App\Models\Teacher;
use App\Models\Classes;
use App\Rules\AvailableHour;
use App\Rules\AvailableHourStore;
use PDF;

class ScheduleController extends Controller
{
    public function show(Request $request, $id, $idClass)
    {
        $perPage = $request->input('perPage', 10);
        if ($perPage === 'semua') {
            $perPage = 9999;
        }

        $schedulescheck = Schedule::where('class_id', $idClass)->get();

        $teacherIdscheck = Teacher::whereHas('classes', function ($query) use ($idClass) {
            $query->where('class_id', $idClass);
        })->pluck('id');

        $schedulesToDelete = $schedulescheck->whereNotIn('teacher_id', $teacherIdscheck);

        foreach ($schedulesToDelete as $schedule) {
            $schedule->delete();
        }

        $schedules = Schedule::where('class_id', $idClass)->paginate($perPage);

        $teachers = Teacher::whereHas('classes', function ($query) use ($idClass) {
            $query->where('class_id', $idClass);
        })->get();
        $availableSubjects = [];

        foreach ($teachers as $teacher) {
            $availableSubjects[$teacher->id] = $teacher->subject;
        }
        $className = Classes::where('id', $idClass)->value('class_name');

        return view('schedule', compact('schedules', 'id', 'idClass', 'perPage', 'teachers', 'availableSubjects', 'className'));
    }

    public function search(Request $request, $id, $idClass)
    {
        $perPage = $request->input('perPage', 9999);
        if ($perPage === 'semua') {
            $perPage = 9999;
        }

        $search = $request->input('search');
        $selectedDay = $request->input('selected_day');

        $schedules = Schedule::where('class_id', $idClass)
        ->leftJoin('teachers', 'schedules.teacher_id', '=', 'teachers.id')
        ->leftJoin('subjects', 'schedules.subject_id', '=', 'subjects.id')
        ->where(function ($query) use ($search) {
            $query->where('teachers.teacher_name', 'LIKE', '%' . $search . '%')
                ->orWhere('subjects.subject_name', 'LIKE', '%' . $search . '%');
        })
        ->when($selectedDay, function ($query) use ($selectedDay) {
            return $query->where('schedules.day', $selectedDay);
        })
        ->paginate($perPage);

        $teachers = Teacher::whereHas('classes', function ($query) use ($idClass) {
            $query->where('class_id', $idClass);
        })->get();
        $availableSubjects = [];

        foreach ($teachers as $teacher) {
            $availableSubjects[$teacher->id] = $teacher->subject;
        }
        $className = Classes::where('id', $idClass)->value('class_name');

        return view('schedule', compact('schedules', 'id', 'idClass', 'perPage', 'teachers', 'availableSubjects', 'className', 'search'));
    }

    public function create($id, $idClass)
    {
        $schedules = Schedule::where('class_id', $idClass)->get();

        $teachers = Teacher::whereHas('classes', function ($query) use ($idClass) {
            $query->where('class_id', $idClass);
        })->get();
        $availableSubjects = [];

        foreach ($teachers as $teacher) {
            $availableSubjects[$teacher->id] = $teacher->subject;
        }

        return view('scheduleTambah', compact('schedules', 'id', 'idClass', 'teachers', 'availableSubjects'));
    }

    public function store(Request $request, $id, $idClass)
    {
        $this->validate($request, [
            'scheduleAmount' => 'required|integer|min:1',
        ]);

        $scheduleAmount = (int) $request->input('scheduleAmount');

        for ($i = 1; $i <= $scheduleAmount; $i++) {
            $this->validate($request, [
                "day{$i}" => 'required|in:senin,selasa,rabu,kamis,jumat',
                "teacher{$i}" => 'required|exists:teachers,id',
                "subject{$i}" => 'required|exists:subjects,id',
                "start_hour{$i}" => [
                    "required",
                    "integer",
                    "min:1",
                    new AvailableHourStore($request->input("day{$i}"), $request->input("start_hour{$i}"), $request->input("end_hour{$i}"), $request->input("teacher{$i}"), $idClass),
                ],
                "end_hour{$i}" => [
                    "required",
                    "integer",
                    "min:1",
                    new AvailableHourStore($request->input("day{$i}"), $request->input("start_hour{$i}"), $request->input("end_hour{$i}"), $request->input("teacher{$i}"), $idClass),
                ],
            ]);

            $day = $request->input("day{$i}");

            $existingSchedule = Schedule::where('class_id', $idClass)
                ->where("day", $day)
                ->where("teacher_id", $request->input("teacher{$i}"))
                ->where("subject_id", $request->input("subject{$i}"))
                ->first();

                if ($existingSchedule) {
                    return back()->withErrors(['error' => 'Tidak dapat mengisi guru dengan mata pelajaran yang sama di hari ' . $day]);
                }

            $schedule = new Schedule([
                'class_id' => $idClass,
                'day' => $request->input("day{$i}"),
                'teacher_id' => $request->input("teacher{$i}"),
                'subject_id' => $request->input("subject{$i}"),
                'start_hour' => $request->input("start_hour{$i}"),
                'end_hour' => $request->input("end_hour{$i}"),
            ]);
            $schedule->save();
        }

        return redirect()->route('schedule.show', [$id, $idClass])->with('success', 'Schedules added successfully.');
    }

    public function update(Request $request, $id, $idClass, $idSchedule)
    {
        $request->validate([
            "day" => "required|in:senin,selasa,rabu,kamis,jumat",
            "teacher" => "required|exists:teachers,id",
            "subject" => "required|exists:subjects,id",
            "start_hour" => [
                "required",
                "integer",
                "min:1",
                new AvailableHour($request->input('day'), $idSchedule, $request->input("teacher"), $idClass),
            ],
            "end_hour" => [
                "required",
                "integer",
                "min:1",
                new AvailableHour($request->input('day'), $idSchedule, $request->input("teacher"), $idClass),
            ],
        ]);

        $existingSchedule = Schedule::where('class_id', $idClass)
            ->where("day", $request->input("day"))
            ->where("subject_id", $request->input("subject"))
            ->where("teacher_id", $request->input("teacher"))
            ->where("id", "<>", $idSchedule)
            ->first();

        if ($existingSchedule) {
            return back()->withErrors(['error' => 'The selected combination of subject and teacher already exists for this day.']);
        }

        $schedule = Schedule::findOrFail($idSchedule);
        $schedule->day = $request->input("day");
        $schedule->teacher_id = $request->input("teacher");
        $schedule->subject_id = $request->input("subject");
        $schedule->start_hour = $request->input("start_hour");
        $schedule->end_hour = $request->input("end_hour");
        $schedule->save();

        return redirect()->back()->with('success', 'Schedule updated successfully.');
    }

    public function deleteSelected(Request $request)
    {
        $ids = $request->input('selected');

         Schedule::whereIn('id', $ids)->delete();

         return back()->with('success', 'Selected rows have been deleted.');
    }

    public function timetable(Request $request, $id)
    {
        $idClass = $request->input('selected_class');
        $className = Classes::where('id', $idClass)->value('class_name');
        $weekDays = ['senin', 'selasa', 'rabu', 'kamis', 'jumat'];
        $hours = range(1, 10);

        $schedules = Schedule::where('class_id', $idClass)
            ->get()
            ->map(function ($schedule) {
                $schedule->duration = $schedule->end_hour - $schedule->start_hour + 1;
                return $schedule;
            })
            ->groupBy('day')
            ->map(function ($schedulesPerDay) {
                return $schedulesPerDay->groupBy('start_hour');
            });
            $remainingDuration = array_fill_keys($weekDays, 0);

        $classes = Classes::where('user_id', $id)->get();

        session([
            'weekDays' => $weekDays,
            'hours' => $hours,
            'schedules' => $schedules,
            'remainingDuration' => $remainingDuration,
            'className' => $className,
            'classes' => $classes,
        ]);

        return view('timeTable', compact('id', 'idClass', 'weekDays', 'hours', 'schedules', 'remainingDuration', 'className', 'classes'));
    }

    public function timetablePDF()
    {
        $data = [
            'weekDays' => session('weekDays'),
            'hours' => session('hours'),
            'schedules' => session('schedules'),
            'remainingDuration' => session('remainingDuration'),
            'className' => session('className'),
            'classes' => session('classes'),
        ];

        $pdf = PDF::loadView('timeTablePDF', $data)
            ->setPaper('a4', 'portrait');

        $filename = 'jadwal_' . $data['className'] . '.pdf';

        return $pdf->download($filename);
    }
}
