<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Classes;
use App\Models\Student;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ClassesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, $id)
    {
        $perPage = $request->input('perPage', 10);
        if ($perPage === 'semua') {
            $perPage = 9999;
        }
        if (Auth::user()->hasRole('admin')) {
            $classes = Classes::withCount('students', 'teachers')
            ->where('user_id', $id)
            ->paginate($perPage);
        } elseif (Auth::user()->hasRole('teacher')) {
            $teacherId = User::where('teacher_kode', Auth::user()->teacher_kode)->first()->teacher->id;
            $classes = Classes::withCount('students', 'teachers')
            ->join('teacher_class', 'classes.id', '=', 'teacher_class.class_id')
            ->where('teacher_class.teacher_id', $teacherId)
            ->paginate($perPage);
        }

        $classGrades = Classes::where('user_id', $id)->get();
        $grades = [];

        foreach ($classGrades as $class) {
            $words = explode(' ', $class->class_name);
            $grades[] = $words[0];
        }
        $grades = array_unique($grades);

        if ($classes->isEmpty()) {
            return redirect()->route('class.create', ['id' => $id]);
        }

        return view('classes', compact('classes', 'id', 'perPage', 'grades'));
    }

    public function search(Request $request, $id)
    {
        $perPage = $request->input('perPage', 9999);
        if ($perPage === 'semua') {
            $perPage = 9999;
        }
        $search = $request->input('search');
        $selectedNumeral = $request->input('selected_numeral');

        if (Auth::user()->hasRole('admin')) {
            $classes = Classes::withCount('students', 'teachers')
                ->where('classes.user_id', $id)
                ->where(function ($query) use ($search, $selectedNumeral) {
                    $query->where('class_name', 'LIKE', '%' . $search . '%');

                    if (!empty($selectedNumeral)) {
                        $query->where('class_name', 'LIKE', $selectedNumeral . ' %');
                    }
                })
                ->leftJoin('students', 'classes.id', '=', 'students.class_id')
                ->groupBy('classes.id')
                ->paginate($perPage);
        } elseif (Auth::user()->hasRole('teacher')) {
            $teacherId = User::where('teacher_kode', Auth::user()->teacher_kode)->first()->teacher->id;
            $classes = Classes::join('teacher_class', 'classes.id', '=', 'teacher_class.class_id')
                ->where('teacher_class.teacher_id', $teacherId)
                ->where(function ($query) use ($search, $selectedNumeral) {
                    $query->where('class_name', 'LIKE', '%' . $search . '%');

                    if (!empty($selectedNumeral)) {
                        $query->where('class_name', 'LIKE', $selectedNumeral . ' %');
                    }
                })
                ->leftJoin('students', 'classes.id', '=', 'students.class_id')
                ->groupBy('classes.id')
                ->paginate($perPage);
        }

        $classGrades = Classes::where('user_id', $id)->get();
        $grades = [];

        foreach ($classGrades as $class) {
            $words = explode(' ', $class->class_name);
            $grades[] = $words[0];
        }
        $grades = array_unique($grades);

        return view('classes', compact('classes', 'search', 'id', 'perPage', 'grades'));
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create($id)
    {
        $classes = Classes::where('user_id', $id)->get();
        return view('classesTambah', compact('classes', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, $id)
    {
        $validatedData = $request->validate([
            'classAmount' => 'required|integer|min:1',
        ], [
            'classAmount.required' => 'Jumlah kelas harus diisi.',
            'classAmount.integer' => 'Jumlah kelas harus berupa angka.',
            'classAmount.min' => 'Jumlah kelas minimal harus 1.',
        ]);

        $classAmount = $validatedData['classAmount'];
        $createdClasses = [];

        for ($i = 1; $i <= $classAmount; $i++) {
            $validatedClassData = $request->validate([
                "class{$i}" => 'required',
                "class{$i}_name" => 'required|string|max:255',
            ], [
                "class{$i}.required" => "Kode kelas {$i} harus diisi.",
                "class{$i}_name.required" => "Nama kelas {$i} harus diisi.",
                "class{$i}_name.max" => "Nama kelas {$i} tidak boleh melebihi 255 karakter.",
            ]);

            $classCode = $validatedClassData["class{$i}"];
            $className = $validatedClassData["class{$i}_name"];
            $fullClassName = $classCode . ' ' . $className;

            $existingClass = Classes::where('user_id', auth()->user()->id)
                ->where('class_name', $fullClassName)
                ->first();

            if ($existingClass) {
                $errorMessage = "Kelas dengan nama '{$fullClassName}' sudah ada.";
                return redirect()->back()->withErrors([$errorMessage]);
            }

            $class = new Classes();
            $class->class_name = $fullClassName;
            $class->user_id = auth()->user()->id;
            $class->save();

            $createdClasses[] = $fullClassName;
        }

        $classCount = count($createdClasses);
        $successMessage = "{$classCount} kelas baru berhasil dibuat";

        session()->flash('success', $successMessage);

        return redirect()->route('class.show', $id);
    }


    public function deleteSelected(Request $request)
    {
        $ids = $request->input('selected');

        $deletedCount = Classes::whereIn('id', $ids)->delete();

        if ($deletedCount > 0) {
            $successMessage = "{$deletedCount} kelas berhasil dihapus";
            session()->flash('success', $successMessage);
        }

        return back();
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $idClass)
    {
        $class = Classes::where('id', $idClass)->firstOrFail();
        $classBefore = $class->class_name;

        $className = $request->input('class') . ' ' . $request->input('class_name');
        $existingClass = Classes::where('user_id', auth()->user()->id)
                            ->where('class_name', $className)
                            ->first();

        if ($existingClass) {
            $errorMessage = "Kelas dengan nama '{$className}' sudah ada.";
            return redirect()->back()->withErrors([$errorMessage]);
        }

        $class->class_name = $className;
        $class->save();

        $classAfter = $class->class_name;
        $successMessage = "Kelas '{$classBefore}' berhasil diedit menjadi '{$classAfter}'";
        session()->flash('success', $successMessage);

        return redirect()->back();
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
