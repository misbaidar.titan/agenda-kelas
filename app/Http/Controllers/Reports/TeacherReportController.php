<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Interfaces\ReportGenerator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use App\Models\Schedule;
use App\Models\Teacher;
use App\Models\DailyAgenda;
use App\Models\DailyAgendaContent;
use PDF;

class TeacherReportController extends Controller
{
    public function report(Request $request, $id)
    {
        $start_date = request('start_date_teacher');
        $end_date = request('end_date_teacher');
        $teacher_id = request('selected_guru');

        // Get the dates between start and end date
        $dateRange = CarbonPeriod::create($start_date, $end_date);

        // Get the schedule of the selected teacher
        $schedules = Schedule::where('teacher_id', $teacher_id)->get();

        $report = [];

        foreach ($dateRange as $date) {
            $dayName = strtolower($date->locale('id')->dayName);

            // Check if the current day is in the teacher's schedule
            $matchingSchedules = $schedules->where('day', $dayName);

            foreach ($matchingSchedules as $schedule) {
                $classSchedule = $schedule->class_id;
                $dailyAgenda = DailyAgenda::whereDate('date', $date->format('Y-m-d'))->where('class_id', $classSchedule)->first();

                if ($dailyAgenda) {
                    $hasAgendaToday = DailyAgendaContent::where('daily_agenda_id', $dailyAgenda->id)
                        ->where('subject_id', $schedule->subject_id)
                        ->where('teacher_kode', $teacher_id)
                        ->exists();
                } else {
                    $hasAgendaToday = false;
                }

                $status = $hasAgendaToday ? 'Mengisi' : 'Tidak mengisi';

                $report[$schedule->subject_id][] = [
                    'date' => $date->format('Y-m-d'),
                    'status' => $status,
                    'dailyAgenda' => $dailyAgenda,
                    'class' => $schedule->class,
                    'subject' => $schedule->subject,
                ];
            }
        }

        $teacher = Teacher::leftJoin('teacher_subject', 'teachers.id', '=', 'teacher_subject.teacher_id')
        ->leftJoin('subjects', 'teacher_subject.subject_id', '=', 'subjects.id')
        ->leftJoin('teacher_class', 'teachers.id', '=', 'teacher_class.teacher_id')
        ->leftJoin('classes', 'teacher_class.class_id', '=', 'classes.id')
        ->leftJoin('users', 'teachers.user_id', '=', 'users.id')
        ->where('teachers.id', $teacher_id)
        ->select(
            'teachers.id',
            'teachers.kode',
            'teachers.teacher_name',
            'teachers.password',
            'users.picture',
            'users.email',
            DB::raw('GROUP_CONCAT(DISTINCT subjects.subject_name) as subject_names'),
            DB::raw('GROUP_CONCAT(DISTINCT classes.class_name) as class_names')
        )
        ->groupBy('teachers.id', 'teachers.kode', 'teachers.teacher_name', 'teachers.password', 'users.picture', 'users.email')
        ->get();

        $start_date = Carbon::parse($start_date)->locale('id')->isoFormat('D MMMM YYYY');
        $end_date = Carbon::parse($end_date)->locale('id')->isoFormat('D MMMM YYYY');

        session([
            'report' => $report,
            'teacher' => $teacher,
            'start_date' => $start_date,
            'end_date' => $end_date,
        ]);

        return view('admin.reportTeacher', compact('report', 'id', 'teacher', 'start_date', 'end_date'));
    }
    public function reportPDF()
    {
        $data = [
            'report' => session('report'),
            'teacher' => session('teacher'),
            'start_date' => session('start_date'),
            'end_date' => session('end_date'),
        ];


        $pdf = PDF::loadView('admin.reportTeacherPDF', $data)
            ->setPaper('a4', 'portrait');

        $filename = 'laporan_guru_' . $data['teacher']->first()->teacher_name . '_' . $data['start_date'] . '_' . $data['end_date'] . '.pdf';

        return $pdf->download($filename);
    }
}
