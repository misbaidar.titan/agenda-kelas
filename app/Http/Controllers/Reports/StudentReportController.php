<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Interfaces\ReportGenerator;
use App\Models\Classes;
use App\Models\Student;
use App\Charts\StudentAbsenceChart;
use Carbon\Carbon;
use PDF;

class StudentReportController extends Controller
{
    public function report(Request $request, $id)
    {
        $selectedClass = $request->selected_class;
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $students = Student::with(['absentStudents' => function ($query) use ($selectedClass, $startDate, $endDate) {
            $query->whereHas('dailyAgenda', function ($q) use ($selectedClass, $startDate, $endDate) {
                $q->where('class_id', $selectedClass)
                    ->whereBetween('date', [$startDate, $endDate]);
            });
        }])->where('class_id', $selectedClass)->get();

        $students->each(function ($student) use ($request, $selectedClass, $startDate, $endDate) {
            $student->sakit_total = $student->absentStudents()->whereHas('dailyAgenda', function ($q) use ($request, $selectedClass, $startDate, $endDate) {
                $q->where('class_id', $selectedClass)
                    ->whereBetween('date', [$startDate, $endDate]);
            })->where('reason', 'sakit')->count();

            $student->izin_total = $student->absentStudents()->whereHas('dailyAgenda', function ($q) use ($request, $selectedClass, $startDate, $endDate) {
                $q->where('class_id', $selectedClass)
                    ->whereBetween('date', [$startDate, $endDate]);
            })->where('reason', 'izin')->count();

            $student->alpa_total = $student->absentStudents()->whereHas('dailyAgenda', function ($q) use ($request, $selectedClass, $startDate, $endDate) {
                $q->where('class_id', $selectedClass)
                    ->whereBetween('date', [$startDate, $endDate]);
            })->where('reason', 'alpa')->count();

            $student->total = $student->sakit_total + $student->izin_total + $student->alpa_total;
            $student->chart = (new StudentAbsenceChart)->build($student);
        });

        $class = Classes::where('id', $selectedClass)->first();
        $startDate = Carbon::parse($request->start_date)->locale('id')->isoFormat('D MMMM YYYY');
        $endDate = Carbon::parse($request->end_date)->locale('id')->isoFormat('D MMMM YYYY');

        session([
            'students' => $students,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'class' => $class,
        ]);

        return view('admin.reportStudent', compact('class','startDate','endDate'), ['students' => $students, 'id' => $id]);
    }

    public function reportPDF()
    {
        $data = [
            'students' => session('students'),
            'startDate' => session('startDate'),
            'endDate' => session('endDate'),
            'class' => session('class'),
        ];

        $pdf = PDF::loadView('admin.reportStudentPDF', $data)
            ->setPaper('a4', 'portrait');

        $filename = 'rekap_absensi_' . $data['class']->class_name . '_' . $data['startDate'] . '_' . $data['endDate'] . '.pdf';

        return $pdf->download($filename);
    }
}
