<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function showAuthPilih()
    {
        return view('auth.auth_pilih');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $messages = [
            'email.required'  => 'Harap bagian email diisi.',
            'password.required'  => 'Harap bagian password diisi.',
        ];

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $credentials = $request->only('email', 'password');
        $remember = $request->has('remember');

        if (Auth::attempt($credentials, $remember)) {
            $user = Auth::user();

            if ($user->hasRole('teacher')) {
                $admin = User::where('school_name', $user->school_name)
                    ->whereDoesntHave('teacher')
                    ->first();

                if ($admin) {
                    $id = $admin->id;
                    return redirect()->route('dashboard-guru.show', ['id' => $id]);
                }
            }

            $id = $user->id;
            return redirect()->route('dashboard.show', ['id' => $id]);
        }

        return back()->withErrors(['message' => 'Terdapat kesalahan pada password atau nama'])->withInput();
    }



    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {
        $messages = [
            'name.required'  => 'Harap bagian nama diisi.',
            'school_name.required'  => 'Harap bagian nama sekolah diisi.',
            'password.required'  => 'Harap bagian password diisi.',
            'name.unique' => 'Nama sudah ada yang menggunakan',
            'password.min' => 'Password minimal 3 karakter',
        ];

        $request->validate([
            'name' => 'required|unique:users',
            'school_name' => 'required',
            'password' => 'required|min:3',
        ], $messages);

        $user = new User([
            'name' => $request->name,
            'school_name' => $request->school_name,
            'password' => Hash::make($request->password),
        ]);

        $user->save();

        $UserRole = Role::where('name', 'admin')->where('guard_name', Auth::getDefaultDriver())->first();
        $user->assignRole($UserRole);

        Auth::login($user);
        $request->session()->flash('message', 'Registrasi berhasil. Log in untuk melanjutkan');
        return redirect('/login');
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();
        
        $request->session()->flash('message', 'Anda berhasil log out');
        return redirect('/login');
    }

    public function forgotForm()
    {
        return view('auth.forgot_password');
    }

    public function forgotPassword(Request $request)
    {
        $request->validate(['email' => 'required|email']);

        $user = User::where('email', $request->email)->first();

        if ($user === null) {
            return back()->withErrors(['error' => 'Email yang diberikan tidak terdaftar']);
        }

        $token = Password::createToken($user);

        $user->sendPasswordResetNotification($token);

        return back()->with(['message' => 'Kami sudah mengirimkan link ke email yang diberikan']);
    }

    public function showResetForm(Request $request, $token = null, $email = null)
    {
        return view('auth.password_reset')->with(
            ['token' => $token,
             'email' => $email,
            ]
        );
    }

    public function reset(Request $request)
    {
        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:8',
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $status = Password::reset($credentials, function ($user, $password) {
            $user->forceFill([
                'password' => bcrypt($password),
                'remember_token' => Str::random(60),
            ])->save();

            Auth::login($user);
        });

        if ($status == Password::PASSWORD_RESET) {
            $user = Auth::user();

            if ($user->hasRole('teacher')) {
                $admin = User::where('school_name', $user->school_name)
                    ->whereNull('teacher_kode')
                    ->first();

                if ($admin) {
                    $id = $admin->id;
                    return redirect()->route('dashboard-guru.show', ['id' => $id])->with('status', __($status));
                }
            }

            // Assuming 'admin' is the role for admin users
            if ($user->hasRole('admin')) {
                return redirect()->route('dashboard.show', ['id' => $user->id])->with('status', __($status));
            }
        }

        throw ValidationException::withMessages(['email' => [__($status)]]);
    }
}
