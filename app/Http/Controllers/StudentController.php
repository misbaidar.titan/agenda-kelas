<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\Classes;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\StudentsImport;
use App\Exports\ExampleExportStudent;
use Exception;

class StudentController extends Controller
{
    public function show(Request $request, $id)
    {
        $perPage = $request->input('perPage', 10);
        if ($perPage === 'semua') {
            $perPage = 9999;
        }
        $student = Student::where('user_id', $id)->paginate($perPage);
        $classes = Classes::where('user_id', $id)->get();

        return view('student', compact('student', 'classes', 'id', 'perPage'));
    }

    public function create($id)
    {
        $classes = Classes::where('user_id', $id)->get();
        $student = Student::where('user_id', $id)->get();
        return view('studentTambah', compact('student', 'classes', 'id'));
    }

    public function store(Request $request, $id)
    {
        $validatedData = $request->validate([
            'studentAmount' => 'required|integer|min:1',
        ]);

        $studentAmount = $validatedData['studentAmount'];


        for ($i = 1; $i <= $studentAmount; $i++) {
            $validatedStudentData = $request->validate([
                "student{$i}_kode" => 'required|integer|unique:students,student_kode|digits:10',
                "student{$i}_name" => 'required|string|max:255',
                "class{$i}_name" => 'nullable|string|max:255',
                "kelasbaru{$i}_name" => 'nullable|string|max:255',
                "tingkatkelasbaru{$i}_name" => 'nullable|integer',
            ]);

            $className = $validatedStudentData["tingkatkelasbaru{$i}_name"] ?? null;

            if ($className !== null) {
                $className .= ' ' . $validatedStudentData["kelasbaru{$i}_name"];
            }

            $student = new Student();
            $student->student_kode = $validatedStudentData["student{$i}_kode"];
            $student->student_name = $validatedStudentData["student{$i}_name"];

            if ($className) {
                $class = new Classes();
                $class->class_name = $className;
                $class->user_id = auth()->user()->id;
                $class->save();

                $student->class_id = $class->id;
            } else {
                $className = $validatedStudentData["class{$i}_name"];
                $class = Classes::where('class_name', $className)
                    ->where('user_id', $id)
                    ->firstOrFail();
                $student->class_id = $class->id;
            }

            $student->user_id = auth()->user()->id;
            $student->save();
        }

        return redirect()->route('student.show', $id);
    }

    public function import(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xlsx,xls|max:2048',
        ]);

        try {
            Excel::import(new StudentsImport(), $request->file('file'));

            return redirect()->back()->with('success', 'Students imported successfully');
        } catch (Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function deleteSelected(Request $request)
    {
        $ids = $request->input('selected');

         Student::whereIn('id', $ids)->delete();

         return back()->with('success', 'Selected rows have been deleted.');
    }

    public function search(Request $request, $id)
    {
        $perPage = $request->input('perPage', 9999);
        if ($perPage === 'semua') {
            $perPage = 9999;
        }
        $search = $request->input('search');
        $classId = $request->input('class_id');
        $selectedClass = $request->input('selected_class');

        $student = Student::where('user_id', $id)
            ->where(function ($query) use ($search) {
                $query->where('student_name', 'LIKE', '%' . $search . '%')
                    ->orWhere('student_kode', 'LIKE', '%' . $search . '%');
            })
            ->when(!empty($selectedClass) || !empty($classId), function ($query) use ($selectedClass, $classId) {
                $query->where('class_id', $selectedClass)
                    ->orWhere('class_id', $classId);
            })
            ->paginate($perPage);

        $classes = Classes::where('user_id', $id)->get();

        return view('student', compact('student', 'classes', 'search', 'id', 'perPage'));
    }

    public function update(Request $request, $idStudent)
    {
        $student = Student::where('id', $idStudent)->firstOrFail();
        $student->student_kode = $request->input('student_kode');
        $student->student_name = $request->input('student_name');
        $student->class_id = $request->input('class_name');
        $student->save();

        return redirect()->back()->with('success', 'Class information updated successfully.');
    }

    public function export()
    {
        return Excel::download(new ExampleExportStudent, 'template_siswa.xlsx');
    }
}
