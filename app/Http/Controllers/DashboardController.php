<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use App\Models\User;
use App\Models\Teacher;
use App\Models\Subject;
use App\Models\Student;
use App\Models\Classes;
use App\Models\DailyAgenda;
use App\Models\DailyAgendaContent;
use App\Models\AbsentStudent;
use App\Models\Schedule;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function show($id)
    {
        $totalTeachers = DB::table('teacher_view')
        ->where('school_name', auth()->user()->school_name)
        ->count();
        $totalSubjects = Subject::where('user_id', $id)->count();
        $totalStudents = Student::where('user_id', $id)->count();
        $totalClasses = Classes::where('user_id', $id)->count();

        $today = Carbon::now()->locale('id')->dayName;
        $schedules = Schedule::where('day', $today)->get();
        $todayDate = Carbon::now()->format('Y-m-d');
        foreach ($schedules as $schedule) {
            $classSchedule = $schedule->class_id;
            $dailyAgenda = DailyAgenda::whereDate('date', $todayDate)->where('class_id', $classSchedule)->first();
            if($dailyAgenda) {
                $hasAgendaToday = DailyAgendaContent::where('daily_agenda_id', $dailyAgenda->id)
                                                    ->where('subject_id', $schedule->subject_id)
                                                    ->where('teacher_kode', $schedule->teacher_id)
                                                    ->exists();
            } else {
                $hasAgendaToday = false;
            }
            $schedule->status = $hasAgendaToday ? 'Sudah mengisi' : 'Belum mengisi';
            $schedule->day_id = $dailyAgenda;
        }

        $classes = Classes::where('user_id', $id)->get();
        $teachers = DB::table('teacher_view')->where('school_name', auth()->user()->school_name)->get();

        return view('admin.dashboardAdmin', compact('id', 'totalTeachers', 'totalSubjects', 'totalStudents', 'totalClasses', 'schedules', 'classes', 'teachers'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "email" => [
                'required',
                'email',
                Rule::unique('users', 'email')->ignore($id), // Exclude the current teacher
            ],
            'name' => 'required|string|max:255',
            'school_name' => 'required|string|max:255',
            'current_password' => 'nullable|string',
            'new_password' => 'nullable|string|min:3',
            'picture' => 'image',
        ]);

        $user = User::find($id);

        $user->email = $request->input('email');
        $user->name = $request->input('name');

        $oldSchoolName = $user->school_name;
        $newSchoolName = $request->input('school_name');

        $usersWithOldSchoolName = User::where('school_name', $oldSchoolName)->get();

        foreach ($usersWithOldSchoolName as $userWithOldSchoolName) {
            $userWithOldSchoolName->update(['school_name' => $newSchoolName]);
        }

        $user->update(['school_name' => $newSchoolName]);

        if ($request->filled('current_password') && $request->filled('new_password')) {
            if (Hash::check($request->input('current_password'), $user->password)) {
                $user->password = Hash::make($request->input('new_password'));
            } else {
                return redirect()->back()->withErrors(['error' => 'Incorrect current password']);
            }
        }

        if ($request->input('remove_picture')) {
            Storage::delete('public/pictures/'.$user->picture);
            $user->picture = null;
        }

        if ($request->hasFile("picture")) {
            if (!is_null($user->picture)) {
                Storage::delete('public/pictures/'.$user->picture);
            }

            $image = $request->file("picture");
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('public/pictures', $filename);
            $user->picture = $filename;
        }

        $user->save();

        return redirect()->route('dashboard.show', compact('id'))->with('success', 'Profile updated successfully');
    }

    public function guru($id)
    {
        $teacherId = auth()->user()->teacher->id;
        $weekDays = ['senin', 'selasa', 'rabu', 'kamis', 'jumat'];
        $hours = range(1, 10);

        $schedules = Schedule::where('teacher_id', $teacherId)
            ->get()
            ->map(function ($schedule) {
                $schedule->duration = $schedule->end_hour - $schedule->start_hour + 1;
                return $schedule;
            })
            ->groupBy('day')
            ->map(function ($schedulesPerDay) {
                return $schedulesPerDay->groupBy('start_hour');
            });
            $remainingDuration = array_fill_keys($weekDays, 0);


        $teacher = Teacher::leftJoin('teacher_subject', 'teachers.id', '=', 'teacher_subject.teacher_id')
        ->leftJoin('subjects', 'teacher_subject.subject_id', '=', 'subjects.id')
        ->leftJoin('teacher_class', 'teachers.id', '=', 'teacher_class.teacher_id')
        ->leftJoin('classes', 'teacher_class.class_id', '=', 'classes.id')
        ->leftJoin('users', 'teachers.user_id', '=', 'users.id')
        ->where('teachers.id', $teacherId)
        ->select(
            'teachers.id',
            'teachers.user_id',
            'teachers.kode',
            'teachers.teacher_name',
            'teachers.password',
            'users.picture',
            'users.email',
            DB::raw('GROUP_CONCAT(DISTINCT subjects.subject_name) as subject_names'),
            DB::raw('GROUP_CONCAT(DISTINCT classes.class_name) as class_names')
        )
        ->groupBy('teachers.id', 'teachers.user_id', 'teachers.kode', 'teachers.teacher_name', 'teachers.password', 'users.picture', 'users.email')
        ->get();

        $today = Carbon::now()->locale('id')->dayName;
        $teacherId = auth()->user()->teacher->id; // Assuming you've defined $teacherId

        $schedule = Schedule::where('day', $today)->where('teacher_id', $teacherId)->get();
        $todayDate = Carbon::now()->format('Y-m-d');

        foreach ($schedule as $s) {
            $classSchedule = $s->class_id;

            $dailyAgenda = DailyAgenda::firstOrNew([
                'date' => $todayDate,
                'class_id' => $classSchedule,
            ]);

            if (!$dailyAgenda->exists) {
                $dailyAgenda->day = $today;
                $dailyAgenda->save();
            }

            $hasAgendaToday = DailyAgendaContent::where('daily_agenda_id', $dailyAgenda->id)
                ->where('subject_id', $s->subject_id)
                ->where('teacher_kode', $s->teacher_id)
                ->exists();

            $s->status = $hasAgendaToday ? 'Sudah diisi' : 'Belum diisi';
            $s->day_id = $dailyAgenda;

            if ($hasAgendaToday) {
                $s->dailycontent = DailyAgendaContent::where('daily_agenda_id', $dailyAgenda->id)
                    ->where('subject_id', $s->subject_id)
                    ->where('teacher_kode', $s->teacher_id)
                    ->first();
            }
        }

        $uniqueClasses = $schedule->unique('class_id');

        $classArray = [];

        foreach ($uniqueClasses as $classSchedule) {
            $students = Student::where('class_id', $classSchedule->class->id)->get();

            $absentStudents = [];

            $dailyAgenda = DailyAgenda::whereDate('date', $todayDate)
            ->where('class_id', $classSchedule->class->id)
            ->first();

            if (!$dailyAgenda) {
                $dailyAgenda = new DailyAgenda();
                $dailyAgenda->date = $todayDate;
                $dailyAgenda->day = $today;
                $dailyAgenda->class_id = $classSchedule->class->id;
                $dailyAgenda->save();
            }

            foreach ($students as $student) {
                $absentStudentsForStudent = AbsentStudent::where('daily_agenda_id', $dailyAgenda->id)
                    ->where('student_kode', $student->id)
                    ->get();

                foreach ($absentStudentsForStudent as $absentStudent) {
                    $absentStudents[] = $absentStudent;
                }
            }

            $classData = [
                'class_schedule' => $classSchedule->class,
                'students' => $students,
                'daily_agenda' => $dailyAgenda,
                'absent_students' => $absentStudents,
            ];

            $classArray[] = $classData;
        }

        $archives = DailyAgendaContent::with(['dailyAgenda.class', 'dailyAgenda'])
        ->where('teacher_kode', $teacherId)
        ->get()
        ->sortBy('lesson_hours')
        ->groupBy(function ($item) {
            return $item->dailyAgenda->date;
        })
        ->map(function ($groupedItems) {
            return $groupedItems->groupBy('dailyAgenda.class_id');
        })
        ->filter(function ($group, $date) {
            $oneMonthAgo = Carbon::now()->subMonth();
            return Carbon::parse($date)->greaterThanOrEqualTo($oneMonthAgo);
        })
        ->sortByDesc(function ($item, $key) {
            return $key;
        });

        return view('guru.dashboardGuru', compact('id', 'weekDays', 'hours', 'schedules', 'remainingDuration', 'teacher', 'schedule', 'classArray', 'archives'));
    }

    public function updateEmail(Request $request, $id, $user_id)
    {
        $request->validate([
            "email" => [
                'required',
                'email',
                Rule::unique('users', 'email')->ignore($user_id), // Exclude the current teacher
            ],
        ]);
        $user = User::where('id', $user_id)->firstorfail();
        $user->email = $request->input('email');
        $user->save();

        return redirect()->back()->with('success', 'Email berhasil diperbarui.');
    }

    public function storeAgenda(Request $request, $id, $day_id)
    {
        if ($request->hasFile("confirmation_image")) {
            $image = $request->file("confirmation_image");
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('public/confirmation_images', $filename);
        }

        for ($hour = $request->get('start_hour'); $hour <= $request->get('end_hour'); $hour++) {
            $dailycontent = new DailyAgendaContent;
            $dailycontent->lesson_hours = $hour;
            $dailycontent->subject_id = $request->get('subject_id');
            $dailycontent->learning_activities = $request->get('learning_activities');
            $dailycontent->description = $request->get('description');
            $dailycontent->indicator = $request->get('indicator');
            $dailycontent->confirmation_image = $filename;
            $dailycontent->daily_agenda_id = $day_id;
            $dailycontent->teacher_kode = $request->get('teacher_id');
            $dailycontent->save();
        }

        return redirect()->route('dashboard-guru.show', compact('id'))->with('success', 'Berhasil mengisi agenda kelas');
    }

    public function updateAgenda(Request $request, $id, $day_id)
    {
        if ($request->hasFile("confirmation_image")) {
            $image = $request->file("confirmation_image");
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('public/confirmation_images', $filename);
        }

        $teacherId = $request->get('teacher_id');
        $subjectId = $request->get('subject_id');

        // Find the existing records based on teacher and subject
        $dailycontent = DailyAgendaContent::where('teacher_kode', $teacherId)
            ->where('subject_id', $subjectId)
            ->where('daily_agenda_id', $day_id)
            ->get();

        foreach ($dailycontent as $content) {
            // Update each individual record
            $content->learning_activities = $request->get('learning_activities');
            $content->description = $request->get('description');
            $content->indicator = $request->get('indicator');

            // Check if confirmation_image is not empty
            if (!empty($filename)) {
                $content->confirmation_image = $filename;
            }

            $content->save();
        }

        return redirect()->route('dashboard-guru.show', compact('id'))->with('success', 'Berhasil mengisi agenda kelas');
    }

    public function storeAbsent(Request $request, $id, $day_id)
    {
        $absentData = $request->input('absent');

        // If no absence data is provided, delete all AbsentStudent records for the given daily agenda
        if (empty($absentData)) {
            AbsentStudent::where('daily_agenda_id', $day_id)->delete();
        } else {
            // Filter out and update the existing AbsentStudent records
            $studentIds = collect($absentData)->map(function ($absentItem) {
                list($studentId, $reason) = explode(',', $absentItem);
                return $studentId;
            })->toArray();

            // Delete AbsentStudent records for students who aren't in the absence data
            AbsentStudent::where('daily_agenda_id', $day_id)
                ->whereNotIn('student_kode', $studentIds)
                ->delete();

            foreach ($absentData as $absentItem) {
                list($studentId, $reason) = explode(',', $absentItem);

                $student = Student::where('id', $studentId)->first();

                if ($student) {
                    $existingRecord = AbsentStudent::where('student_kode', $studentId)
                        ->where('daily_agenda_id', $day_id)
                        ->first();

                    if ($existingRecord) {
                        // Update the reason if it's different
                        if ($existingRecord->reason !== $reason) {
                            $existingRecord->reason = $reason;
                            $existingRecord->save();
                        }
                    } else {
                        $absentStudent = new AbsentStudent();
                        $absentStudent->student_kode = $studentId;
                        $absentStudent->reason = $reason;
                        $absentStudent->daily_agenda_id = $day_id;
                        $absentStudent->save();
                    }
                }
            }
        }

        return redirect()->route('dashboard-guru.show', compact('id'))->with('success', 'Berhasil mengisi siswa yang tidak hadir');
    }

}
