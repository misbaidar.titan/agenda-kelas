<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use App\Models\Teacher;
use App\Models\Subject;
use App\Models\Classes;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\TeachersImport;
use App\Exports\ExampleExport;
use Exception;

class TeacherController extends Controller
{
    public function cariGuru(Request $request, $id)
    {
        $perPage = $request->input('perPage', 9999);
        if ($perPage === 'semua') {
            $perPage = 9999;
        }
        $user = Auth::user();
        $search = $request->input('search');
        $subjectFilter = $request->input('subject_filter');
        $ClassFilter = $request->input('class_filter');
        $idSubject = $request->input('idSubject');
        $idClass = $request->input('idClass');
        $teachers = DB::table('teachers')
        ->select('teachers.id', 'teachers.kode', 'teachers.teacher_name', 'teachers.password', 'users.picture', 'users.email')
        ->selectRaw('GROUP_CONCAT(DISTINCT subjects.subject_name) as subject_names')
        ->selectRaw('GROUP_CONCAT(DISTINCT classes.class_name) as class_names')
        ->leftJoin('teacher_subject', 'teachers.id', '=', 'teacher_subject.teacher_id')
        ->leftJoin('subjects', 'teacher_subject.subject_id', '=', 'subjects.id')
        ->leftJoin('teacher_class', 'teachers.id', '=', 'teacher_class.teacher_id')
        ->leftJoin('classes', 'teacher_class.class_id', '=', 'classes.id')
        ->leftJoin('users', 'teachers.user_id', '=', 'users.id')
        ->whereExists(function ($query) use ($user) {
            $query->select('id')
                ->from('users')
                ->whereColumn('teachers.user_id', 'users.id')
                ->where('users.school_name', $user->school_name);
        })
        ->where(function ($query) use ($search) {
            $query->where('teachers.kode', 'LIKE', "%$search%")
                ->orWhere('teachers.teacher_name', 'LIKE', "%$search%");
        })
        ->where(function ($query) use ($subjectFilter, $idSubject) {
            $query->where(function ($subquery) use ($subjectFilter, $idSubject) {
                if (!empty($subjectFilter) || !empty($idSubject)) {
                    $subquery->whereIn('teachers.id', function ($subsubquery) use ($subjectFilter, $idSubject) {
                        $subsubquery->select('teachers.id')
                            ->from('teachers')
                            ->leftJoin('teacher_subject', 'teachers.id', '=', 'teacher_subject.teacher_id')
                            ->leftJoin('subjects', 'teacher_subject.subject_id', '=', 'subjects.id')
                            ->where('subjects.id', $subjectFilter)
                            ->orWhere('subjects.id', $idSubject)
                            ->distinct();
                    });
                }
            });
        })
        ->where(function ($query) use ($ClassFilter, $idClass) {
            if (!empty($ClassFilter) && !empty($idClass)) {
                $query->whereIn('teachers.id', function ($subquery) use ($ClassFilter, $idClass) {
                    $subquery->select('teachers.id')
                        ->from('teachers')
                        ->leftJoin('teacher_class', 'teachers.id', '=', 'teacher_class.teacher_id')
                        ->leftJoin('classes', 'teacher_class.class_id', '=', 'classes.id')
                        ->where(function ($classCondition) use ($ClassFilter, $idClass) {
                            $classCondition->where('classes.id', $ClassFilter)
                                          ->orWhere('classes.id', $idClass);
                        })
                        ->distinct();
                });
            } elseif (!empty($ClassFilter)) {
                $query->whereIn('teachers.id', function ($subquery) use ($ClassFilter) {
                    $subquery->select('teachers.id')
                        ->from('teachers')
                        ->leftJoin('teacher_class', 'teachers.id', '=', 'teacher_class.teacher_id')
                        ->leftJoin('classes', 'teacher_class.class_id', '=', 'classes.id')
                        ->where('classes.id', $ClassFilter)
                        ->distinct();
                });
            } elseif (!empty($idClass)) {
                $query->whereIn('teachers.id', function ($subquery) use ($idClass) {
                    $subquery->select('teachers.id')
                        ->from('teachers')
                        ->leftJoin('teacher_class', 'teachers.id', '=', 'teacher_class.teacher_id')
                        ->leftJoin('classes', 'teacher_class.class_id', '=', 'classes.id')
                        ->where('classes.id', $idClass)
                        ->distinct();
                });
            }
        })
        ->groupBy('teachers.id', 'teachers.kode', 'teachers.teacher_name', 'teachers.password', 'users.picture', 'users.email')
        ->paginate($perPage);

        $subjects = Subject::where('user_id', $id)->get();
        $classes = Classes::where('user_id', $id)->get();
        $subjectFilterName = null;
        foreach ($subjects as $subject) {
            if ($subject->id == $subjectFilter) {
                $subjectFilterName = $subject->subject_name;
                break;
            }
        }
        $idSubjectName = null;
        foreach ($subjects as $subject) {
            if ($subject->id == $idSubject) {
                $idSubjectName = $subject->subject_name;
                break;
            }
        }
        $classFilterName = null;
        foreach ($classes as $class) {
            if ($class->id == $ClassFilter) {
                $classFilterName = $class->class_name;
                break;
            }
        }
        $idClassName = null;
        foreach ($classes as $class) {
            if ($class->id == $idClass) {
                $idClassName = $class->class_name;
                break;
            }
        }

        return view('guru', compact('teachers', 'id', 'search', 'classes', 'subjects', 'perPage', 'subjectFilterName', 'idSubjectName', 'classFilterName', 'idClassName'));
    }


        /**
     * Display a listing of the resource.
     */
    public function showGuru(Request $request, $id)
    {
        $perPage = $request->input('perPage', 10);
        if ($perPage === 'semua') {
            $perPage = 9999;
        }
        $user = Auth::user();
        $schoolName = $user->school_name;

        $teachers = DB::table('teacher_view')
            ->where('school_name', $schoolName)
            ->paginate($perPage);
        $subjects = Subject::where('user_id', $id)->get();
        $classes = Classes::where('user_id', $id)->get();
        $subjectFilterName = null;
        $idSubjectName = null;
        $classFilterName = null;
        $idClassName = null;
        return view('guru', compact('teachers', 'id', 'subjects', 'classes', 'perPage', 'subjectFilterName', 'idSubjectName', 'classFilterName', 'idClassName'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create($id)
    {
        $subjects = Subject::where('user_id', $id)->get();
        return view('guruTambah', compact('id', 'subjects'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, $id)
    {
        $validatedData = $request->validate([
            'teacherAmount' => 'required|integer|min:1',
        ]);

        $teacherAmount = $validatedData['teacherAmount'];

        $schoolName = Auth::user()->school_name;
        $subjectIds = null;

        for ($i = 1; $i <= $teacherAmount; $i++) {
            $subject = $request->{"subjectbaru{$i}_name"};
            if ($subject !== null) {
                $subjects = explode(',', $subject);
                $subjects = array_map(function($str) {
                    return preg_replace('/^\s+|\s+$/', '', $str);
                }, $subjects);

                foreach ($subjects as $subjectName) {
                    $subject = new Subject();
                    $subject->subject_name = $subjectName;
                    $subject->user_id = Auth::user()->id;
                    $subject->save();
                    $subjectIds[] = $subject->id;
                }
            }

            $validatedTeacherData = $request->validate([
                "teacher{$i}_kode" => 'required|integer|unique:teachers,kode',
                "teacher{$i}_email" => 'required|email|unique:users,email',
                "teacher{$i}_name" => 'required|string|max:255',
                "subjects{$i}" => 'nullable|array',
                "subjects{$i}.*" => 'nullable|exists:subjects,id',
                "teacher{$i}_password" => 'required|string|min:6',
            ]);

            $teacherUser = new User();
            $teacherUser->name = $validatedTeacherData["teacher{$i}_name"];
            $teacherUser->school_name = $schoolName;
            $teacherUser->password = Hash::make($validatedTeacherData["teacher{$i}_password"]);
            $teacherUser->email = $validatedTeacherData["teacher{$i}_email"];
            $teacherUser->save();

            $user_id = $teacherUser->id;

            $teacher = new Teacher();
            $teacher->kode = $validatedTeacherData["teacher{$i}_kode"];
            $teacher->user_id = $user_id;
            $teacher->teacher_name = $validatedTeacherData["teacher{$i}_name"];
            $teacher->password = $validatedTeacherData["teacher{$i}_password"];
            $teacher->save();
            if ($subjectIds !== null) {
                $teacher->subject()->sync($subjectIds);
            } else {
                $teacher->subject()->sync($validatedTeacherData["subjects{$i}"]);
            }

            $teacherUserRole = Role::where('name', 'teacher')->where('guard_name', Auth::getDefaultDriver())->first();
            $teacherUser->assignRole($teacherUserRole);
        }

        return redirect()->route('teachers.showGuru', $id);
    }

    public function import(Request $request)
    {
        try {
            $schoolName = Auth::user()->school_name;
            $roleName = 'teacher';
            $guardName = Auth::getDefaultDriver();
            Excel::import(new TeachersImport($schoolName, $roleName, $guardName), $request->file('file'));
            $importCount = count(Excel::toArray(new TeachersImport, $request->file('file'))[0]);

            return back()->with('success', "{$importCount} guru berhasil di-import");
        } catch (Exception $e) {
            return back()->withErrors(['error' => $e->getMessage()]);
        }
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id, $idTeacher)
    {
        $user = Teacher::find($idTeacher)->user;
        $request->validate([
            "teachers_kode" => [
                'required',
                'integer',
                Rule::unique('teachers', 'kode')->ignore($idTeacher), // Exclude the current teacher
            ],
            "email" => [
                'required',
                'email',
                Rule::unique('users', 'email')->ignore($user->id), // Exclude the current teacher
            ],
            "name" => 'required|string|max:255',
            "password" => 'required|string|min:3',
            "picture" => 'image',
        ]);
        $teacher = Teacher::where('id', $idTeacher)->firstOrFail();

        $originalTeacherData = $teacher->toArray();
        $originalSubjects = $teacher->subject->pluck('subject_name', 'id')->toArray();
        $originalClasses = $teacher->classes->pluck('class_name', 'id')->toArray();

        $teacher->kode = $request->input('teachers_kode');
        $teacher->teacher_name = $request->input('name');
        $teacher->password = $request->input('password');
        $teacher->save();
        $request->validate([
            "subjects" => 'required|array',
            "subjects.*" => 'exists:subjects,id',
            "classes" => 'nullable|array',
            "classes.*" => 'exists:classes,id',
        ]);

        $teacher->subject()->sync($request->input('subjects'));
        $teacher->classes()->sync($request->input('classes'));

        $user_id = $teacher->user_id;

        $teacherUser = User::where('id', $user_id)->firstOrFail();
        $originalTeacherUserData = $teacherUser->toArray();

        $teacherUser->email = $request->input('email');
        $teacherUser->name = $teacher->teacher_name;
        $teacherUser->password = Hash::make($request->input('password'));

        if ($request->input('remove_picture')) {
            Storage::delete('public/pictures/'.$teacherUser->picture);
            $teacherUser->picture = null;
        }

        if ($request->hasFile("picture")) {
            if (!is_null($teacherUser->picture)) {
                Storage::delete('public/pictures/'.$teacherUser->picture);
            }

            $image = $request->file("picture");
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('public/pictures', $filename);
            $teacherUser->picture = $filename;
        }

        $teacherUser->save();

        $teacher = Teacher::where('id', $idTeacher)->with('subject', 'classes')->firstOrFail();
        $currentSubjects = $teacher->subject->pluck('subject_name', 'id')->toArray();
        $currentClasses = $teacher->classes->pluck('class_name', 'id')->toArray();

        Log::channel('events')->info("Proses update dilakukan pada data guru dengan dengan nama(kode) $teacher->teacher_name($idTeacher). Original data: " . json_encode($originalTeacherData) . ". Current data: " . json_encode($teacher->toArray()));

        Log::channel('events')->info("Proses update dilakukan pada data user guru dengan nama(id) $teacher->teacher_name($teacherUser->id). Original data: " . json_encode($originalTeacherUserData) . ". Current data: " . json_encode($teacherUser->toArray()));

        Log::channel('events')->info("Proses update dilakukan pada data mata pelajaran guru $teacher->teacher_name($idTeacher). Original data: " . json_encode($originalSubjects) . ". Current data: " . json_encode($currentSubjects));

        Log::channel('events')->info("Proses update dilakukan pada data kelas guru $teacher->teacher_name($idTeacher). Original data: " . json_encode($originalClasses) . ". Current data: " . json_encode($currentClasses));

        return redirect()->back()->with('success', 'Teacher information updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function deleteSelected(Request $request)
    {
        $ids = $request->input('selected');

        $userIds = Teacher::whereIn('kode', $ids)->pluck('user_id')->toArray();

        Teacher::whereIn('kode', $ids)->delete();

        User::whereIn('id', $userIds)->delete();

        return back()->with('success', 'Berhasil menghapus seluruh data yang dipilih');
    }

    public function export()
    {
        return Excel::download(new ExampleExport, 'template_guru.xlsx');
    }
}
