<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class DatabaseBackupController extends Controller
{
    public function backup()
    {
        Artisan::call('backup:run');

        $backupPath = storage_path('app/Agenda Kelas Digital');
        $backupFiles = scandir($backupPath, SCANDIR_SORT_DESCENDING);

        $latestBackup = $backupFiles[0];

        return response()->download("{$backupPath}/{$latestBackup}");
    }
}
