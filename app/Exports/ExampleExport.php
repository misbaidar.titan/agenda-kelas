<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ExampleExport implements FromCollection, WithColumnFormatting, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // Create an example collection of data
        return collect([
            ['1094758909384758', 'Supriadi Muhammad', 'Matematika', 'supriadi@gmail', '123'],
            ['5044794905687788', 'Adi Muzakar', 'Bahasa Indonesia', 'adi@gmail', '123'],
            ['3044321002567975', 'Eli Putri', 'Infomatika, Komputer', 'eli@gmail', '123'],
        ]);
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
