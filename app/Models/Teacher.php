<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;


class Teacher extends Authenticatable
{
    protected $fillable = [
        'name',
        'kode',
        'password',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function subject()
    {
        return $this->belongsToMany(Subject::class, 'teacher_subject');
    }

    public function classes()
    {
        return $this->belongsToMany(Classes::class, 'teacher_class', 'teacher_id', 'class_id');
    }

    use HasFactory, HasRoles, Notifiable;
}
