<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DailyAgendaContent extends Model
{
    use HasFactory;

    protected $fillable = ['lesson_hours', 'subject_id', 'indicator', 'learning_activities', 'description', 'confirmation_image', 'daily_agenda_id', 'teacher_kode'];

    public function dailyAgenda()
    {
        return $this->belongsTo(DailyAgenda::class);
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class, 'teacher_kode', 'id');
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id', 'id');
    }
}
