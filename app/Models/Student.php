<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'kode',
        'student_name',
        'class_id',
        'user_id'
    ];

    public function class()
    {
        return $this->belongsTo(Classes::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function absentStudents()
    {
        return $this->hasMany(AbsentStudent::class, 'student_kode');
    }
}
