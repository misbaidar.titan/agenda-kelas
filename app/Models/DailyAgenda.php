<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DailyAgenda extends Model
{
    use HasFactory;

    protected $fillable = ['day', 'date', 'description', 'class_id'];

    public function class()
    {
        return $this->belongsTo(Classes::class);
    }

    public function dailyAgendaContents()
    {
        return $this->hasMany(DailyAgendaContent::class);
    }

    public function absentStudents()
    {
        return $this->hasMany(AbsentStudent::class);
    }
}
