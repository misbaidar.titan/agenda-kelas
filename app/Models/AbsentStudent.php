<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AbsentStudent extends Model
{
    use HasFactory;

    protected $fillable = ['student_name', 'reason', 'daily_agenda_id'];

    public function dailyAgenda()
    {
        return $this->belongsTo(DailyAgenda::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_kode', 'id');
    }
}
