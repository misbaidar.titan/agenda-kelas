<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\ClassesController;
use App\Http\Controllers\DailyAgendaController;
use App\Http\Controllers\DailyContentController;
use App\Http\Controllers\MapelController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\ScheduleController;
use App\Http\Controllers\DatabaseBackupController;
use App\Http\Controllers\Reports\StudentReportController;
use App\Http\Controllers\Reports\TeacherReportController;

// Route::get('auth', [AuthController::class, 'showAuthPilih'])->name('auth');
// Route::get('register', [AuthController::class, 'showRegistrationForm'])->name('register');
// Route::post('register', [AuthController::class, 'register']);
Route::get('/', function () {
    return redirect()->route('login');
});
Route::get('login', [AuthController::class, 'showLoginForm'])->name('login');
Route::post('login', [AuthController::class, 'login']);
Route::get('lupaPassword', [AuthController::class, 'forgotForm'])->name('forgotForm');
Route::post('lupaPassword', [AuthController::class, 'forgotPassword'])->name('forgotPassword');
Route::post('logout', [AuthController::class, 'logout'])->name('logout');
Route::get('password/reset/{token}/{email}', [AuthController::class, 'showResetForm'])->name('password.reset');
Route::post('password/reset', [AuthController::class, 'reset'])->name('password.update');

Route::get('/backup-database', [DatabaseBackupController::class, 'backup'])
    ->name('database.dump');

Route::middleware(['check.authenticated', 'prevent-back-history'])->group(function () {
    Route::middleware(['teacher'])->group(function () {
        Route::get('/dashboard-guru/{id}', [DashboardController::class, 'guru'])
            ->name('dashboard-guru.show');

        Route::post('/dashboard-guru/{id}/store/{day_id}', [DashboardController::class, 'storeAgenda'])
        ->name('dashboard-guru.store');

        Route::put('/dashboard-guru/{id}/update/{day_id}', [DashboardController::class, 'updateAgenda'])
        ->name('dashboard-guru.update');

        Route::post('/dashboard-guru/{id}/storeAbsent/{day_id}', [DashboardController::class, 'storeAbsent'])
        ->name('dashboard-guru.storeAbsent');

        Route::post('/dashboard-guru/{id}/updateAbsent/{content_id}', [DashboardController::class, 'storeAgenda'])
        ->name('dashboard-guru.updateAbsent');

        Route::post('/dashboard-guru/{id}/updateEmail/{user_id}', [DashboardController::class, 'updateEmail'])
        ->name('dashboard-guru.updateEmail');

        Route::get('/classes/{id}/agenda-harian/{idClass}/harian-isi/{idDay}/tambah', [DailyContentController::class, 'create'])
            ->name('daily-content.create');

        Route::post('/classes/{id}/agenda-harian/{idClass}/harian-isi/{idDay}/tambah', [DailyContentController::class, 'store'])
            ->name('daily-content.store');

        Route::get('/classes/{id}/agenda-harian/{idClass}/harian-isi/{idDay}/tambahAbsent', [DailyContentController::class, 'createAbsent'])
            ->name('daily-content.createAbsent');

        Route::post('/classes/{id}/agenda-harian/{idClass}/harian-isi/{idDay}/tambahAbsent', [DailyContentController::class, 'storeAbsent'])
            ->name('daily-content.storeAbsent');
        });

    Route::middleware(['admin'])->group(function () {
        Route::get('/dashboard/{id}', [DashboardController::class, 'show'])
            ->name('dashboard.show');

        Route::put('/dashboard/{id}/update', [DashboardController::class, 'update'])
            ->name('dashboard.update');

        Route::get('/dashboard/{id}/reportTeacher', [TeacherReportController::class, 'report'])
            ->name('dashboard.reportTeacher');

        Route::get('/dashboard/{id}/reportTeacherPDF', [TeacherReportController::class, 'reportPDF'])
            ->name('dashboard.reportTeacherPDF');

        Route::get('/dashboard/{id}/reportStudent', [StudentReportController::class, 'report'])
            ->name('dashboard.reportStudent');

        Route::get('/dashboard/{id}/reportStudentPDF', [StudentReportController::class, 'reportPDF'])
            ->name('dashboard.reportStudentPDF');

        Route::get('/guru/{id}', [TeacherController::class, 'showGuru'])
            ->name('teachers.showGuru');

        Route::get('/guru/{id}/cari', [TeacherController::class, 'cariGuru'])
            ->name('teachers.cariGuru');

        Route::get('/guru/{id}/tambah', [TeacherController::class, 'create'])
            ->name('teachers.create');

        Route::post('/guru/{id}/tambah', [TeacherController::class, 'store'])
            ->name('teachers.store');

        Route::post('/guru/{id}/import', [TeacherController::class, 'import'])
            ->name('teachers.import');

        Route::get('/guru/{id}/export', [TeacherController::class, 'export'])
            ->name('teachers.export');

        Route::delete('/guru/{id}/delete-selected', [TeacherController::class, 'deleteSelected'])
            ->name('teachers.delete-selected');

        Route::put('/guru/{id}/{idTeacher}', [TeacherController::class, 'update'])
            ->name('teachers.update');

        Route::get('/mapel/{id}', [MapelController::class, 'show'])
            ->name('subject.show');

        Route::get('/mapel/{id}/cari', [MapelController::class, 'search'])
            ->name('subject.search');

        Route::get('/mapel/{id}/tambah', [MapelController::class, 'create'])
            ->name('subject.create');

        Route::post('/mapel/{id}/tambah', [MapelController::class, 'store'])
            ->name('subject.store');

        Route::delete('/mapel/{id}/delete-selected', [MapelController::class, 'deleteSelected'])
            ->name('subject.delete-selected');

        Route::put('/mapel/{idMapel}', [MapelController::class, 'update'])
            ->name('subject.update');

        Route::get('/Student/{id}', [StudentController::class, 'show'])
            ->name('student.show');

        Route::get('/Student/{id}/cari', [StudentController::class, 'search'])
            ->name('student.search');

        Route::get('/Student/{id}/tambah', [StudentController::class, 'create'])
            ->name('student.create');

        Route::post('/Student/{id}/tambah', [StudentController::class, 'store'])
            ->name('student.store');

        Route::post('/Student/{id}/import', [StudentController::class, 'import'])
            ->name('student.import');

        Route::get('/Student/{id}/export', [StudentController::class, 'export'])
            ->name('student.export');

        Route::delete('/Student/{id}/delete-selected', [StudentController::class, 'deleteSelected'])
            ->name('student.delete-selected');

        Route::put('/Student/{idStudent}', [StudentController::class, 'update'])
            ->name('student.update');

        Route::delete('/classes/{id}/delete-selected', [ClassesController::class, 'deleteSelected'])
            ->name('class.delete-selected');

        Route::put('/classes/{idClass}', [ClassesController::class, 'update'])
            ->name('class.update');

        Route::get('/classes/{id}/tambah', [ClassesController::class, 'create'])
            ->name('class.create');

        Route::post('/classes/{id}/tambah', [ClassesController::class, 'store'])
            ->name('class.store');

        Route::delete('/classes/{id}/agenda-harian/{idClass}/delete-selected', [DailyAgendaController::class, 'deleteSelected'])
            ->name('daily-agenda.delete-selected');

        Route::get('/classes/{id}/jadwal/{idClass}', [ScheduleController::class, 'show'])
            ->name('schedule.show');

        Route::get('/classes/{id}/jadwal/{idClass}/tambah', [ScheduleController::class, 'create'])
            ->name('schedule.create');

        Route::get('/classes/{id}/jadwal/{idClass}/cari', [ScheduleController::class, 'search'])
            ->name('schedule.search');

        Route::post('/classes/{id}/jadwal/{idClass}/tambah', [ScheduleController::class, 'store'])
            ->name('schedule.store');

        Route::put('/classes/{id}/jadwal/{idClass}/{idSchedule}', [ScheduleController::class, 'update'])
            ->name('schedule.update');

        Route::delete('/classes/{id}/jadwal/delete-selected', [ScheduleController::class, 'deleteSelected'])
            ->name('schedule.delete-selected');

        Route::get('/classes/{id}/jadwal', [ScheduleController::class, 'timetable'])
            ->name('schedule.timetable');

        Route::get('/classes/{id}/PDF', [ScheduleController::class, 'timetablePDF'])
            ->name('schedule.PDF');

        Route::get('/logs/{id}', function ($id) {
            $logs = DB::table('event_logs')->latest()->paginate(10);

            return view('eventlogs', ['logs' => $logs, 'id' => $id]);
        })->name('logs.show');
    });

    // guru dan admin dapat akses route ini

    Route::get('/classes/{id}', [ClassesController::class, 'index'])
        ->name('class.show');

    Route::get('/classes/{id}/cari', [ClassesController::class, 'search'])
        ->name('class.search');

    Route::get('/classes/{id}/agenda-harian/{idClass}', [DailyAgendaController::class, 'show'])
        ->name('daily-agenda.show');

    Route::get('/classes/{id}/agenda-harian/{idClass}/tambah', [DailyAgendaController::class, 'create'])
        ->name('daily-agenda.create');

    Route::post('/classes/{id}/agenda-harian/{idClass}/tambah', [DailyAgendaController::class, 'store'])
        ->name('daily-agenda.store');

    Route::get('/classes/{id}/agenda-harian/{idClass}/cari', [DailyAgendaController::class, 'search'])
        ->name('daily-agenda.search');

    Route::put('/classes/{id}/agenda-harian/{idDay}', [DailyAgendaController::class, 'update'])
        ->name('daily-agenda.update');

    Route::get('/classes/{id}/agenda-harian/{idClass}/harian-isi/{idDay}', [DailyContentController::class, 'show'])
        ->name('daily-content.show');

    Route::delete('/classes/{id}/agenda-harian/{idClass}/harian-isi/{idDay}/delete-selected', [DailyContentController::class, 'deleteSelected'])
        ->name('daily-content.delete-selected');

    Route::delete('/classes/{id}/agenda-harian/{idClass}/harian-isi/{idDay}/delete-selectedAbsent', [DailyContentController::class, 'deleteSelectedAbsent'])
        ->name('daily-content.delete-selectedAbsent');

    Route::put('/classes/{id}/agenda-harian/{idClass}/harian-isi/absent/{idStudent}', [DailyContentController::class, 'updateAbsent'])
        ->name('daily-content.updateAbsent');

    Route::put('/classes/{id}/agenda-harian/{idClass}/harian-isi/{idContent}', [DailyContentController::class, 'update'])
        ->name('daily-content.update');
});

