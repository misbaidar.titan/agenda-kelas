<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        // Define your query here
        $query = "
            SELECT
                teachers.id,
                teachers.kode,
                teachers.user_id,
                teachers.teacher_name,
                teachers.password,
                users.school_name,
                users.picture,
                users.email,
                GROUP_CONCAT(DISTINCT subjects.subject_name) as subject_names,
                GROUP_CONCAT(DISTINCT classes.class_name) as class_names
            FROM
                teachers
                LEFT JOIN teacher_subject ON teachers.id = teacher_subject.teacher_id
                LEFT JOIN subjects ON teacher_subject.subject_id = subjects.id
                LEFT JOIN teacher_class ON teachers.id = teacher_class.teacher_id
                LEFT JOIN classes ON teacher_class.class_id = classes.id
                LEFT JOIN users ON users.id = teachers.user_id
            GROUP BY
                teachers.id, teachers.kode, teachers.teacher_name, teachers.password, users.picture, users.email
        ";

        // Create the view
        DB::statement("CREATE VIEW teacher_view AS $query");
    }

    public function down()
    {
        // Drop the view
        DB::statement('DROP VIEW IF EXISTS teacher_view');
    }
};
