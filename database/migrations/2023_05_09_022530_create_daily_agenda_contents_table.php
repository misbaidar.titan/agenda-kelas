<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('daily_agenda_contents', function (Blueprint $table) {
            $table->id();
            $table->string('lesson_hours');
            $table->string('subject_study');
            $table->text('learning_activities')->nullable();
            $table->text('description')->nullable();
            $table->string('confirmation_image')->nullable();
            $table->unsignedBigInteger('daily_agenda_id');
            $table->foreign('daily_agenda_id')->references('id')->on('daily_agendas')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('teacher_kode');
            $table->foreign('teacher_kode')->references('kode')->on('teachers')->onDelete('no action')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('daily_agenda_contents');
    }
};
