<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('absent_students', function (Blueprint $table) {
            $table->unsignedBigInteger('student_kode')->after('id')->nullable();

            $table->foreign('student_kode')->references('student_kode')->on('students');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('absent_students', function (Blueprint $table) {
            $table->dropForeign(['student_kode']);
            $table->dropColumn(['student_kode']);
        });
    }
};
