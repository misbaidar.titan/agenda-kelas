<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::unprepared('
        CREATE TRIGGER student_insert_trigger
        AFTER INSERT ON students
        FOR EACH ROW
        BEGIN
            INSERT INTO event_logs (table_name, event, new_values, created_at, updated_at)
            VALUES ("students", "insert", CONCAT("student_kode: ", NEW.student_kode, ", student_name: ", NEW.student_name, ", class_id: ", NEW.class_id, ", user_id: ", NEW.user_id), NOW(), NOW());
        END
        ');
        DB::unprepared('
        CREATE TRIGGER student_update_trigger
        AFTER UPDATE ON students
        FOR EACH ROW
        BEGIN
            INSERT INTO event_logs (table_name, event, old_values, new_values, created_at, updated_at)
            VALUES ("students", "update", CONCAT("student_kode: ", OLD.student_kode, ", student_name: ", OLD.student_name, ", class_id: ", OLD.class_id, ", user_id: ", OLD.user_id),
            CONCAT("student_kode: ", NEW.student_kode, ", student_name: ", NEW.student_name, ", class_id: ", NEW.class_id, ", user_id: ", NEW.user_id), NOW(), NOW());
        END
        ');
        DB::unprepared('
        CREATE TRIGGER student_delete_trigger
        AFTER DELETE ON students
        FOR EACH ROW
        BEGIN
            INSERT INTO event_logs (table_name, event, old_values, created_at, updated_at)
            VALUES ("students", "delete", CONCAT("student_kode: ", OLD.student_kode, ", student_name: ", OLD.student_name, ", class_id: ", OLD.class_id, ", user_id: ", OLD.user_id), NOW(), NOW());
        END
        ');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::unprepared('DROP TRIGGER `student_insert_trigger`');
        DB::unprepared('DROP TRIGGER `student_update_trigger`');
        DB::unprepared('DROP TRIGGER `student_delete_trigger`');
    }
};
