<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('absent_students', function (Blueprint $table) {
            $table->dropColumn('student_name');

            $table->foreign('student_kode')->references('id')->on('students')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('absent_students', function (Blueprint $table) {
            $table->string('student_name');

            $table->foreign('student_kode')->references('kode')->on('students');
        });
    }
};
