<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('absent_students', function (Blueprint $table) {
            $table->id();
            $table->string('student_name');
            $table->text('reason');
            $table->unsignedBigInteger('daily_agenda_id');
            $table->foreign('daily_agenda_id')->references('id')->on('daily_agendas')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('absent_students');
    }
};
